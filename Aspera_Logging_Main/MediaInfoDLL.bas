Attribute VB_Name = "MediaInfoDLL"
Option Explicit

' Include this module in your Visual Basic project for access to MediaInfo.dll and
' keep two things in mind concerning strings for Unicode ability:
'
' 1. All strings (constants and variables) passed to MediaInfo must be passed with StrPtr().
' 2. All strings returned by MediaInfo must be converted with bstr() to a Visual Basic String.
'
' This module is tested and will work with Visual Basic 5 and 6 on Windows 98SE and 2000
' (at least).
'
' Use at own risk, under the same license as MediaInfo itself.
'
' Ingo Br�ckl, May 2006

#If MEDIAINFO_NO_ENUMS Then
#Else

Public Enum MediaInfo_stream_C
  MediaInfo_Stream_General
  MediaInfo_Stream_Video
  MediaInfo_Stream_Audio
  MediaInfo_Stream_Text
  MediaInfo_Stream_Chapters
  MediaInfo_Stream_Image
  MediaInfo_Stream_Menu
  MediaInfo_Stream_Max
End Enum

Public Enum MediaInfo_info_C
  MediaInfo_Info_Name
  MediaInfo_Info_Text
  MediaInfo_Info_Measure
  MediaInfo_Info_Options
  MediaInfo_Info_Name_Text
  MediaInfo_Info_Measure_Text
  MediaInfo_Info_Info
  MediaInfo_Info_HowTo
  MediaInfo_Info_Max
End Enum

Public Enum MediaInfo_infooptions_C
  MediaInfo_InfoOption_ShowInInform
  MediaInfo_InfoOption_Support
  MediaInfo_InfoOption_ShowInSupported
  MediaInfo_InfoOption_TypeOfValue
  MediaInfo_InfoOption_Max
End Enum

#End If

Type MediaInfoData
    txtFormat As String
    txtStreamType As String
    txtVideoCodec As String
    txtAudioCodec As String
    txtFrameRate As String
    txtCBRVBR As String
    txtVideoBitrate As String
    txtAudioBitrate As String
    txtOverallBitrate As String
    txtTrackCount As String
    txtChannelCount As String
    txtWidth As String
    txtHeight As String
    txtInterlace As String
    txtAspect As String
    txtGeometry As String
    txtDuration As String
    txtTimecodestart As String
    txtTimecodestop As String
    txtColorSpace As String
    txtChromaSubsmapling As String
    txtSeriesTitle As String
    txtSeriesNumber As String
    txtProgrammeTitle As String
    txtEpisodeNumber As String
    txtLineUpStart As String
    txtIdentClockStart As String
    txtTotalProgrammeDuration As String
    txtSynopsis As String
    txtCopyrightYear As String
    txtNumberOfParts As String
End Type

Public Declare Sub MediaInfo_Close Lib "MediaInfo.dll" (ByVal handle As Long)
Public Declare Sub MediaInfo_Delete Lib "MediaInfo.dll" (ByVal handle As Long)
Public Declare Function MediaInfo_Count_Get Lib "MediaInfo.dll" (ByVal handle As Long, ByVal StreamKind As MediaInfo_stream_C, ByVal StreamNumber As Long) As Long
Public Declare Function MediaInfo_Get Lib "MediaInfo.dll" (ByVal handle As Long, ByVal StreamKind As MediaInfo_stream_C, ByVal StreamNumber As Long, ByVal Parameter As Long, ByVal InfoKind As MediaInfo_info_C, ByVal SearchKind As MediaInfo_info_C) As Long
Public Declare Function MediaInfo_GetI Lib "MediaInfo.dll" (ByVal handle As Long, ByVal StreamKind As MediaInfo_stream_C, ByVal StreamNumber As Long, ByVal Parameter As Long, ByVal InfoKind As MediaInfo_info_C) As Long
Public Declare Function MediaInfo_Inform Lib "MediaInfo.dll" (ByVal handle As Long, ByVal Reserved As Long) As Long
Public Declare Function MediaInfo_New Lib "MediaInfo.dll" () As Long
Public Declare Function MediaInfo_New_Quick Lib "MediaInfo.dll" (ByVal File As Long, ByVal Options As Long) As Long
Public Declare Function MediaInfo_Open Lib "MediaInfo.dll" (ByVal handle As Long, ByVal File As Long) As Long
Public Declare Function MediaInfo_Open_Buffer Lib "MediaInfo.dll" (ByVal handle As Long, Begin As Any, ByVal Begin_Size As Long, End_ As Any, ByVal End_Size As Long) As Long
Public Declare Function MediaInfo_Option Lib "MediaInfo.dll" (ByVal handle As Long, ByVal Option_ As Long, ByVal Value As Long) As Long
Public Declare Function MediaInfo_Save Lib "MediaInfo.dll" (ByVal handle As Long) As Long
Public Declare Function MediaInfo_Set Lib "MediaInfo.dll" (ByVal handle As Long, ByVal ToSet As Long, ByVal StreamKind As MediaInfo_stream_C, ByVal StreamNumber As Long, ByVal Parameter As Long, ByVal OldParameter As Long) As Long
Public Declare Function MediaInfo_SetI Lib "MediaInfo.dll" (ByVal handle As Long, ByVal ToSet As Long, ByVal StreamKind As MediaInfo_stream_C, ByVal StreamNumber As Long, ByVal Parameter As Long, ByVal OldParameter As Long) As Long
Public Declare Function MediaInfo_State_Get Lib "MediaInfo.dll" (ByVal handle As Long) As Long

Private Declare Function lstrlenW Lib "kernel32" (ByVal pStr As Long) As Long
Private Declare Sub RtlMoveMemory Lib "kernel32" (pDst As Any, pSrc As Any, ByVal bLen As Long)

Public Function bstr(ptr As Long) As String
' convert a C wchar* to a Visual Basic string

  Dim l As Long

  l = lstrlenW(ptr)
  bstr = String$(l, vbNullChar)

  RtlMoveMemory ByVal StrPtr(bstr), ByVal ptr, l * 2

End Function
Public Function GetMediaInfoOnFile(lp_strFilePath As String) As MediaInfoData

Dim m_Framerate As Integer
Dim handle As Long, l_curAudioBitRate As Currency, Count As Long, TempStr As String, l_lngAudioChannelCount As Long
Dim lblFormat As String, lblGeneralFormat As String, lblVideoCodec As String, lblCodecID As String, lblAudioCodec As String, lblFrameRate As String
Dim lblVideoBitRate As String, lblInterlace As String, lblVideoBitDepth As String, lblOverallBitrate As String, lblSampleCount As String
Dim lblDisplayAspect As String, lblFrameCount As String, lblPixelRatio As String, lblBitDepth As String, lblAudioChannels As String
Dim lblAudioBitrate As String, lblTimecodeType As String, lblTimecode As String, lblMatroskaTimecode As String, lblNumberOfTracks As String, lblSamplingRate As String, lblTransferCharacteristics As String
Dim lblSeriesTitle As String, lblSeriesNumber As String, lblProgrammeTitle As String, lblEpisodeTitleNumber As String, lblEpisodeNumber As String
Dim lblLineUpStart As String, lblIdentClockStart As String, lblTotalProgrammeDuration As String, lblSynopsis As String, lblCopyrightYear As String, lblNumberOfParts As String

'Open the File with MediaInfo.dll and collect the available information
handle = MediaInfo_New()
Call MediaInfo_Open(handle, StrPtr(lp_strFilePath))
Screen.MousePointer = vbNormal

With GetMediaInfoOnFile
    lblFormat = bstr(MediaInfo_Get(handle, MediaInfo_Stream_General, 0, StrPtr("Format_Profile"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    If lblFormat = "" Then lblFormat = bstr(MediaInfo_Get(handle, MediaInfo_Stream_General, 0, StrPtr("Format_Commercial"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblGeneralFormat = bstr(MediaInfo_Get(handle, MediaInfo_Stream_General, 0, StrPtr("Format"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblVideoCodec = bstr(MediaInfo_Get(handle, MediaInfo_Stream_General, 0, StrPtr("Video_Format_List"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblCodecID = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, StrPtr("CodecID"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblAudioCodec = bstr(MediaInfo_Get(handle, MediaInfo_Stream_General, 0, StrPtr("Audio_Format_List"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblFrameRate = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, StrPtr("FrameRate"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    .txtCBRVBR = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, StrPtr("BitRate_Mode"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblVideoBitRate = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, StrPtr("BitRate_Maximum"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    If lblVideoBitRate = "" Then lblVideoBitRate = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, StrPtr("BitRate"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    .txtVideoBitrate = Int(Val(lblVideoBitRate) / 1000)
    If .txtVideoBitrate = 0 Then .txtVideoBitrate = ""
    .txtWidth = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, StrPtr("Width_Original"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    If .txtWidth = "" Then .txtWidth = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, StrPtr("Width"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    If .txtWidth = "" Then .txtWidth = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Image, 0, StrPtr("Width"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    .txtHeight = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, StrPtr("Height_Original"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    If .txtHeight = "" Then .txtHeight = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, StrPtr("Height"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    If .txtHeight = "" Then .txtHeight = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Image, 0, StrPtr("Height"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblInterlace = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, StrPtr("ScanType"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblDisplayAspect = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, StrPtr("DisplayAspectRatio"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblFrameCount = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, StrPtr("FrameCount"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblPixelRatio = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, StrPtr("PixelAspectRatio_Original"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    If lblPixelRatio = "" Then lblPixelRatio = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, StrPtr("PixelAspectRatio"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblAudioCodec = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Audio, 0, StrPtr("Format"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    If lblAudioCodec = "MPEG Audio" Then lblAudioCodec = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Audio, 0, StrPtr("Codec"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblBitDepth = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Audio, 0, StrPtr("BitDepth"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    .txtColorSpace = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, StrPtr("ColorSpace"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblTransferCharacteristics = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, StrPtr("transfer_characteristics"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    If lblTransferCharacteristics = "HLG" Then .txtColorSpace = IIf(.txtColorSpace <> "", .txtColorSpace & " ", "") & "HDR"
    .txtChromaSubsmapling = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, StrPtr("ChromaSubsampling"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblVideoBitDepth = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, StrPtr("BitDepth"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblSamplingRate = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Audio, 0, StrPtr("SamplingRate"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblSampleCount = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Audio, 0, StrPtr("SamplingCount"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblAudioChannels = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Audio, 0, StrPtr("Channel(s)"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    .txtTrackCount = bstr(MediaInfo_Get(handle, MediaInfo_Stream_General, 0, StrPtr("AudioCount"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    l_curAudioBitRate = 0
    l_lngAudioChannelCount = 0
    For Count = 0 To Val(.txtTrackCount) - 1
        If Val(bstr(MediaInfo_Get(handle, MediaInfo_Stream_Audio, Count, StrPtr("BitRate"), MediaInfo_Info_Text, MediaInfo_Info_Name))) <> 0 Then
            l_curAudioBitRate = l_curAudioBitRate + Val(bstr(MediaInfo_Get(handle, MediaInfo_Stream_Audio, Count, StrPtr("BitRate"), MediaInfo_Info_Text, MediaInfo_Info_Name)))
        Else
            l_curAudioBitRate = l_curAudioBitRate + Val(bstr(MediaInfo_Get(handle, MediaInfo_Stream_Audio, Count, StrPtr("BitRate_Maximum"), MediaInfo_Info_Text, MediaInfo_Info_Name)))
        End If
        l_lngAudioChannelCount = l_lngAudioChannelCount + Val(bstr(MediaInfo_Get(handle, MediaInfo_Stream_Audio, Count, StrPtr("Channel(s)"), MediaInfo_Info_Text, MediaInfo_Info_Name)))
    Next
    If Val(l_curAudioBitRate) = 0 Then
        If lblVideoBitRate <> "" And l_lngAudioChannelCount <> 0 Then
            l_curAudioBitRate = Val(bstr(MediaInfo_Get(handle, MediaInfo_Stream_General, 0, StrPtr("OverallBitRate"), MediaInfo_Info_Text, MediaInfo_Info_Name))) - Val(lblVideoBitRate)
        ElseIf lblVideoBitRate <> "" Then
            lblOverallBitrate = Val(lblVideoBitRate)
        Else
            lblOverallBitrate = bstr(MediaInfo_Get(handle, MediaInfo_Stream_General, 0, StrPtr("OverallBitRate"), MediaInfo_Info_Text, MediaInfo_Info_Name))
        End If
    End If
    .txtAudioBitrate = Int(l_curAudioBitRate / 1000)
    .txtChannelCount = l_lngAudioChannelCount
    If .txtAudioBitrate = "0" Then .txtAudioBitrate = ""
    If .txtAudioBitrate = "" And .txtVideoBitrate = "" Then .txtOverallBitrate = Int(Val(lblOverallBitrate) / 1000)
    lblTimecodeType = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Chapters, 0, StrPtr("Type"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblTimecode = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Chapters, 0, StrPtr("TimeCode_FirstFrame"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblMatroskaTimecode = bstr(MediaInfo_Get(handle, MediaInfo_Stream_General, 0, StrPtr("TimeCode_FirstFrame"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    If lblVideoCodec = "DV" Then
        lblVideoCodec = bstr(MediaInfo_Get(handle, MediaInfo_Stream_General, 0, StrPtr("Format_Commercial"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    End If
    If lblGeneralFormat = "QuickTime" Then
        lblFormat = "Quicktime"
        lblVideoCodec = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, StrPtr("Format_Commercial"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    ElseIf lblGeneralFormat = "MXF" Then
        lblFormat = "MXF " & lblFormat
        lblVideoCodec = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, StrPtr("Format_Commercial"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    ElseIf lblVideoCodec = "MPEG Video" And lblCodecID = "61" Then
        lblFormat = "MPEG 4"
        lblVideoCodec = "XDCAM EX 35"
    ElseIf lblVideoCodec = "MPEG Video" And lblFormat <> "QuickTime" Then
        If lblCodecID = "xdv7" Then
            lblVideoCodec = "XDCAM EX 35"
        ElseIf Left(bstr(MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, StrPtr("Format_Commercial"), MediaInfo_Info_Text, MediaInfo_Info_Name)), 6) = "MPEG-2" Then
            lblFormat = "MPEG 2"
        Else
            lblFormat = "MPEG"
        End If
    End If
    If lblFormat = "MPEG Audio" Then
        lblFormat = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Audio, 0, StrPtr("Codec"), MediaInfo_Info_Text, MediaInfo_Info_Name))
        lblAudioCodec = lblFormat & " " & lblAudioChannels
    End If
    
    Dim ParsingOut As RegExp
    Dim Matches As MatchCollection
    Set ParsingOut = New RegExp
    ParsingOut.Pattern = "(\d+)"
    
    If bstr(MediaInfo_Get(handle, MediaInfo_Stream_Chapters, 2, StrPtr("Format"), MediaInfo_Info_Text, MediaInfo_Info_Name)) = "AS-11 Core" Then
        lblSeriesTitle = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Chapters, 2, StrPtr("SeriesTitle"), MediaInfo_Info_Text, MediaInfo_Info_Name))
        Set Matches = ParsingOut.Execute(lblSeriesTitle)
        If Matches.Count > 0 Then
            If Matches(0).SubMatches.Count > 0 Then
                lblSeriesNumber = Matches(0).SubMatches(0)
            Else
                lblSeriesNumber = ""
            End If
        Else
            lblSeriesNumber = ""
        End If
        lblProgrammeTitle = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Chapters, 2, StrPtr("ProgrammeTitle"), MediaInfo_Info_Text, MediaInfo_Info_Name))
        lblEpisodeTitleNumber = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Chapters, 2, StrPtr("EpisodeTitleNumber"), MediaInfo_Info_Text, MediaInfo_Info_Name))
        Set Matches = ParsingOut.Execute(lblEpisodeTitleNumber)
        If Matches.Count > 0 Then
            If Matches(0).SubMatches.Count > 0 Then
                lblEpisodeNumber = Matches(0).SubMatches(0)
            Else
                lblEpisodeNumber = ""
            End If
        Else
            lblEpisodeNumber = ""
        End If
        lblLineUpStart = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Chapters, 3, StrPtr("LineUpStart"), MediaInfo_Info_Text, MediaInfo_Info_Name))
        lblIdentClockStart = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Chapters, 3, StrPtr("IdentClockStart"), MediaInfo_Info_Text, MediaInfo_Info_Name))
        lblTotalProgrammeDuration = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Chapters, 3, StrPtr("TotalProgrammeDuration"), MediaInfo_Info_Text, MediaInfo_Info_Name))
        lblSynopsis = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Chapters, 3, StrPtr("Synopsis"), MediaInfo_Info_Text, MediaInfo_Info_Name))
        lblCopyrightYear = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Chapters, 3, StrPtr("CopyrightYear"), MediaInfo_Info_Text, MediaInfo_Info_Name))
        lblNumberOfParts = bstr(MediaInfo_Get(handle, MediaInfo_Stream_Chapters, 3, StrPtr("TotalNumberOfParts"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    End If
    
    'Close the MediaInfo
    Call MediaInfo_Close(handle)
    Call MediaInfo_Delete(handle)
    
    'Translate the Infirmation into JCA speak

    If lblFormat = "Wave" Then
        .txtVideoCodec = GetAlias(lblVideoCodec)
    ElseIf lblCodecID = "AVdn" Then
        .txtVideoCodec = GetAlias(Trim(lblVideoCodec & " " & lblCodecID) & " " & lblVideoBitDepth)
    ElseIf lblFormat <> "" Then
        .txtVideoCodec = GetAlias(Trim(lblVideoCodec & " " & lblCodecID))
    End If
    If lblAudioCodec = "PCM" Or lblAudioCodec = "MPA1L2" Then
        .txtAudioCodec = GetAlias(Trim(lblAudioCodec & " " & lblBitDepth & " " & lblSamplingRate & " " & lblAudioChannels))
    Else
        .txtAudioCodec = GetAlias(lblAudioCodec)
    End If
    If Val(lblFrameRate) = Int(Val(lblFrameRate)) Then
        .txtFrameRate = Int(Val(lblFrameRate))
    Else
        .txtFrameRate = Format(Val(lblFrameRate), "#.00")
    End If
    
    Select Case .txtFrameRate
    
        Case "25"
            m_Framerate = TC_25
        Case "29.97"
            m_Framerate = TC_29
            If lblTimecode <> "" Then
                TempStr = Left(lblTimecode, 8)
                TempStr = TempStr & ";"
                TempStr = TempStr & Mid(lblTimecode, 10, 2)
                lblTimecode = TempStr
            End If
        Case "30"
            m_Framerate = TC_30
        Case "24", "23.98"
            m_Framerate = TC_24
        Case "50"
            m_Framerate = TC_50
        Case "60"
            m_Framerate = TC_60
        Case "59.94"
            m_Framerate = TC_59
            If lblTimecode <> "" Then
                TempStr = Left(lblTimecode, 8)
                TempStr = TempStr & ";"
                TempStr = TempStr & Mid(lblTimecode, 10, 2)
                lblTimecode = TempStr
            End If
        Case Else
            m_Framerate = TC_UN
    
    End Select
    
    .txtInterlace = GetAlias(lblInterlace)
    If lblFrameCount <> "" Then
        .txtDuration = Timecode_From_FrameNumber(Val(lblFrameCount), m_Framerate)
    ElseIf Val(lblSamplingRate) <> 0 Then
        lblFrameCount = Val(lblSampleCount) / Val(lblSamplingRate) * 25
        .txtDuration = Timecode_From_FrameNumber(Val(lblFrameCount), m_Framerate)
    End If
    If lblTimecodeType = "Time code" Then
        .txtTimecodestart = lblTimecode
        .txtTimecodestop = Timecode_Add(.txtTimecodestart, .txtDuration, m_Framerate)
    ElseIf lblMatroskaTimecode <> "" Then
        .txtTimecodestart = lblMatroskaTimecode
        .txtTimecodestop = Timecode_Add(.txtTimecodestart, .txtDuration, m_Framerate)
    Else
        Select Case m_Framerate
            Case TC_29, TC_59
                .txtTimecodestart = "00:00:00;00"
            Case Else
                .txtTimecodestart = "00:00:00:00"
        End Select
        .txtTimecodestop = .txtDuration
    End If
    If lblFormat <> "MPA1L3" Then
        If CLng(Val(lblDisplayAspect) * 9) <= 14 Then
            .txtAspect = "4:3"
        ElseIf CLng(Val(lblDisplayAspect) * 9) <= 16 Then
            .txtAspect = Int(Val(lblDisplayAspect) * 9) & ":9"
        ElseIf Val(lblDisplayAspect) < 2 Then
            .txtAspect = Format(Val(lblDisplayAspect), "#.00") & ":1"
        Else
            .txtAspect = Format(Val(lblDisplayAspect), "#.0") & ":1"
        End If
        If lblPixelRatio = "1.000" And lblFormat <> "AVI" Then
            .txtGeometry = "SQUARE"
        ElseIf Val(lblPixelRatio) < 1.3 Then
            .txtGeometry = "NORMAL"
        Else
            .txtGeometry = "ANAMORPHIC"
        End If
    End If
    If lblFormat = "MPEG 2" Then
        .txtFormat = GetAlias(lblFormat & " " & .txtAspect)
        .txtStreamType = GetAlias(lblGeneralFormat)
'        If Val(.txtVideoBitrate) > 45000 Then
'            If .txtAudioCodec <> "" Then
'                .txtVideoCodec = "50I VID + AUD"
'            Else
'                .txtVideoCodec = "50I VID Only"
'            End If
'        Else
            If .txtAudioCodec <> "" Then
                .txtVideoCodec = "VID + AUD"
            Else
                .txtVideoCodec = "VID Only"
            End If
'        End If
    ElseIf lblFormat = "Windows Media" Then
        .txtFormat = GetAlias(lblFormat & " " & .txtAspect)
        .txtStreamType = ""
        If .txtAudioCodec <> "" Then
            .txtVideoCodec = "VID + AUD"
        Else
            .txtVideoCodec = "VID Only"
        End If
    ElseIf lblFormat = "MPEG" Then
        .txtFormat = GetAlias(lblFormat)
        .txtStreamType = GetAlias(lblGeneralFormat)
        If .txtAudioCodec <> "" Then
            .txtVideoCodec = "VID + AUD"
        Else
            .txtVideoCodec = "VID Only"
        End If
    ElseIf lblFormat = "Wave" Then
        .txtFormat = GetAlias(lblFormat)
        .txtGeometry = ""
        .txtAspect = ""
    ElseIf lblFormat = "PDF" Then
        If UCase(Right(lp_strFilePath, 3)) = ".AI" Then
            .txtFormat = "Adobe AI File"
            .txtAspect = ""
        End If
    Else
        .txtFormat = GetAlias(lblFormat)
    End If

    If lblSeriesTitle <> "" Then
        .txtSeriesTitle = lblSeriesTitle
        .txtSeriesNumber = lblSeriesNumber
        .txtProgrammeTitle = lblProgrammeTitle
        .txtEpisodeNumber = lblEpisodeNumber
        .txtSynopsis = lblSynopsis
    End If
    
End With

End Function

Public Function GetAlias(lp_strText As String) As String

'make sure we don't get balnks
If lp_strText = "" Then

    'return a blank
    GetAlias = ""
    
    'and exit
    Exit Function
    
End If

Dim i&
For i& = 1 To Len(lp_strText)
    If Mid$(lp_strText, i&, 1) = Chr$(34) Then
        Mid$(lp_strText, i&, 1) = Chr$(39)
    End If
Next

'get the alias of some text from the system, i.e. if there is some text
'in the spreadsheet, match it to an item in the alias table
Dim l_dynAlias As ADODB.Recordset
Dim l_sql$

l_sql$ = "SELECT * FROM bbcalias WHERE typedstring = '" & QuoteSanitise(lp_strText) & "'"
'create a recordset to get the value out
Set l_dynAlias = New ADODB.Recordset
l_dynAlias.Open l_sql$, cnn, 3, 3

'check if there is a match
If l_dynAlias.RecordCount > 0 Then

    'there is a match, so use the alias instead of the spreadsheet text
    GetAlias = l_dynAlias("bbcAlias")
    
End If

'close the recordset
l_dynAlias.Close: Set l_dynAlias = Nothing

End Function


