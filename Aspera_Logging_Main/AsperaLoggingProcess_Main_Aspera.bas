Attribute VB_Name = "AsperaLoggingProcess"
Option Explicit
Public g_strCetaConnection As String
Public g_strAsperaConnection As String
Public cnn As ADODB.Connection
Public g_strUserEmailAddress As String
Public g_strUserEmailName As String
Public g_strAdministratorEmailAddress As String
Public g_strAdministratorEmailName As String
Public g_strDADCInternalEmailAddress As String
Public g_strDADCInternalEmailName As String
Public g_strDADCExternalEmailAddress As String
Public g_strDADCExternalEmailName As String
Public g_strDADCSvenskExternalEmailAddress As String
Public g_strDADCSvenskExternalEmailName As String
Public g_strSkibblyEmailAddress As String
Public g_strSkibblyEmailName As String

Public g_strManagerEmailAddress As String
Public g_strManagerEmailName As String

Const g_strSMTPServer = "jca-aspera.jcatv.co.uk"
Const g_strSMTPUserName = "" ' "aria@freenetname.co.uk"
Const g_strSMTPPassword = "" '"Chorale1"
Const g_strEmailFooter = ""
Const g_strFullUserName = "MX1 Operations"

Public l_rstLoggedEvents As ADODB.Recordset, l_lngClipID As Long, l_lngLibraryID As Long, l_strPathToFile As String, l_strEmail As String
Public SQL As String, l_strCommandLine As String, l_strResult As String, l_lngFileHandle As Long, l_curFileSize As Currency
Public l_datNow As Date, l_strCetaClientCode As String, l_rstPermission As ADODB.Recordset, l_strClipFormat As String, l_strNewPathToFile As String
Public FSO As Scripting.FileSystemObject, l_strFilename As String, rstClip As ADODB.Recordset, l_lngOldLibraryID As Long, l_strFilePath As String
Public cnn2 As New ADODB.Connection, l_rstSession As ADODB.Recordset
Public Nowdate As Date, SessionID As String, fileName As String, TempResult As String, l_rstAsperaFiles As ADODB.Recordset
Public l_strAsperaStore As String, l_strStreamStore As String, l_strFlashStore As String, timewhen As Date, Count As Long
Public MediaData As MediaInfoData, l_strNewLocation  As String
Public emailmessage As String

Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)


Public Sub RunAsperaLogging()

Dim LockFlag As Long

Set FSO = New Scripting.FileSystemObject

cnn.Open g_strCetaConnection

LockFlag = GetData("setting", "value", "name", "LockSystem")
If (LockFlag And 16) = 16 Then
    cnn.Close
    Exit Sub
End If

cnn2.Open g_strAsperaConnection

Set l_rstLoggedEvents = New ADODB.Recordset

SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailAddress';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strUserEmailAddress = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailName';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strUserEmailName = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'AdministratorEmailAddress';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strAdministratorEmailAddress = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'AdministratorEmailName';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strAdministratorEmailName = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'DADCInternalEmailAddress';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strDADCInternalEmailAddress = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'DADCInternalEmailName';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strDADCInternalEmailName = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'DADCExternalEmailAddress';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strDADCExternalEmailAddress = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'DADCExternalEmailName';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strDADCExternalEmailName = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'DADCSvenskExternalEmailAddress';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strDADCSvenskExternalEmailAddress = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'DADCSvenskExternalEmailName';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strDADCSvenskExternalEmailName = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'ManagerEmailAddress';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strManagerEmailAddress = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'ManagerEmailName';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strManagerEmailName = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'SkibblyEmailAddress';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strSkibblyEmailAddress = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'SkibblyEmailName';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strSkibblyEmailName = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close

Set l_rstAsperaFiles = New ADODB.Recordset

Nowdate = DateAdd("d", -2, Now)

'Check through Aspera Logs for Main Server Uploads

CheckMainServerUploads

'Check through Aspera Logs for P2P Uploads

CheckP2PUploads

''Check through Aspera Logs for Main Server Downloads

Nowdate = DateAdd("d", -5, Now)

CheckMainServerDownloads

''Finish off and close down.

Set l_rstLoggedEvents = Nothing

Set l_rstAsperaFiles = Nothing

cnn.Close
cnn2.Close

End Sub

Function GetAsperaData(lp_strTableName As String, lp_strFieldToReturn As String, lp_strFieldToSearch As String, lp_varCriterea As Variant, Optional lp_strFieldToSearch2 As String, Optional lp_varCriteria2 As Variant) As Variant
        '<EhHeader>
        On Error GoTo GetData_Err
        '</EhHeader>


    Dim l_strSQL As String
    l_strSQL = "SELECT " & lp_strFieldToReturn & " FROM " & lp_strTableName & " WHERE " & lp_strFieldToSearch & " = '" & (lp_varCriterea) & "'"
    If lp_strFieldToSearch2 <> "" Then
        l_strSQL = l_strSQL & " AND " & lp_strFieldToSearch2 & " = '" & (lp_varCriteria2) & "'"
    End If

    Dim l_rstGetData As New ADODB.Recordset
    Dim l_con As New ADODB.Connection

    l_con.Open g_strAsperaConnection
    l_rstGetData.Open l_strSQL, l_con, adOpenStatic, adLockReadOnly

    If Not l_rstGetData.EOF Then
    
        l_rstGetData.MoveLast
        If Not IsNull(l_rstGetData(lp_strFieldToReturn)) Then
            GetAsperaData = l_rstGetData(lp_strFieldToReturn)
            GoTo Proc_CloseDB
        End If

    Else
        GoTo Proc_CloseDB

    End If

    Select Case l_rstGetData.Fields(lp_strFieldToReturn).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
        GetAsperaData = ""
    Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
        GetAsperaData = 0
    Case adDate, adDBDate, adDBTime, adDBTimeStamp
        GetAsperaData = 0
    Case Else
        GetAsperaData = False
    End Select

Proc_CloseDB:

    On Error Resume Next

    l_rstGetData.Close
    Set l_rstGetData = Nothing

    l_con.Close
    Set l_con = Nothing


        '<EhFooter>
        Exit Function

GetData_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.GetData " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function
Function SetCetaData(lp_strTableName As String, lp_strFieldToEdit As String, lp_strFieldToSearch As String, lp_varCriterea As Variant, lp_varValue As Variant) As Variant

Dim l_strSQL As String, l_UpdateValue As String, c As ADODB.Connection

If UCase(lp_varValue) = "NULL" Or lp_varValue = "" Then
    l_UpdateValue = "NULL"
Else
    l_UpdateValue = "'" & lp_varValue & "'"
End If

l_strSQL = "UPDATE " & lp_strTableName & " SET " & lp_strFieldToEdit & " = " & l_UpdateValue & " WHERE " & lp_strFieldToSearch & " = '" & lp_varCriterea & "'"

Set c = New ADODB.Connection
c.Open g_strCetaConnection

c.Execute l_strSQL
c.Close
Set c = Nothing

End Function

Function QuoteSanitise(lp_strText) As String
        '<EhHeader>
        On Error GoTo QuoteSanitise_Err
        '</EhHeader>


        Dim l_strNewText As String

        Dim l_strOldText As String

        Dim l_intLoop    As Integer

         l_strNewText = ""
    
         If IsNull(lp_strText) Then
            l_strOldText = ""
            l_strNewText = l_strOldText
            lp_strText = ""
        Else
            l_strOldText = lp_strText
        End If
    
        If InStr(lp_strText, "'") <> 0 Then
        
            For l_intLoop = 1 To Len(l_strOldText)
            
                If Mid(l_strOldText, l_intLoop, 1) = "'" Then
                    l_strNewText = l_strNewText & "''"
                Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
                End If
            
            Next

        Else
            l_strNewText = lp_strText
        End If
    
        If Right(l_strNewText, 1) = "/" Or Right(l_strNewText, 1) = "\" Then
            l_strNewText = Left(l_strNewText, Len(l_strNewText) - 1)
        End If
    
        QuoteSanitise = l_strNewText
    

        '<EhFooter>
        Exit Function

QuoteSanitise_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.QuoteSanitise " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function

Sub SendSMTPMail(ByVal lp_strEmailToAddress As String, ByVal lp_strEmailToName As String, ByVal lp_strSubject As String, ByVal lp_strBody As String, ByVal lp_strAttachment As String, _
ByVal lp_blnDontShowErrors As Boolean, ByVal lp_strCopyToEmailAddress As String, ByVal lp_strCopyToName As String, Optional ByVal lp_strBCCAddress As String, _
Optional lp_blnNoGlobalFooter As Boolean, Optional lp_strEmailFromAddress As String)

Dim SQL As String, cnn2 As ADODB.Connection

If lp_blnNoGlobalFooter = True Then
    'Nothing
Else
    lp_strBody = lp_strBody & vbCrLf & vbCrLf & g_strEmailFooter
End If

SQL = "INSERT INTO emailmessage (emailTo, emailCC, emailBCC, emailSubject, emailBodyPlain, emailAttachmentPath, emailFrom, cuser, muser, SendViaService) VALUES ("
SQL = SQL & "'" & QuoteSanitise(lp_strEmailToAddress) & "', "
SQL = SQL & IIf(lp_strCopyToEmailAddress <> "", "'" & QuoteSanitise(lp_strCopyToEmailAddress) & "', ", "Null, ")
SQL = SQL & IIf(lp_strBCCAddress <> "", "'" & QuoteSanitise(lp_strBCCAddress) & "', ", "Null, ")
SQL = SQL & "'" & QuoteSanitise(lp_strSubject) & "', "
SQL = SQL & "'" & QuoteSanitise(lp_strBody) & "', "
SQL = SQL & IIf(lp_strAttachment <> "", "'" & QuoteSanitise(lp_strAttachment) & "', ", "Null, ")
SQL = SQL & IIf(lp_strEmailFromAddress <> "", "'" & QuoteSanitise(lp_strEmailFromAddress) & "', ", "Null, ")
SQL = SQL & "'Aspera', "
SQL = SQL & "'Aspera', "
SQL = SQL & "1);"

'On Error Resume Next
Debug.Print SQL
Set cnn2 = New ADODB.Connection
cnn2.Open g_strCetaConnection
cnn2.Execute SQL
cnn2.Close
Set cnn2 = Nothing
'On Error GoTo 0

End Sub

Function GetData(lp_strTableName As String, lp_strFieldToReturn As String, lp_strFieldToSearch As String, lp_varCriterea As Variant, Optional lp_blnReturnTrueNulls As Boolean) As Variant
        '<EhHeader>
    Dim l_strSQL As String
    
    On Error Resume Next
    l_strSQL = "SELECT " & lp_strFieldToReturn & " FROM " & lp_strTableName & " WHERE " & lp_strFieldToSearch & " = '" & (lp_varCriterea) & "'"

    Dim l_rstGetData As New ADODB.Recordset
    Dim l_con As New ADODB.Connection

    l_con.Open g_strCetaConnection
    l_rstGetData.Open l_strSQL, l_con, adOpenStatic, adLockReadOnly

    If Not l_rstGetData.EOF Then
    
        If Not IsNull(l_rstGetData(lp_strFieldToReturn)) Then
            GetData = l_rstGetData(lp_strFieldToReturn)
            GoTo Proc_CloseDB
        ElseIf lp_blnReturnTrueNulls = True Then
            GetData = Null
            GoTo Proc_CloseDB
        End If

    Else
        GoTo Proc_CloseDB

    End If

    Select Case l_rstGetData.Fields(lp_strFieldToReturn).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
        GetData = ""
    Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
        GetData = 0
    Case adDate, adDBDate, adDBTime, adDBTimeStamp
        GetData = 0
    Case Else
        GetData = False
    End Select

Proc_CloseDB:

    l_rstGetData.Close
    Set l_rstGetData = Nothing

    l_con.Close
    Set l_con = Nothing

End Function

Function SetData(lp_strTableName As String, lp_strFieldToEdit As String, lp_strFieldToSearch As String, lp_varCriterea As Variant, lp_varValue As Variant) As Variant

Dim l_strSQL As String, l_UpdateValue As String, c As ADODB.Connection

If UCase(lp_varValue) = "NULL" Or lp_varValue = "" Then
    l_UpdateValue = "NULL"
Else
    l_UpdateValue = "'" & QuoteSanitise(lp_varValue) & "'"
End If

l_strSQL = "UPDATE " & lp_strTableName & " SET " & lp_strFieldToEdit & " = " & l_UpdateValue & " WHERE " & lp_strFieldToSearch & " = '" & QuoteSanitise(lp_varCriterea) & "'"

Set c = New ADODB.Connection
c.Open g_strCetaConnection

c.Execute l_strSQL
c.Close
Set c = Nothing

End Function

Public Function FormatSQLDate(lDate As Variant, Optional lp_blnMySQL As Boolean)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If IsNull(lDate) Then
        FormatSQLDate = Null
        Exit Function
    End If
    If lDate = "" Then
        FormatSQLDate = Null
        Exit Function
    End If
    If Not IsDate(lDate) Then
        FormatSQLDate = Null
        Exit Function
    
    End If
    
    If lp_blnMySQL = True Then
        FormatSQLDate = Year(lDate) & "/" & Month(lDate) & "/" & Day(lDate) & " " & Format(Hour(lDate), "00") & ":" & Format(Minute(lDate), "00") & ":" & Format(Second(lDate), "00")
    Else
        FormatSQLDate = Month(lDate) & "/" & Day(lDate) & "/" & Year(lDate) & " " & Hour(lDate) & ":" & Minute(lDate) & ":" & Second(lDate)
    End If
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetNextSequence(lp_strSequenceName As String) As Long
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String, c As ADODB.Connection
    Dim l_lngGetNextSequence  As Long
    l_strSQL = "SELECT * FROM sequence WHERE sequencename = '" & lp_strSequenceName & "';"
    
    Set c = New ADODB.Connection
    c.Open g_strCetaConnection
    Dim l_rsSequence As ADODB.Recordset
    Set l_rsSequence = New ADODB.Recordset
    l_rsSequence.Open l_strSQL, c, 3, 3
    
    If Not l_rsSequence.EOF Then
        l_rsSequence.MoveFirst
        l_lngGetNextSequence = l_rsSequence("sequencevalue")
                
        l_strSQL = "UPDATE sequence SET sequencevalue = '" & l_lngGetNextSequence + 1 & "', muser = 'ASPERA', mdate = '" & FormatSQLDate(Now()) & "' WHERE sequencename = '" & lp_strSequenceName & "';"
        
        c.Execute l_strSQL
        
    Else
        
        Dim l_strSequenceNumber As String
PROC_Input_Number:
        l_strSequenceNumber = InputBox("There is currently no default value for a sequence of " & lp_strSequenceName & ". Please specify the initial value.", "No default value")
        If Not IsNumeric(l_strSequenceNumber) Then
            MsgBox "Please enter a valid numeric, positive integer.", vbExclamation
            GoTo PROC_Input_Number
        Else
            l_rsSequence.AddNew
            l_rsSequence("sequencename") = lp_strSequenceName
            l_rsSequence("sequencevalue") = Val(l_strSequenceNumber) + 1
            l_rsSequence("muser") = "ASPERA"
            l_rsSequence("mdate") = Now()
            l_rsSequence.Update
            l_lngGetNextSequence = Val(l_strSequenceNumber)
        End If
    End If
    
    l_rsSequence.Close
    Set l_rsSequence = Nothing
    
    c.Close
    Set c = Nothing
    GetNextSequence = l_lngGetNextSequence
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Public Function GetAlias(lp_strText As String) As String

'make sure we don't get balnks
If lp_strText = "" Then

    'return a blank
    GetAlias = ""
    
    'and exit
    Exit Function
    
End If

Dim i&
For i& = 1 To Len(lp_strText)
    If Mid$(lp_strText, i&, 1) = Chr$(34) Then
        Mid$(lp_strText, i&, 1) = Chr$(39)
    End If
Next

'get the alias of some text from the system, i.e. if there is some text
'in the spreadsheet, match it to an item in the alias table
Dim l_dynAlias As ADODB.Recordset
Dim l_sql$

l_sql$ = "SELECT * FROM bbcalias WHERE typedstring = '" & QuoteSanitise(lp_strText) & "'"
'create a recordset to get the value out
Set l_dynAlias = New ADODB.Recordset
l_dynAlias.Open l_sql$, cnn, 3, 3

'check if there is a match
If l_dynAlias.RecordCount > 0 Then

    'there is a match, so use the alias instead of the spreadsheet text
    GetAlias = l_dynAlias("bbcAlias")
    
End If

'close the recordset
l_dynAlias.Close: Set l_dynAlias = Nothing

End Function

Function FlipSlash(lp_strText) As String
        '<EhHeader>
        On Error GoTo QuoteSanitise_Err
        '</EhHeader>


        Dim l_strNewText As String

        Dim l_strOldText As String

        Dim l_intLoop    As Integer

        l_strNewText = ""
    
        If IsNull(lp_strText) Then
            l_strOldText = ""
            l_strNewText = l_strOldText
            lp_strText = ""
        Else
            l_strOldText = lp_strText
        End If
    
        If InStr(lp_strText, "/") <> 0 Then
        
            For l_intLoop = 1 To Len(l_strOldText)
            
                If Mid(l_strOldText, l_intLoop, 1) = "/" Then
                    l_strNewText = l_strNewText & "\"
                Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
                End If
            
            Next

        Else
            l_strNewText = lp_strText
        End If
    
        FlipSlash = l_strNewText

        '<EhFooter>
        Exit Function

QuoteSanitise_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.QuoteSanitise " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function

Public Sub CheckP2PUploads()

Dim l_ImageDimensions As ImgDimType, Temp As String, l_lngCounter As Long
Dim l_rstID As ADODB.Recordset, LastID As Long, AltLocation As String, l_blnWarpError As Boolean, l_blnWarpError2 As Boolean
Dim l_strDatedSubfolder As String

Set l_rstID = New ADODB.Recordset
l_blnWarpError = False

SQL = "SELECT * FROM fasp_sessions WHERE (user = 'JCA-Aspera-DADC' OR user = 'ASPERA-DADC-HDD') AND operation = 'Upload' AND status = 'completed' AND created_at > '" & FormatSQLDate(Nowdate, True) & "' ORDER BY created_at;"
l_rstLoggedEvents.Open SQL, cnn2, 3, 3
If Not l_rstLoggedEvents.EOF Then
    Debug.Print "P2P Uploads " & l_rstLoggedEvents.RecordCount & " records"
    l_lngCounter = 1
    l_rstLoggedEvents.MoveFirst
    While Not l_rstLoggedEvents.EOF
        Debug.Print l_lngCounter
        l_lngCounter = l_lngCounter + 1
        If GetData("webdelivery", "webdeliveryID", "asperatoken", l_rstLoggedEvents("session_id")) = 0 Then
            'New Upload detected
            l_rstAsperaFiles.Open "SELECT * FROM fasp_files WHERE session_id = '" & l_rstLoggedEvents("session_id") & "';", cnn2, 3, 3
            If Not l_rstAsperaFiles.EOF Then
                l_rstAsperaFiles.MoveFirst
                l_strDatedSubfolder = Format(Now, "YYYYMMDDHHNNSS")
                While Not l_rstAsperaFiles.EOF
                    fileName = l_rstAsperaFiles("file_basename")
                    If l_rstLoggedEvents("user") = "JCA-Aspera-DADC" Then
                        TempResult = Mid(l_rstAsperaFiles("file_fullpath"), 37)
                    ElseIf l_rstLoggedEvents("user") = "ASPERA-DADC-HDD" Then
                        TempResult = Mid(l_rstAsperaFiles("file_fullpath"), 18)
                    End If
                    Count = InStr(TempResult, fileName)
                    If Count > 2 Then
                        AltLocation = FlipSlash(Left(TempResult, Count - 2))
                    Else
                        AltLocation = ""
                    End If
'                    'Move the file from the landing store to the warp
'                    l_blnWarpError = False
'                    l_blnWarpError2 = False
'                    If l_rstLoggedEvents("user") = "JCA-Aspera-DADC" Then
'                        On Error GoTo RELOCATIONERROR
'                        FSO.CopyFile FlipSlash(l_rstAsperaFiles("file_fullpath")), GetData("library", "subtitle", "libraryID", 447261) & "\JCA-ASPERA-DADC\" & fileName, True
'                        On Error GoTo 0
'                        FSO.DeleteFile FlipSlash(l_rstAsperaFiles("file_fullpath")), True
'                        AltLocation = "JCA-ASPERA-DADC"
'                        GoTo CONTINUATION
'RELOCATIONERROR:
'                        SendSMTPMail g_strUserEmailAddress, g_strUserEmailName, "File Upload via Aspera Point-to-Point", fileName & " has been uploaded to WARP-OVERFLOW-2, but there was an error moving to the warp", "", True, g_strManagerEmailAddress, g_strManagerEmailName, g_strAdministratorEmailAddress
'                        l_blnWarpError = True
'                        l_blnWarpError2 = True
'                        Resume CONTINUATION
'CONTINUATION:
'                    End If
                    
                    'Then carry on and log the file in and update it's MediaInfo.
'                    If l_rstLoggedEvents("user") = "JCA-Aspera-DADC" And l_blnWarpError2 = False Then
'                        SQL = "SELECT eventID, bigfilesize, soundlay FROM events WHERE companyID = 1370 AND libraryID = 447261 AND altlocation = '" & AltLocation & "' AND clipfilename = '" & fileName & "' AND system_deleted = 0;"
'                    ElseIf l_rstLoggedEvents("user") = "JCA-Aspera-DADC" And l_blnWarpError2 = True Then
                    If l_rstLoggedEvents("user") = "JCA-Aspera-DADC" Then
                        SQL = "SELECT eventID, bigfilesize, soundlay FROM events WHERE companyID = 1370 AND libraryID = 722004 AND altlocation = '" & AltLocation & "' AND clipfilename = '" & QuoteSanitise(fileName) & "' AND system_deleted = 0;"
                    ElseIf l_rstLoggedEvents("user") = "ASPERA-DADC-HDD" Then
                        SQL = "SELECT eventID, bigfilesize, soundlay FROM events WHERE companyID = 1418 AND libraryID = 609337 AND altlocation = '" & AltLocation & "' AND clipfilename = '" & QuoteSanitise(fileName) & "' AND system_deleted = 0;"
                    End If
                    l_rstID.Open SQL, cnn, 3, 3
                    If l_rstID.EOF Then
                        l_rstID.Close
                        SQL = "INSERT INTO events (eventmediatype, libraryID, altlocation, clipfilename, companyID, companyname, eventtype, fileversion, clipsoundformat, clipreference, internalreference, cdate, cuser, mdate, muser, soundlay, bigfilesize) VALUES ("
                        SQL = SQL & "'FILE', "
'                        If l_blnWarpError = False And l_rstLoggedEvents("user") = "JCA-Aspera-DADC" Then
'                            SQL = SQL & "447261, "
'                        ElseIf l_rstLoggedEvents("user") = "ASPERA-DADC-HDD" Then
                        If l_rstLoggedEvents("user") = "ASPERA-DADC-HDD" Then
                            SQL = SQL & "609337, "
                        Else
                            SQL = SQL & "722004, "
                        End If
                        SQL = SQL & "'" & AltLocation & "', "
                        SQL = SQL & "'" & QuoteSanitise(fileName) & "', "
                        If l_rstLoggedEvents("user") = "JCA-Aspera-DADC" Then
                            SQL = SQL & "287, "
                            SQL = SQL & "'BBC Studios Ltd', "
                            SQL = SQL & "'Original Master', "
                            SQL = SQL & "'Original Masterfile', "
                            SQL = SQL & "'UK_1_BBCWW', "
                        ElseIf l_rstLoggedEvents("user") = "ASPERA-DADC-HDD" Then
                            SQL = SQL & "1418, "
                            SQL = SQL & "'Sony DADC Ltd (BBCWW HDD)', "
                            SQL = SQL & "'Original Master', "
                            SQL = SQL & "'Original Masterfile', "
                            SQL = SQL & "'UK_1_DADCHDD', "
                        End If
                        Count = 0
                        While InStr(Mid(fileName, Count + 1), ".") > 0
                            Count = Count + InStr(Mid(fileName, Count + 1), ".")
                        Wend
                        If Count > 0 Then
                            SQL = SQL & "'" & QuoteSanitise(Left(fileName, Count - 1)) & "', "
                        Else
                            SQL = SQL & "'" & QuoteSanitise(fileName) & "', "
                        End If
                        SQL = SQL & GetNextSequence("internalreference") & ", "
                        SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                        SQL = SQL & "'ASPERA', "
                        SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                        SQL = SQL & "'ASPERA', "
                        SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                        SQL = SQL & "'" & l_rstAsperaFiles("size") & "');SELECT SCOPE_IDENTITY();"
                        
                        Set l_rstID = cnn.Execute(SQL)
                        LastID = l_rstID.NextRecordset().Fields(0).Value
                    
                        If LastID <> 0 Then
                            If l_rstLoggedEvents("user") = "ASPERA-DADC-HDD" Then
                                l_strPathToFile = GetData("library", "subtitle", "libraryID", 609337)
                            Else
                                l_strPathToFile = GetData("library", "subtitle", "libraryID", 722004)
                            End If
                            l_strPathToFile = l_strPathToFile & "\" & AltLocation
                            l_strPathToFile = l_strPathToFile & "\" & fileName
                            MediaData = GetMediaInfoOnFile(Replace(l_strPathToFile, "\\", "\\?\UNC\"))
                            If MediaData.txtFormat = "TTML" Then
                                SetData "events", "clipformat", "eventID", LastID, "ITT Subtitles"
'                            ElseIf MediaData.txtFormat = "JPEG Image" Or MediaData.txtFormat = "PNG Image" Or MediaData.txtFormat = "BMP Image" Or MediaData.txtFormat = "GIF Image" Then
'                                SetData "events", "clipformat", "eventID", LastID, MediaData.txtFormat
'                                If getImgDim(l_strPathToFile, l_ImageDimensions, Temp) = True Then
'                                    SetData "events", "clipverticalpixels", "eventID", LastID, l_ImageDimensions.height
'                                    SetData "events", "cliphorizontalpixels", "eventID", LastID, l_ImageDimensions.width
'                                End If
                            ElseIf MediaData.txtFormat <> "" Then
                                SetData "events", "clipformat", "eventID", LastID, MediaData.txtFormat
                                SetData "events", "clipaudiocodec", "eventID", LastID, MediaData.txtAudioCodec
                                SetData "events", "clipframerate", "eventID", LastID, MediaData.txtFrameRate
                                SetData "events", "cbrvbr", "eventID", LastID, MediaData.txtCBRVBR
                                SetData "events", "cliphorizontalpixels", "eventID", LastID, MediaData.txtWidth
                                SetData "events", "clipverticalpixels", "eventID", LastID, MediaData.txtHeight
                                SetData "events", "videobitrate", "eventID", LastID, MediaData.txtVideoBitrate
                                SetData "events", "interlace", "eventID", LastID, MediaData.txtInterlace
                                SetData "events", "timecodestart", "eventID", LastID, MediaData.txtTimecodestart
                                SetData "events", "timecodestop", "eventID", LastID, MediaData.txtTimecodestop
                                SetData "events", "fd_length", "eventID", LastID, MediaData.txtDuration
                                SetData "events", "audiobitrate", "eventID", LastID, MediaData.txtAudioBitrate
                                SetData "events", "trackcount", "eventID", LastID, MediaData.txtTrackCount
                                SetData "events", "channelcount", "eventID", LastID, MediaData.txtChannelCount
                                If Val(MediaData.txtAudioBitrate) + Val(MediaData.txtVideoBitrate) <> 0 Then
                                    SetData "events", "clipbitrate", "eventID", LastID, Val(MediaData.txtAudioBitrate) + Val(MediaData.txtVideoBitrate)
                                Else
                                    SetData "events", "clipbitrate", "eventID", LastID, Val(MediaData.txtOverallBitrate)
                                End If
                                SetData "events", "aspectratio", "eventID", LastID, MediaData.txtAspect
                                If MediaData.txtAspect = "4:3" Then
                                    SetData "events", "fourbythreeflag", "eventID", LastID, 1
                                Else
                                    SetData "events", "fourbythreeflag", "eventID", LastID, 0
                                End If
                                SetData "events", "geometriclinearity", "eventID", LastID, MediaData.txtGeometry
                                SetData "events", "clipcodec", "eventID", LastID, MediaData.txtVideoCodec
                                SetData "events", "colorspace", "eventID", LastID, MediaData.txtColorSpace
                                SetData "events", "chromasubsampling", "eventID", LastID, MediaData.txtChromaSubsmapling
                                If Trim(" " & GetData("events", "eventtype", "eventID", LastID)) = "" Then SetData "events", "eventtype", "eventID", LastID, GetData("xref", "format", "description", MediaData.txtVideoCodec)
                                SetData "events", "mediastreamtype", "eventID", LastID, MediaData.txtStreamType
                                If MediaData.txtSeriesTitle <> "" Then
                                    SetData "events", "eventtitle", "eventID", LastID, MediaData.txtSeriesTitle
                                    SetData "events", "eventseries", "eventID", LastID, MediaData.txtSeriesNumber
                                    SetData "events", "eventsubtitle", "eventID", LastID, MediaData.txtProgrammeTitle
                                    SetData "events", "eventepisode", "eventID", LastID, MediaData.txtEpisodeNumber
                                    SetData "events", "notes", "eventID", LastID, MediaData.txtSynopsis
                                End If
                                SetData "events", "lastmediainfoquery", "eventID", LastID, FormatSQLDate(Now)
                            Else
                                Select Case UCase(Right(fileName, 3))
                                    Case "ISO"
                                        SetData "events", "clipformat", "eventID", LastID, "DVD ISO Image"
                                    Case "PDF"
                                        SetData "events", "clipformat", "eventID", LastID, "PDF File"
                                    Case "PSD"
                                        SetData "events", "clipformat", "eventID", LastID, "PSD Still Image"
                                    Case "SCC"
                                        SetData "events", "clipformat", "eventID", LastID, "SCC File (EIA 608)"
                                    Case "TIF"
                                        SetData "events", "clipformat", "eventID", LastID, "TIF Still Image"
                                    Case "DOC", "DOCX"
                                        SetData "events", "clipformat", "eventID", LastID, "Word File"
                                    Case "XLS", "XLSX"
                                        SetData "events", "clipformat", "eventID", LastID, "XLS File"
                                    Case "XML"
                                        SetData "events", "clipformat", "eventID", LastID, "XML File"
                                    Case "RAR"
                                        SetData "events", "clipformat", "eventID", LastID, "RAR Archive"
                                    Case "ZIP"
                                        SetData "events", "clipformat", "eventID", LastID, "ZIP Archive"
                                    Case "TAR"
                                        SetData "events", "clipformat", "eventID", LastID, "TAR Archive"
                                    Case Else
                                        SetData "events", "clipformat", "eventID", LastID, "Other"
                                End Select
                            End If
                                
                            cnn.Execute "INSERT INTO eventhistory (eventID, datesaved, username, description) VALUES (" & LastID & ", getdate(), 'DADC', 'DADC Aspera Upload');"
                            
                            If l_rstLoggedEvents("user") = "ASPERA-DADC-HDD" Then
                                SQL = "SELECT datearrived, FileLocation, FileReference, ActualFileSize, ActualFileName FROM tracker_hdd_item WHERE FileReference = '" & GetData("events", "clipreference", "eventID", LastID) & "';"
                                l_rstID.Open SQL, cnn, 3, 3
                                If l_rstID.RecordCount > 0 Then
                                    l_rstID("datearrived") = FormatSQLDate(Now)
                                    l_rstID("FileLocation") = AltLocation
                                    l_rstID("FileReference") = GetData("events", "clipreference", "eventID", LastID)
                                    l_rstID("ActualFileSize") = GetData("events", "bigfilesize", "eventID", LastID)
                                    l_rstID("ActualFileName") = GetData("events", "clipfilenaame", "eventID", LastID)
                                    l_rstID.Update
                                End If
                                l_rstID.Close
                                If Left(AltLocation, 44) = "ASPERA-DADC-HDD\to_DADC_London\bbcw\bbcw_mx1" Then
                                    Sleep 60000
                                    SQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, NewLibraryID, NewAltLocation, bigfilesize, RequestDate, RequestName, Urgent) VALUES ("
                                    SQL = SQL & LastID & ", "
                                    SQL = SQL & GetData("events", "libraryID", "eventID", LastID) & ", "
                                    SQL = SQL & GetData("Event_File_Request_Type", "Event_File_Request_TypeID", "description", "Move") & ", "
                                    SQL = SQL & GetData("events", "libraryID", "eventID", LastID) & ", "
                                    SQL = SQL & "'AutoDelete30\" & AltLocation & "', "
                                    SQL = SQL & l_rstAsperaFiles("size") & ", "
                                    SQL = SQL & "Getdate(), 'Aspera', 0)"
                                    Debug.Print SQL
                                    cnn.Execute SQL
                                End If
                            End If
                        End If
                    Else
                        LastID = l_rstID("eventID")
                        l_rstID("bigfilesize") = Trim(" " & l_rstAsperaFiles("size"))
                        l_rstID("soundlay") = FormatSQLDate(Now)
                        l_rstID.Update
                        l_rstID.Close
                    End If
                        
                    
                    SQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipid, accesstype, finalstatus, asperatoken, bigfilesize, processafterupload) VALUES ("
                    If l_rstLoggedEvents("user") = "JCA-Aspera-DADC" Then
                        SQL = SQL & "287, "
                        SQL = SQL & "1370, "
                    ElseIf l_rstLoggedEvents("user") = "ASPERA-DADC-HDD" Then
                        SQL = SQL & "2753, "
                        SQL = SQL & "1418, "
                    End If
                    SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                    SQL = SQL & LastID & ", "
                    SQL = SQL & "'Aspera Upload', "
                    SQL = SQL & "'Completed', "
                    SQL = SQL & "'" & l_rstLoggedEvents("session_id") & "', "
                    SQL = SQL & "'" & l_rstAsperaFiles("size") & "', "
                    SQL = SQL & "0);"
                    
                    cnn.Execute SQL
                    
                    If l_rstLoggedEvents("user") = "JCA-Aspera-DADC" Then
                        SendSMTPMail g_strDADCInternalEmailAddress, g_strDADCInternalEmailName, "DADC Upload via Aspera Point-to-Point", fileName & " has been uploaded and is being moved to BFS-CETA", "", True, g_strDADCExternalEmailAddress, g_strDADCExternalEmailName, g_strAdministratorEmailAddress
                        SQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, bigfilesize, RequestName, RequesterEmail) VALUES ("
                        SQL = SQL & LastID & ", "
                        SQL = SQL & "722004, "
                        SQL = SQL & GetData("event_file_request_type", "event_file_Request_typeID", "description", "Move") & ", "
                        SQL = SQL & "719952, "
                        SQL = SQL & "'287\ASPERA-FROM-DADC\" & l_strDatedSubfolder & "', "
                        SQL = SQL & l_rstAsperaFiles("Size") & ", "
                        SQL = SQL & "'AsperaP2P', NULL);"
                        Debug.Print SQL
                        cnn.Execute SQL
                    ElseIf l_rstLoggedEvents("user") = "ASPERA-DADC-HDD" Then
                        SendSMTPMail g_strDADCInternalEmailAddress, g_strDADCInternalEmailName, "DADC Upload via Aspera Point-to-Point", fileName & " has been uploaded and is on LAT-CETA", "", True, g_strDADCExternalEmailAddress, g_strDADCExternalEmailName, g_strAdministratorEmailAddress
                    End If
                    l_rstAsperaFiles.MoveNext
                Wend
            End If
            l_rstAsperaFiles.Close
        End If
        l_rstLoggedEvents.MoveNext
    Wend
End If
l_rstLoggedEvents.Close
Set l_rstID = Nothing

End Sub

Public Sub CheckMainServerUploads()

Dim l_ImageDimensions As ImgDimType, Temp As String, BBCMG_OrderID As Long, BBCMG_MediaKey As String
Dim l_rstID As ADODB.Recordset, l_rstAsperaUploadAccount As ADODB.Recordset, LastID As Long, AltLocation As String, NewAltlocation As String, l_blnWarpError As Boolean, l_blnWarpError2 As Boolean, l_lngCounter As Long
Dim l_strDatedSubfolder As String
Dim TranscodeSpecID As Long, WorkflowVariantID As Long

Set l_rstID = New ADODB.Recordset
Set l_rstAsperaUploadAccount = New ADODB.Recordset

l_rstAsperaUploadAccount.Open "SELECT * FROM AsperaUploadAccount WHERE AccountActive <> 0 AND Arrival_System = 'AsperaP2P';", cnn, 3, 3

If l_rstAsperaUploadAccount.RecordCount > 0 Then

    l_rstAsperaUploadAccount.MoveFirst
    Do While Not l_rstAsperaUploadAccount.EOF
    
        SQL = "SELECT * FROM fasp_sessions WHERE (user = '" & l_rstAsperaUploadAccount("AsperaUserName") & "') AND operation = 'Upload' AND (status = 'completed' or status = 'running' or status = 'error' or status = 'cancelled') AND created_at > '" & FormatSQLDate(Nowdate, True) & "' ORDER BY created_at;"
        
        l_rstLoggedEvents.Open SQL, cnn2, 3, 3
        If Not l_rstLoggedEvents.EOF Then
            l_lngCounter = 0
            l_rstLoggedEvents.MoveFirst
            Do While Not l_rstLoggedEvents.EOF
                l_lngCounter = l_lngCounter + 1
                l_rstLoggedEvents.MoveNext
            Loop
            l_rstLoggedEvents.MoveFirst
            Debug.Print "Aspera Main Server Uploads Delivery " & l_lngCounter & " records"
            l_lngCounter = 1
            l_rstLoggedEvents.MoveFirst
            Do While Not l_rstLoggedEvents.EOF
                Debug.Print l_lngCounter
                l_lngCounter = l_lngCounter + 1
'                If GetData("webdelivery", "webdeliveryID", "asperatoken", l_rstLoggedEvents("session_id")) = 0 Then
                    'New Upload detected
                    l_rstAsperaFiles.Open "SELECT * FROM fasp_files WHERE session_id = '" & l_rstLoggedEvents("session_id") & "' AND status = 'completed';", cnn2, 3, 3
                    If Not l_rstAsperaFiles.EOF Then
                        l_rstAsperaFiles.MoveFirst
                        l_strDatedSubfolder = Format(Now, "YYYYMMDDHHNNSS")
                        Do While Not l_rstAsperaFiles.EOF
                            If FSO.FileExists(Replace(FlipSlash(l_rstAsperaFiles("file_fullpath")), "\\", "\\?\UNC\")) Then
                                fileName = l_rstAsperaFiles("file_basename")
                                TempResult = Mid(l_rstAsperaFiles("file_fullpath"), l_rstAsperaUploadAccount("OffsetCounter"))
                                Count = InStr(TempResult, fileName)
                                If Count > 2 Then
                                    AltLocation = FlipSlash(Left(TempResult, Count - 2))
                                Else
                                    AltLocation = ""
                                End If
                                
                                'Then carry on and log the file in and update it's MediaInfo.
                                SQL = "SELECT eventID, bigfilesize, soundlay FROM events WHERE companyID = " & l_rstAsperaUploadAccount("companyID") & " AND libraryID = " & l_rstAsperaUploadAccount("libraryID") & " AND altlocation = '" & QuoteSanitise(AltLocation) & "' AND clipfilename = '" & QuoteSanitise(fileName) & "' AND system_deleted = 0;"
                                l_rstID.Open SQL, cnn, 3, 3
                                If l_rstID.EOF Then
                                    l_rstID.Close
                                    SQL = "INSERT INTO events (eventmediatype, libraryID, altlocation, clipfilename, companyID, companyname, eventtype, fileversion, clipsoundformat, clipreference, internalreference, cdate, cuser, mdate, muser, soundlay, bigfilesize) VALUES ("
                                    SQL = SQL & "'FILE', "
                                    SQL = SQL & l_rstAsperaUploadAccount("libraryID") & ", "
                                    SQL = SQL & "'" & QuoteSanitise(AltLocation) & "', "
                                    SQL = SQL & "'" & QuoteSanitise(fileName) & "', "
                                    SQL = SQL & l_rstAsperaUploadAccount("companyID") & ", "
                                    SQL = SQL & "'" & QuoteSanitise(l_rstAsperaUploadAccount("CompanyName")) & "', "
                                    SQL = SQL & "'Original Master', "
                                    SQL = SQL & "'Original Masterfile', "
                                    SQL = SQL & "'" & l_rstAsperaUploadAccount("DIVA_Category") & "', "
                                    Count = 0
                                    While InStr(Mid(fileName, Count + 1), ".") > 0
                                        Count = Count + InStr(Mid(fileName, Count + 1), ".")
                                    Wend
                                    If Count > 0 Then
                                        SQL = SQL & "'" & QuoteSanitise(Left(fileName, Count - 1)) & "', "
                                    Else
                                        SQL = SQL & "'" & QuoteSanitise(fileName) & "', "
                                    End If
                                    SQL = SQL & GetNextSequence("internalreference") & ", "
                                    SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                                    SQL = SQL & "'ASPERA', "
                                    SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                                    SQL = SQL & "'ASPERA', "
                                    SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                                    SQL = SQL & "'" & l_rstAsperaFiles("size") & "');SELECT SCOPE_IDENTITY();"
                                    
                                    Debug.Print SQL
                                    Set l_rstID = cnn.Execute(SQL)
                                    LastID = l_rstID.NextRecordset().Fields(0).Value
                                Else
                                    LastID = l_rstID("eventID")
                                    l_rstID("bigfilesize") = Trim(" " & l_rstAsperaFiles("size"))
                                    l_rstID("soundlay") = FormatSQLDate(Now)
                                    l_rstID.Update
                                    l_rstID.Close
                                End If
                                
                                If LastID <> 0 Then
                                    l_strPathToFile = GetData("library", "subtitle", "libraryID", l_rstAsperaUploadAccount("libraryID"))
                                    l_strPathToFile = l_strPathToFile & "\" & AltLocation
                                    l_strPathToFile = l_strPathToFile & "\" & fileName
                                    MediaData = GetMediaInfoOnFile(Replace(l_strPathToFile, "\\", "\\?\UNC\"))
                                    If MediaData.txtFormat = "TTML" Then
                                        SetData "events", "clipformat", "eventID", LastID, "ITT Subtitles"
                                    ElseIf MediaData.txtFormat <> "" Then
                                        SetData "events", "clipformat", "eventID", LastID, MediaData.txtFormat
                                        SetData "events", "clipaudiocodec", "eventID", LastID, MediaData.txtAudioCodec
                                        SetData "events", "clipframerate", "eventID", LastID, MediaData.txtFrameRate
                                        SetData "events", "cbrvbr", "eventID", LastID, MediaData.txtCBRVBR
                                        SetData "events", "cliphorizontalpixels", "eventID", LastID, MediaData.txtWidth
                                        SetData "events", "clipverticalpixels", "eventID", LastID, MediaData.txtHeight
                                        SetData "events", "videobitrate", "eventID", LastID, MediaData.txtVideoBitrate
                                        SetData "events", "interlace", "eventID", LastID, MediaData.txtInterlace
                                        SetData "events", "timecodestart", "eventID", LastID, MediaData.txtTimecodestart
                                        SetData "events", "timecodestop", "eventID", LastID, MediaData.txtTimecodestop
                                        SetData "events", "fd_length", "eventID", LastID, MediaData.txtDuration
                                        SetData "events", "audiobitrate", "eventID", LastID, MediaData.txtAudioBitrate
                                        SetData "events", "trackcount", "eventID", LastID, MediaData.txtTrackCount
                                        SetData "events", "channelcount", "eventID", LastID, MediaData.txtChannelCount
                                        If Val(MediaData.txtAudioBitrate) + Val(MediaData.txtVideoBitrate) <> 0 Then
                                            SetData "events", "clipbitrate", "eventID", LastID, Val(MediaData.txtAudioBitrate) + Val(MediaData.txtVideoBitrate)
                                        Else
                                            SetData "events", "clipbitrate", "eventID", LastID, Val(MediaData.txtOverallBitrate)
                                        End If
                                        SetData "events", "aspectratio", "eventID", LastID, MediaData.txtAspect
                                        If MediaData.txtAspect = "4:3" Then
                                            SetData "events", "fourbythreeflag", "eventID", LastID, 1
                                        Else
                                            SetData "events", "fourbythreeflag", "eventID", LastID, 0
                                        End If
                                        SetData "events", "geometriclinearity", "eventID", LastID, MediaData.txtGeometry
                                        SetData "events", "clipcodec", "eventID", LastID, MediaData.txtVideoCodec
                                        SetData "events", "colorspace", "eventID", LastID, MediaData.txtColorSpace
                                        SetData "events", "chromasubsampling", "eventID", LastID, MediaData.txtChromaSubsmapling
                                        If Trim(" " & GetData("events", "eventtype", "eventID", LastID)) = "" Then SetData "events", "eventtype", "eventID", LastID, GetData("xref", "format", "description", MediaData.txtVideoCodec)
                                        SetData "events", "mediastreamtype", "eventID", LastID, MediaData.txtStreamType
                                        If MediaData.txtSeriesTitle <> "" Then
                                            SetData "events", "eventtitle", "eventID", LastID, MediaData.txtSeriesTitle
                                            SetData "events", "eventseries", "eventID", LastID, MediaData.txtSeriesNumber
                                            SetData "events", "eventsubtitle", "eventID", LastID, MediaData.txtProgrammeTitle
                                            SetData "events", "eventepisode", "eventID", LastID, MediaData.txtEpisodeNumber
                                            SetData "events", "notes", "eventID", LastID, MediaData.txtSynopsis
                                        End If
                                        SetData "events", "lastmediainfoquery", "eventID", LastID, FormatSQLDate(Now)
                                    Else
                                        Select Case UCase(Right(fileName, 3))
                                            Case "ISO"
                                                SetData "events", "clipformat", "eventID", LastID, "DVD ISO Image"
                                            Case "PDF"
                                                SetData "events", "clipformat", "eventID", LastID, "PDF File"
                                            Case "PSD"
                                                SetData "events", "clipformat", "eventID", LastID, "PSD Still Image"
                                            Case "SCC"
                                                SetData "events", "clipformat", "eventID", LastID, "SCC File (EIA 608)"
                                            Case "TIF"
                                                SetData "events", "clipformat", "eventID", LastID, "TIF Still Image"
                                            Case "DOC", "DOCX"
                                                SetData "events", "clipformat", "eventID", LastID, "Word File"
                                            Case "XLS", "XLSX"
                                                SetData "events", "clipformat", "eventID", LastID, "XLS File"
                                            Case "XML"
                                                SetData "events", "clipformat", "eventID", LastID, "XML File"
                                            Case "RAR"
                                                SetData "events", "clipformat", "eventID", LastID, "RAR Archive"
                                            Case "ZIP"
                                                SetData "events", "clipformat", "eventID", LastID, "ZIP Archive"
                                            Case "TAR"
                                                SetData "events", "clipformat", "eventID", LastID, "TAR Archive"
                                            Case Else
                                                SetData "events", "clipformat", "eventID", LastID, "Other"
                                        End Select
                                    End If
                                    cnn.Execute "INSERT INTO eventhistory (eventID, datesaved, username, description) VALUES (" & LastID & ", getdate(), '" & l_rstAsperaUploadAccount("UserName") & "', '" & l_rstAsperaUploadAccount("Description") & "');"
                                End If
                                    
                                If LastID <> 0 And (l_rstLoggedEvents("status") = "completed" Or l_rstLoggedEvents("status") = "error" Or l_rstLoggedEvents("status") = "cancelled") And GetData("webdelivery", "webdeliveryID", "clipID", LastID) = 0 Then
                                    SQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipid, accesstype, finalstatus, asperatoken, bigfilesize, processafterupload) VALUES ("
                                    SQL = SQL & "2753, "
                                    SQL = SQL & l_rstAsperaUploadAccount("companyID") & ", "
                                    SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                                    SQL = SQL & LastID & ", "
                                    SQL = SQL & "'Aspera Upload', "
                                    SQL = SQL & "'Completed', "
                                    SQL = SQL & "'" & l_rstLoggedEvents("session_id") & "', "
                                    SQL = SQL & Trim(" " & l_rstAsperaFiles("size")) & ", "
                                    SQL = SQL & "0);"
                                    cnn.Execute SQL
                                    If l_rstAsperaUploadAccount("SendEmailNotice") <> 0 Then
                                        SendSMTPMail g_strUserEmailAddress, g_strUserEmailName, l_rstAsperaUploadAccount("EmailSubjectLine"), fileName & l_rstAsperaUploadAccount("EmailBodyLine"), "", True, IIf(Not IsNull(l_rstAsperaUploadAccount("EmailCCAddress")), l_rstAsperaUploadAccount("EmailCCAddress"), ""), "", g_strAdministratorEmailAddress
                                    End If
                                    If InStr(l_rstAsperaUploadAccount("customprocessing"), "SendLionEmail") > 0 Then
                                        SendSMTPMail g_strAdministratorEmailAddress, g_strAdministratorEmailName, l_rstAsperaUploadAccount("EmailSubjectLine"), fileName & l_rstAsperaUploadAccount("EmailBodyLine"), "", True, IIf(Not IsNull(l_rstAsperaUploadAccount("EmailCCAddress")), l_rstAsperaUploadAccount("EmailCCAddress"), ""), ""
                                    End If
                                    
                                    If fileName = ".DS_Store" Or Left(fileName, 2) = "._" Or LCase(fileName = "thumbs.db") Then
                                        'Just get rid of it :-)
                                        SQL = "INSERT INTO event_file_request (eventID, sourcelibraryID, event_file_Request_typeID, NewLibraryID, RequestName, Requestdate) VALUES ("
                                        SQL = SQL & l_lngClipID & ", "
                                        SQL = SQL & GetData("events", "libraryID", "eventID", l_lngClipID) & ", "
                                        SQL = SQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Delete") & ", "
                                        SQL = SQL & GetData("events", "libraryID", "eventID", l_lngClipID) & ", "
                                        SQL = SQL & "'Aspera', Getdate()');"
                                        Debug.Print SQL
                                        cnn.Execute SQL
                                    End If
                                    
                                    If Not IsNull(l_rstAsperaUploadAccount("destination_libraryID")) And Not (fileName = ".DS_Store" Or Left(fileName, 2) = "._" Or LCase(fileName = "thumbs.db")) Then
                                        NewAltlocation = Trim(" " & l_rstAsperaUploadAccount("destination_altlocation")) & "\" & l_strDatedSubfolder
                                        If l_rstAsperaUploadAccount("Keep_Subfolders") <> 0 Then
                                            If Len(AltLocation) > Len(l_rstAsperaUploadAccount("altlocation")) Then
                                                NewAltlocation = NewAltlocation & Mid(AltLocation, Len(l_rstAsperaUploadAccount("altlocation")) + 1)
                                            End If
                                        End If
                                        If l_rstAsperaUploadAccount("destination_libraryID") <> 0 Then
                                            'There needs to be a move of uploaded file to another store, and we need to wait for it to complete
                                            If FSO.FileExists(GetData("library", "subtitle", "libraryID", l_rstAsperaUploadAccount("destination_libraryID")) & "\" & NewAltlocation & "\" & fileName) Then
                                                NewAltlocation = NewAltlocation & "_" & Format(Now, "YYYYMMDD_HHNNSS")
                                            End If
                                            Sleep 60000
                                            SQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, NewLibraryID, NewAltLocation, bigfilesize, PreserveMasterfileStatus, RequestDate, RequestName, Urgent) VALUES ("
                                            SQL = SQL & LastID & ", "
                                            SQL = SQL & GetData("events", "libraryID", "eventID", LastID) & ", "
                                            SQL = SQL & GetData("Event_File_Request_Type", "Event_File_Request_TypeID", "description", "Move") & ", "
                                            SQL = SQL & l_rstAsperaUploadAccount("destination_libraryID") & ", "
                                            SQL = SQL & "'" & NewAltlocation & "', "
                                            SQL = SQL & l_rstAsperaFiles("size") & ", "
                                            If InStr(l_rstAsperaUploadAccount("customprocessing"), "LockAfterTransfer") > 0 Then
                                                SQL = SQL & "1, "
                                            Else
                                                SQL = SQL & "0, "
                                            End If
                                            SQL = SQL & "Getdate(), 'Aspera', 0)"
                                            Debug.Print SQL
                                            cnn.Execute SQL
                                        End If
                                    End If
                                    
                                    If Not IsNull(l_rstAsperaUploadAccount("delivery_libraryID")) And Not (fileName = ".DS_Store" Or Left(fileName, 2) = "._" Or LCase(fileName = "thumbs.db")) Then
                                        NewAltlocation = Trim(" " & l_rstAsperaUploadAccount("delivery_altlocation")) & "\" & l_strDatedSubfolder
                                        If l_rstAsperaUploadAccount("Keep_Subfolders") <> 0 Then
                                            If Len(AltLocation) > Len(l_rstAsperaUploadAccount("altlocation")) Then
                                                NewAltlocation = NewAltlocation & Mid(AltLocation, Len(l_rstAsperaUploadAccount("altlocation")))
                                            End If
                                        End If
                                        If l_rstAsperaUploadAccount("delivery_libraryID") <> 0 Then
                                            'There needs to be a copy of uploaded file to another store
                                            SQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, NewLibraryID, NewAltLocation, bigfilesize, DoNotRecordCopy, RequestDate) VALUES ("
                                            SQL = SQL & LastID & ", "
                                            SQL = SQL & GetData("events", "libraryID", "eventID", LastID) & ", "
                                            SQL = SQL & GetData("Event_File_Request_Type", "Event_File_Request_TypeID", "description", "Copy") & ", "
                                            SQL = SQL & l_rstAsperaUploadAccount("delivery_libraryID") & ", "
                                            SQL = SQL & "'" & NewAltlocation & "', "
                                            SQL = SQL & l_rstAsperaFiles("size") & ", "
                                            SQL = SQL & "1, Getdate())"
                                            Debug.Print SQL
                                            cnn.Execute SQL
                                        End If
                                    End If
                                    If Not IsNull(l_rstAsperaUploadAccount("customprocessing")) And Not (fileName = ".DS_Store" Or Left(fileName, 2) = "._" Or LCase(fileName = "thumbs.db")) Then
                                        If Left(l_rstAsperaUploadAccount("customprocessing"), 13) = "AutoTranscode" Then
                                            TranscodeSpecID = Val(Mid(l_rstAsperaUploadAccount("customprocessing"), 14))
                                            SQL = "INSERT INTO TranscodeRequest (sourceClipId, TranscodeSpecID, TranscodeSystem, fps, cetauser, savedate, leaveHidden, OverwriteIfExisting, status) VALUES ("
                                            SQL = SQL & LastID & ", "
                                            SQL = SQL & TranscodeSpecID & ", "
                                            SQL = SQL & "'" & GetData("transcodespec", "transcodesystem", "transcodespecID", TranscodeSpecID) & "', "
                                            SQL = SQL & "'" & GetData("events", "clipframerate", "eventID", LastID) & "', "
                                            SQL = SQL & "'Aspera', "
                                            SQL = SQL & "'" & Format(Now, "YYYY-MM-DD HH:NN:SS") & "', "
                                            SQL = SQL & "0, 0, "
                                            SQL = SQL & GetData("transcodestatus", "transcodestatusID", "description", "Requested") & ");"
                                            Debug.Print SQL
                                            cnn.Execute SQL
                                        End If
                                        If Left(l_rstAsperaUploadAccount("customprocessing"), 15) = "WorkflowVariant" Then
                                            WorkflowVariantID = Val(Mid(l_rstAsperaUploadAccount("customprocessing"), 16))
                                            'Make a Generic Tracker Line for this file, and possibly add a Workflow Variant ID to it also.
                                            SQL = "INSERT INTO tracker_item (companyID, itemreference, itemfilename, originaleventID, WorkflowVariantID) VALUES ("
                                            SQL = SQL & l_rstAsperaUploadAccount("companyID") & ", "
                                            SQL = SQL & "'" & QuoteSanitise(GetData("events", "clipreference", "eventID", LastID)) & "', "
                                            SQL = SQL & "'" & QuoteSanitise(GetData("events", "clipfilename", "eventID", LastID)) & "', "
                                            SQL = SQL & LastID & ", "
                                            SQL = SQL & WorkflowVariantID & ");"
                                            Debug.Print SQL
                                            cnn.Execute SQL
                                        End If
                                    End If
                                End If
                            End If
                            l_rstAsperaFiles.MoveNext
                        Loop
                    End If
                    l_rstAsperaFiles.Close
'                End If
                l_rstLoggedEvents.MoveNext
            Loop
        End If
        l_rstLoggedEvents.Close
        l_rstAsperaUploadAccount.MoveNext
    Loop
End If
l_rstAsperaUploadAccount.Close
Set l_rstAsperaUploadAccount = Nothing
Set l_rstID = Nothing

End Sub

Public Sub CheckMainServerDownloads()

Dim l_strSQL As String
Dim l_rstID As ADODB.Recordset, LastID As Long, AltLocation As String, l_blnWarpError As Boolean, l_blnWarpError2 As Boolean, l_lngCounter As Long
Dim l_lngTrackerID As Long, l_strStageField As String, l_rstItemChoice As ADODB.Recordset, l_blnConclusionValue As Boolean

Set l_rstID = New ADODB.Recordset

SQL = "SELECT * FROM fasp_sessions WHERE (user = 'HAT-TRICK-NBCU' or user = 'DISNEY-ASPERA-NBCU' or user = 'SHINE-ASPERA-NBCU' or user = 'GREATPOINT-NBCU') AND operation = 'Download' AND status = 'completed' AND created_at > '" & FormatSQLDate(Nowdate, True) & "' ORDER BY created_at;"
l_rstLoggedEvents.Open SQL, cnn2, 3, 3
If Not l_rstLoggedEvents.EOF Then
    Debug.Print "Main Server Downloads " & l_rstLoggedEvents.RecordCount & " records"
    l_lngCounter = 1
    l_rstLoggedEvents.MoveFirst
    While Not l_rstLoggedEvents.EOF
        Debug.Print l_lngCounter
        l_lngCounter = l_lngCounter + 1
        If GetData("webdeliveryportal", "webdeliveryportalID", "asperatoken", l_rstLoggedEvents("session_id")) = 0 Then
            'New Upload detected
            l_rstAsperaFiles.Open "SELECT * FROM fasp_files WHERE session_id = '" & l_rstLoggedEvents("session_id") & "';", cnn2, 3, 3
            If Not l_rstAsperaFiles.EOF Then
                l_rstAsperaFiles.MoveFirst
                Do While Not l_rstAsperaFiles.EOF
                    fileName = l_rstAsperaFiles("file_basename")
'                    If l_rstLoggedEvents("user") = "DISNEY-ASPERA-NBCU" Or l_rstLoggedEvents("user") = "SHINE-ASPERA-NBCU" Then
                        TempResult = Mid(l_rstAsperaFiles("file_fullpath"), 25)
'                    End If
                    Count = InStr(TempResult, fileName)
                    If Count > 2 Then
                        AltLocation = FlipSlash(Left(TempResult, Count - 2))
                    Else
                        AltLocation = ""
                    End If
                    
                    'Then carry on and log the download.
                    SQL = "SELECT * FROM events WHERE libraryID = 284349 AND altlocation = '" & AltLocation & "' AND clipfilename = '" & QuoteSanitise(fileName) & "';"
                    l_rstID.Open SQL, cnn, 3, 3
                    If Not l_rstID.EOF Then
                    
                        l_strCetaClientCode = GetData("company", "cetaclientcode", "companyID", l_rstID("companyID"))
                        SendSMTPMail g_strUserEmailAddress, g_strUserEmailName, "Aspera Download Completed", "Clip: " & l_rstID("clipfilename") & " " & l_rstID("clipformat") & vbCrLf & "Completed: " & Now, "", True, g_strAdministratorEmailAddress, g_strAdministratorEmailName
                            
                        If l_rstLoggedEvents("user") = "DISNEY-ASPERA-NBCU" Then
                            SQL = "INSERT INTO webdeliveryportal (companyID, portaluserID, timewhen, fullname, clipid, clipdetails, accesstype, finalstatus, bigfilesize, asperatoken) VALUES ("
                            SQL = SQL & "1163, "
                            SQL = SQL & "10640, "
                            SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                            SQL = SQL & "'DISNEY-ASPERA-NBCU', "
                            SQL = SQL & l_rstID("eventID") & ", "
                            SQL = SQL & "'" & l_rstID("clipfilename") & " " & l_rstID("clipformat") & " " & l_rstID("clipbitrate") & "kbps', "
                            SQL = SQL & "'Aspera Download', "
                            SQL = SQL & "'Completed', "
                            SQL = SQL & "'" & l_rstAsperaFiles("size") & "', "
                            SQL = SQL & "'" & l_rstLoggedEvents("session_id") & "'); "
                        ElseIf l_rstLoggedEvents("user") = "HAT-TRICK-NBCU" Then
                            SQL = "INSERT INTO webdeliveryportal (companyID, portaluserID, timewhen, fullname, clipid, clipdetails, accesstype, finalstatus, bigfilesize, asperatoken) VALUES ("
                            SQL = SQL & "305, "
                            SQL = SQL & "22105, "
                            SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                            SQL = SQL & "'HAT-TRICK-NBCU', "
                            SQL = SQL & l_rstID("eventID") & ", "
                            SQL = SQL & "'" & l_rstID("clipfilename") & " " & l_rstID("clipformat") & " " & l_rstID("clipbitrate") & "kbps', "
                            SQL = SQL & "'Aspera Download', "
                            SQL = SQL & "'Completed', "
                            SQL = SQL & "'" & l_rstAsperaFiles("size") & "', "
                            SQL = SQL & "'" & l_rstLoggedEvents("session_id") & "'); "
                        ElseIf l_rstLoggedEvents("user") = "SHINE-ASPERA-NBCU" Then
                            SQL = "INSERT INTO webdeliveryportal (companyID, portaluserID, timewhen, fullname, clipid, clipdetails, accesstype, finalstatus, bigfilesize, asperatoken) VALUES ("
                            SQL = SQL & "769, "
                            SQL = SQL & "11659, "
                            SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                            SQL = SQL & "'SHINE-ASPERA-NBCU', "
                            SQL = SQL & l_rstID("eventID") & ", "
                            SQL = SQL & "'" & l_rstID("clipfilename") & " " & l_rstID("clipformat") & " " & l_rstID("clipbitrate") & "kbps', "
                            SQL = SQL & "'Aspera Download', "
                            SQL = SQL & "'Completed', "
                            SQL = SQL & "'" & l_rstAsperaFiles("size") & "', "
                            SQL = SQL & "'" & l_rstLoggedEvents("session_id") & "'); "
                        ElseIf l_rstLoggedEvents("user") = "GREATPOINT-NBCU" Then
                            SQL = "INSERT INTO webdeliveryportal (companyID, portaluserID, timewhen, fullname, clipid, clipdetails, accesstype, finalstatus, bigfilesize, asperatoken) VALUES ("
                            SQL = SQL & "1422, "
                            SQL = SQL & "17983, "
                            SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                            SQL = SQL & "'GREATPOINT-NBCU', "
                            SQL = SQL & l_rstID("eventID") & ", "
                            SQL = SQL & "'" & l_rstID("clipfilename") & " " & l_rstID("clipformat") & " " & l_rstID("clipbitrate") & "kbps', "
                            SQL = SQL & "'Aspera Download', "
                            SQL = SQL & "'Completed', "
                            SQL = SQL & "'" & l_rstAsperaFiles("size") & "', "
                            SQL = SQL & "'" & l_rstLoggedEvents("session_id") & "'); "
                        Else
                            SQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipid, accesstype, finalstatus, asperatoken, bigfilesize, processafterupload) VALUES ("
                            SQL = SQL & "2753, "
                            SQL = SQL & "501, "
                            SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                            SQL = SQL & l_rstID("eventID") & ", "
                            SQL = SQL & "'Aspera Download', "
                            SQL = SQL & "'Completed', "
                            SQL = SQL & "'" & l_rstLoggedEvents("session_id") & "', "
                            SQL = SQL & "'" & l_rstAsperaFiles("size") & "', "
                            SQL = SQL & "0);"
                        End If
                        
                        cnn.Execute SQL
                        
                        If l_rstLoggedEvents("user") = "DISNEY-ASPERA-NBCU" Then
                            Set l_rstPermission = New ADODB.Recordset
                            SQL = "SELECT * FROM portalpermission WHERE eventID = " & l_rstID("eventID") & " AND portaluserID = 10640;"
                            Debug.Print SQL
                            l_rstPermission.Open SQL, cnn, 3, 3
                            
                            If l_rstPermission.RecordCount > 0 Then
                                
                                SQL = "INSERT INTO portalpermissionexpired (eventID, portaluserID, fullname, mediareference, approval, assignedby, dateassigned, dateexpired) VALUES ("
                                SQL = SQL & l_rstID("eventID") & ", "
                                SQL = SQL & l_rstPermission("portaluserID") & ", "
                                SQL = SQL & "'" & QuoteSanitise(l_rstPermission("fullname")) & "', "
                                SQL = SQL & "'" & QuoteSanitise(l_rstPermission("mediareference")) & "', "
                                SQL = SQL & "'" & QuoteSanitise(l_rstPermission("approval")) & "', "
                                SQL = SQL & "'" & QuoteSanitise(l_rstPermission("assignedby")) & "', "
                                SQL = SQL & "'" & FormatSQLDate(l_rstPermission("dateassigned")) & "', "
                                SQL = SQL & "getdate());"
                                Debug.Print SQL
                                cnn.Execute SQL
                            
                            End If
                            l_rstPermission.Close
                                                
                            SQL = "DELETE FROM portalpermission WHERE eventID = " & l_rstID("eventID") & " AND portaluserID = 10640;"
                            Debug.Print SQL
                            cnn.Execute SQL
                            
                        ElseIf l_rstLoggedEvents("user") = "HAT-TRICK-NBCU" Then
                            Set l_rstPermission = New ADODB.Recordset
                            SQL = "SELECT * FROM portalpermission WHERE eventID = " & l_rstID("eventID") & " AND portaluserID = 22105;"
                            Debug.Print SQL
                            l_rstPermission.Open SQL, cnn, 3, 3
                            
                            If l_rstPermission.RecordCount > 0 Then
                                
                                SQL = "INSERT INTO portalpermissionexpired (eventID, portaluserID, fullname, mediareference, approval, assignedby, dateassigned, dateexpired) VALUES ("
                                SQL = SQL & l_rstID("eventID") & ", "
                                SQL = SQL & l_rstPermission("portaluserID") & ", "
                                SQL = SQL & "'" & QuoteSanitise(l_rstPermission("fullname")) & "', "
                                SQL = SQL & "'" & QuoteSanitise(l_rstPermission("mediareference")) & "', "
                                SQL = SQL & "'" & QuoteSanitise(l_rstPermission("approval")) & "', "
                                SQL = SQL & "'" & QuoteSanitise(l_rstPermission("assignedby")) & "', "
                                SQL = SQL & "'" & FormatSQLDate(l_rstPermission("dateassigned")) & "', "
                                SQL = SQL & "getdate());"
                                Debug.Print SQL
                                cnn.Execute SQL
                            
                            End If
                            l_rstPermission.Close
                                                
                            SQL = "DELETE FROM portalpermission WHERE eventID = " & l_rstID("eventID") & " AND portaluserID = 22105;"
                            Debug.Print SQL
                            cnn.Execute SQL
                            
                        ElseIf l_rstLoggedEvents("user") = "SHINE-ASPERA-NBCU" Then
                            Set l_rstPermission = New ADODB.Recordset
                            SQL = "SELECT * FROM portalpermission WHERE eventID = " & l_rstID("eventID") & " AND portaluserID = 11659;"
                            Debug.Print SQL
                            l_rstPermission.Open SQL, cnn, 3, 3
                            
                            If l_rstPermission.RecordCount > 0 Then
                                
                                SQL = "INSERT INTO portalpermissionexpired (eventID, portaluserID, fullname, mediareference, approval, assignedby, dateassigned, dateexpired) VALUES ("
                                SQL = SQL & l_rstID("eventID") & ", "
                                SQL = SQL & l_rstPermission("portaluserID") & ", "
                                SQL = SQL & "'" & QuoteSanitise(l_rstPermission("fullname")) & "', "
                                SQL = SQL & "'" & QuoteSanitise(l_rstPermission("mediareference")) & "', "
                                SQL = SQL & "'" & QuoteSanitise(l_rstPermission("approval")) & "', "
                                SQL = SQL & "'" & QuoteSanitise(l_rstPermission("assignedby")) & "', "
                                SQL = SQL & "'" & FormatSQLDate(l_rstPermission("dateassigned")) & "', "
                                SQL = SQL & "getdate());"
                                Debug.Print SQL
                                cnn.Execute SQL
                            
                            End If
                            l_rstPermission.Close
                                                
                            SQL = "DELETE FROM portalpermission WHERE eventID = " & l_rstID("eventID") & " AND portaluserID = 11659;"
                            Debug.Print SQL
                            cnn.Execute SQL
                            
                        ElseIf l_rstLoggedEvents("user") = "GREATPOINT-NBCU" Then
                            Set l_rstPermission = New ADODB.Recordset
                            SQL = "SELECT * FROM portalpermission WHERE eventID = " & l_rstID("eventID") & " AND portaluserID = 17983;"
                            Debug.Print SQL
                            l_rstPermission.Open SQL, cnn, 3, 3
                            
                            If l_rstPermission.RecordCount > 0 Then
                                
                                SQL = "INSERT INTO portalpermissionexpired (eventID, portaluserID, fullname, mediareference, approval, assignedby, dateassigned, dateexpired) VALUES ("
                                SQL = SQL & l_rstID("eventID") & ", "
                                SQL = SQL & l_rstPermission("portaluserID") & ", "
                                SQL = SQL & "'" & QuoteSanitise(l_rstPermission("fullname")) & "', "
                                SQL = SQL & "'" & QuoteSanitise(l_rstPermission("mediareference")) & "', "
                                SQL = SQL & "'" & QuoteSanitise(l_rstPermission("approval")) & "', "
                                SQL = SQL & "'" & QuoteSanitise(l_rstPermission("assignedby")) & "', "
                                SQL = SQL & "'" & FormatSQLDate(l_rstPermission("dateassigned")) & "', "
                                SQL = SQL & "getdate());"
                                Debug.Print SQL
                                cnn.Execute SQL
                            
                            End If
                            l_rstPermission.Close
                                                
                            SQL = "DELETE FROM portalpermission WHERE eventID = " & l_rstID("eventID") & " AND portaluserID = 17983;"
                            Debug.Print SQL
                            cnn.Execute SQL
                            
                        End If
                            
                        SQL = "SELECT * FROM portalpermission WHERE eventID = " & l_rstID("eventID") & ";"
                        Debug.Print SQL
                        l_rstPermission.Open SQL, cnn, 3, 3
                        
                        If l_rstPermission.RecordCount <= 0 Then
                            If GetData("distributionpermission", "distributionpermissionID", "eventID", l_rstID("eventID")) = 0 Then
                                Set rstClip = New ADODB.Recordset
                                rstClip.Open "SELECT * FROM events WHERE eventID = " & l_rstID("eventID") & ";", cnn, 3, 3
                                If rstClip.RecordCount = 1 Then
                                    l_lngOldLibraryID = rstClip("libraryID")
                                    If l_lngOldLibraryID = 284349 Or l_lngOldLibraryID = 441212 Or l_lngOldLibraryID = 623206 Or l_lngOldLibraryID = 650950 Then
'                                        rstClip("libraryID") = 307744
                                        rstClip("system_Deleted") = 1
                                        rstClip("hidefromweb") = -1
                                        rstClip.Update
                                        'Record a History event
                                        l_strSQL = "INSERT INTO eventhistory ("
                                        l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                                        l_strSQL = l_strSQL & "'" & l_rstID("eventID") & "', "
                                        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                                        l_strSQL = l_strSQL & "'ASPERA', "
                                        l_strSQL = l_strSQL & "'Clip Deleted' "
                                        l_strSQL = l_strSQL & ");"
                                        cnn.Execute l_strSQL
                                        
                                        l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, RequestName, RequesterEmail) VALUES ("
                                        l_strSQL = l_strSQL & l_rstID("eventID") & ", "
                                        l_strSQL = l_strSQL & l_lngOldLibraryID & ", "
                                        l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Delete") & ", "
                                        l_strSQL = l_strSQL & l_lngOldLibraryID & ", "
                                        l_strSQL = l_strSQL & "'Aspera_Log', Null);"
                                        Debug.Print l_strSQL
                                        cnn.Execute l_strSQL
                                        
                                        l_strEmail = "ClipID: " & l_rstID("eventID") & ", Full Filename: " & l_strFilePath
                                        SendSMTPMail g_strAdministratorEmailAddress, g_strAdministratorEmailName, "Portal Clip Permission Expiry: Clip Physically Deleted", l_strEmail, "", True, "", ""
                                        
                                    End If
                                End If
                                rstClip.Close
                                Set rstClip = Nothing
                            End If
                        End If
                    
                    End If
                    l_rstID.Close
                    
                    l_rstAsperaFiles.MoveNext
                Loop
            End If
            l_rstAsperaFiles.Close
        End If
        l_rstLoggedEvents.MoveNext
    Wend
End If
l_rstLoggedEvents.Close

SQL = "SELECT * FROM fasp_sessions WHERE (user = 'barbie_mx1') AND operation = 'Upload' AND (status = 'completed' OR status = 'inactive' OR status = 'error') AND created_at > '" & FormatSQLDate(Nowdate, True) & "' ORDER BY created_at;"
l_rstLoggedEvents.Open SQL, cnn2, 3, 3
If Not l_rstLoggedEvents.EOF Then
    l_rstLoggedEvents.MoveLast
    l_rstLoggedEvents.MoveFirst
    Debug.Print "Main Server Downloads " & l_rstLoggedEvents.RecordCount & " records"
    l_lngCounter = 1
    l_rstLoggedEvents.MoveFirst
    While Not l_rstLoggedEvents.EOF
        Debug.Print l_lngCounter
        l_lngCounter = l_lngCounter + 1
        If GetData("webdeliveryportal", "webdeliveryportalID", "asperatoken", l_rstLoggedEvents("session_id")) = 0 Then
            'New Upload detected
            l_rstAsperaFiles.Open "SELECT * FROM fasp_files WHERE session_id = '" & l_rstLoggedEvents("session_id") & "';", cnn2, 3, 3
            If Not l_rstAsperaFiles.EOF Then
                l_rstAsperaFiles.MoveFirst
                Do While Not l_rstAsperaFiles.EOF
                    If l_rstAsperaFiles("status") = "completed" Then
                        fileName = l_rstAsperaFiles("file_basename")
    '                    If l_rstLoggedEvents("user") = "DISNEY-ASPERA-NBCU" Or l_rstLoggedEvents("user") = "SHINE-ASPERA-NBCU" Then
                            TempResult = Mid(l_rstAsperaFiles("file_fullpath"), 25)
    '                    End If
                        Count = InStr(TempResult, fileName)
                        If Count > 2 Then
                            AltLocation = FlipSlash(Left(TempResult, Count - 2))
                        Else
                            AltLocation = ""
                        End If
                        
                        SQL = "SELECT * FROM events WHERE companyID = 1103 AND clipfilename = '" & QuoteSanitise(fileName) & "' AND system_deleted = 0 AND bigfilesize IS NOT NULL;"
                        l_rstID.Open SQL, cnn, 3, 3
                        If Not l_rstID.EOF Then
                        
                        'Then carry on and log the download.
                            SQL = "INSERT INTO webdeliveryportal (companyID, portaluserID, timewhen, fullname, clipid, clipdetails, accesstype, finalstatus, bigfilesize, asperatoken) VALUES ("
                            SQL = SQL & "1103, "
                            SQL = SQL & "39098, "
                            SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                            SQL = SQL & "'Mattel Inc.', "
                            SQL = SQL & l_rstID("eventID") & ", "
                            SQL = SQL & "'" & l_rstID("clipfilename") & " " & l_rstID("clipformat") & " " & l_rstID("clipbitrate") & "kbps', "
                            SQL = SQL & "'Aspera Download', "
                            SQL = SQL & "'Completed', "
                            SQL = SQL & "'" & l_rstAsperaFiles("size") & "', "
                            SQL = SQL & "'" & l_rstLoggedEvents("session_id") & "'); "
                            
                            cnn.Execute SQL
                        
                            l_lngTrackerID = Val(Trim(" " & GetDataSQL("SELECT tracker_itemID FROM tracker_item WHERE companyID = 1103 AND itemreference = '" & l_rstID("clipreference") & "'")))
                            If l_lngTrackerID <> 0 Then
                                l_strStageField = Trim(" " & GetDataSQL("SELECT itemfield FROM tracker_itemchoice WHERE companyID = 1103 AND itemheading = 'Delivered'"))
                                If l_strStageField <> "" Then
                                    SetData "tracker_item", l_strStageField, "tracker_itemID", l_lngTrackerID, Format(Now, "YYYY-MM-DD HH:NN:SS")
                                    SetData "tracker_item", "mdate", "tracker_itemID", l_lngTrackerID, Format(Now, "YYYY-MM-DD HH:NN:SS")
                                    SetData "tracker_item", "muser", "tracker_itemID", l_lngTrackerID, "Aspera"
                                    Set l_rstItemChoice = New ADODB.Recordset
                                    If Val(Trim(" " & GetDataSQL("SELECT stage1conclusion FROM tracker_itemchoice WHERE companyID = 1103 AND itemheading = 'Delivered'"))) <> 0 _
                                    Or Val(Trim(" " & GetDataSQL("SELECT stage1NOTconclusion FROM tracker_itemchoice WHERE companyID = 1103 AND itemheading = 'Delivered'"))) <> 0 Then
                                        l_blnConclusionValue = True
                                        l_rstItemChoice.Open "SELECT * FROM tracker_itemchoice WHERE companyID = 1103 AND stage1conclusion <> 0", cnn, 3, 3
                                        If l_rstItemChoice.RecordCount > 0 Then
                                            l_rstItemChoice.MoveFirst
                                            Do While Not l_rstItemChoice.EOF
                                                If IsNull(GetData("tracker_item", l_rstItemChoice("itemfield"), "tracker_itemID", l_lngTrackerID, True)) Then
                                                    l_blnConclusionValue = False
                                                End If
                                                l_rstItemChoice.MoveNext
                                            Loop
                                        End If
                                        l_rstItemChoice.Close
                                        l_rstItemChoice.Open "SELECT * FROM tracker_itemchoice WHERE companyID = 1103 AND stage1NOTconclusion <> 0", cnn, 3, 3
                                        If l_rstItemChoice.RecordCount > 0 Then
                                            l_rstItemChoice.MoveFirst
                                            Do While Not l_rstItemChoice.EOF
                                                If Not IsNull(GetData("tracker_item", l_rstItemChoice("itemfield"), "tracker_itemID", l_lngTrackerID, True)) Then
                                                    l_blnConclusionValue = False
                                                End If
                                                l_rstItemChoice.MoveNext
                                            Loop
                                        End If
                                        l_rstItemChoice.Close
                                        SetData "tracker_item", "conclusionfield1", "tracker_itemID", l_lngTrackerID, IIf(l_blnConclusionValue = True, 1, 0)
                                    End If
                                    If Val(Trim(" " & GetDataSQL("SELECT stage2conclusion FROM tracker_itemchoice WHERE companyID = 1103 AND itemheading = 'Delivered'"))) <> 0 _
                                    Or Val(Trim(" " & GetDataSQL("SELECT stage2NOTconclusion FROM tracker_itemchoice WHERE companyID = 1103 AND itemheading = 'Delivered'"))) <> 0 Then
                                        l_blnConclusionValue = True
                                        l_rstItemChoice.Open "SELECT * FROM tracker_itemchoice WHERE companyID = 1103 AND stage2conclusion <> 0", cnn, 3, 3
                                        If l_rstItemChoice.RecordCount > 0 Then
                                            l_rstItemChoice.MoveFirst
                                            Do While Not l_rstItemChoice.EOF
                                                If IsNull(GetData("tracker_item", l_rstItemChoice("itemfield"), "tracker_itemID", l_lngTrackerID, True)) Then
                                                    l_blnConclusionValue = False
                                                End If
                                                l_rstItemChoice.MoveNext
                                            Loop
                                        End If
                                        l_rstItemChoice.Close
                                        l_rstItemChoice.Open "SELECT * FROM tracker_itemchoice WHERE companyID = 1103 AND stage2NOTconclusion <> 0", cnn, 3, 3
                                        If l_rstItemChoice.RecordCount > 0 Then
                                            l_rstItemChoice.MoveFirst
                                            Do While Not l_rstItemChoice.EOF
                                                If Not IsNull(GetData("tracker_item", l_rstItemChoice("itemfield"), "tracker_itemID", l_lngTrackerID, True)) Then
                                                    l_blnConclusionValue = False
                                                End If
                                                l_rstItemChoice.MoveNext
                                            Loop
                                        End If
                                        l_rstItemChoice.Close
                                        SetData "tracker_item", "conclusionfield2", "tracker_itemID", l_lngTrackerID, IIf(l_blnConclusionValue = True, 1, 0)
                                    End If
                                    If Val(Trim(" " & GetDataSQL("SELECT stage3conclusion FROM tracker_itemchoice WHERE companyID = 1103 AND itemheading = 'Delivered'"))) <> 0 _
                                    Or Val(Trim(" " & GetDataSQL("SELECT stage3NOTconclusion FROM tracker_itemchoice WHERE companyID = 1103 AND itemheading = 'Delivered'"))) <> 0 Then
                                        l_blnConclusionValue = True
                                        l_rstItemChoice.Open "SELECT * FROM tracker_itemchoice WHERE companyID = 1103 AND stage3conclusion <> 0", cnn, 3, 3
                                        If l_rstItemChoice.RecordCount > 0 Then
                                            l_rstItemChoice.MoveFirst
                                            Do While Not l_rstItemChoice.EOF
                                                If IsNull(GetData("tracker_item", l_rstItemChoice("itemfield"), "tracker_itemID", l_lngTrackerID, True)) Then
                                                    l_blnConclusionValue = False
                                                End If
                                                l_rstItemChoice.MoveNext
                                            Loop
                                        End If
                                        l_rstItemChoice.Close
                                        l_rstItemChoice.Open "SELECT * FROM tracker_itemchoice WHERE companyID = 1103 AND stage3NOTconclusion <> 0", cnn, 3, 3
                                        If l_rstItemChoice.RecordCount > 0 Then
                                            l_rstItemChoice.MoveFirst
                                            Do While Not l_rstItemChoice.EOF
                                                If Not IsNull(GetData("tracker_item", l_rstItemChoice("itemfield"), "tracker_itemID", l_lngTrackerID, True)) Then
                                                    l_blnConclusionValue = False
                                                End If
                                                l_rstItemChoice.MoveNext
                                            Loop
                                        End If
                                        l_rstItemChoice.Close
                                        SetData "tracker_item", "conclusionfield3", "tracker_itemID", l_lngTrackerID, IIf(l_blnConclusionValue = True, 1, 0)
                                    End If
                                    If Val(Trim(" " & GetDataSQL("SELECT stage4conclusion FROM tracker_itemchoice WHERE companyID = 1103 AND itemheading = 'Delivered'"))) <> 0 _
                                    Or Val(Trim(" " & GetDataSQL("SELECT stage4NOTconclusion FROM tracker_itemchoice WHERE companyID = 1103 AND itemheading = 'Delivered'"))) <> 0 Then
                                        l_blnConclusionValue = True
                                        l_rstItemChoice.Open "SELECT * FROM tracker_itemchoice WHERE companyID = 1103 AND stage4conclusion <> 0", cnn, 3, 3
                                        If l_rstItemChoice.RecordCount > 0 Then
                                            l_rstItemChoice.MoveFirst
                                            Do While Not l_rstItemChoice.EOF
                                                If IsNull(GetData("tracker_item", l_rstItemChoice("itemfield"), "tracker_itemID", l_lngTrackerID, True)) Then
                                                    l_blnConclusionValue = False
                                                End If
                                                l_rstItemChoice.MoveNext
                                            Loop
                                        End If
                                        l_rstItemChoice.Close
                                        l_rstItemChoice.Open "SELECT * FROM tracker_itemchoice WHERE companyID = 1103 AND stage4NOTconclusion <> 0", cnn, 3, 3
                                        If l_rstItemChoice.RecordCount > 0 Then
                                            l_rstItemChoice.MoveFirst
                                            Do While Not l_rstItemChoice.EOF
                                                If Not IsNull(GetData("tracker_item", l_rstItemChoice("itemfield"), "tracker_itemID", l_lngTrackerID, True)) Then
                                                    l_blnConclusionValue = False
                                                End If
                                                l_rstItemChoice.MoveNext
                                            Loop
                                        End If
                                        l_rstItemChoice.Close
                                        SetData "tracker_item", "conclusionfield4", "tracker_itemID", l_lngTrackerID, IIf(l_blnConclusionValue = True, 1, 0)
                                    End If
                                    If Val(Trim(" " & GetDataSQL("SELECT conclusion FROM tracker_itemchoice WHERE companyID = 1103 AND itemheading = 'Delivered'"))) <> 0 Then
                                        l_blnConclusionValue = True
                                        l_rstItemChoice.Open "SELECT * FROM tracker_itemchoice WHERE companyID = 1103 AND conclusion <> 0", cnn, 3, 3
                                        If l_rstItemChoice.RecordCount > 0 Then
                                            l_rstItemChoice.MoveFirst
                                            Do While Not l_rstItemChoice.EOF
                                                If IsNull(GetData("tracker_item", l_rstItemChoice("itemfield"), "tracker_itemID", l_lngTrackerID, True)) Then
                                                    l_blnConclusionValue = False
                                                End If
                                                l_rstItemChoice.MoveNext
                                            Loop
                                        End If
                                        l_rstItemChoice.Close
                                        SetData "tracker_item", "readytobill", "tracker_itemID", l_lngTrackerID, IIf(l_blnConclusionValue = True, 1, 0)
                                    End If
                                    Set l_rstItemChoice = Nothing
                                End If
                            End If
                        End If
                        l_rstID.Close
                    End If
                    l_rstAsperaFiles.MoveNext
                Loop
            End If
            l_rstAsperaFiles.Close
        End If
        l_rstLoggedEvents.MoveNext
    Wend
End If
l_rstLoggedEvents.Close

Set l_rstID = Nothing

End Sub

Public Function CopyEventToLibraryID(lp_lngEventID As Long, lp_lngLibraryID As Long) As Long

'copy the info from one job to another
Dim l_rstOriginalEvent As ADODB.Recordset
Dim l_rstNewEvent As ADODB.Recordset, l_lngNewEventID As Long
Dim c As ADODB.Connection
Dim l_rstTemp As ADODB.Recordset, l_lngTechrevID As Long

'get the original job's details
Dim l_strSQL As String

Set c = New ADODB.Connection
c.Open g_strCetaConnection

l_strSQL = "INSERT INTO events ("
l_strSQL = l_strSQL & "eventmediatype, libraryID, companyID) VALUES ("
l_strSQL = l_strSQL & "'FILE', " & lp_lngLibraryID & ", 1);SELECT SCOPE_IDENTITY();"

Debug.Print l_strSQL
Set l_rstOriginalEvent = New ADODB.Recordset
Set l_rstOriginalEvent = c.Execute(l_strSQL)

l_lngNewEventID = l_rstOriginalEvent.NextRecordset().Fields(0).Value

l_strSQL = "SELECT * FROM events WHERE eventID = '" & lp_lngEventID & "';"
l_rstOriginalEvent.Open l_strSQL, c, adOpenDynamic, adLockBatchOptimistic

'allow adding of a new job
l_strSQL = "SELECT * FROM events WHERE eventID = '" & l_lngNewEventID & "';"
Set l_rstNewEvent = New ADODB.Recordset
l_rstNewEvent.Open l_strSQL, c, adOpenDynamic, adLockOptimistic

Dim l_intLoop As Integer
Dim l_lngConflict As Long
    
If l_rstOriginalEvent.EOF Then Exit Function

l_rstNewEvent("libraryID") = lp_lngLibraryID

For l_intLoop = 0 To l_rstOriginalEvent.Fields.Count - 1
    If l_rstOriginalEvent.Fields(l_intLoop).Name <> "libraryID" And l_rstOriginalEvent.Fields(l_intLoop).Name <> "eventID" Then
    
        If Not IsNull(l_rstOriginalEvent(l_intLoop)) Then
            l_rstNewEvent(l_intLoop) = l_rstOriginalEvent(l_intLoop)
        End If
    
    End If
Next

l_rstNewEvent("mdate") = Null
l_rstNewEvent("muser") = Null

l_rstNewEvent.Update

'MsgBox g_lngLastID, vbExclamation

'close the original record
l_rstOriginalEvent.Close

'close the new record
l_rstNewEvent.Close

Set l_rstOriginalEvent = Nothing
Set l_rstNewEvent = Nothing

CopyEventToLibraryID = l_lngNewEventID

End Function

Function GetDataSQL(lp_strSQL As String)

On Error GoTo Proc_GetData_Error

Dim l_rstGetData As New ADODB.Recordset
Dim l_conGetData As New ADODB.Connection

l_conGetData.Open g_strCetaConnection
l_rstGetData.Open lp_strSQL, l_conGetData, 3, 3

If Not l_rstGetData.EOF Then
    
    If Not IsNull(l_rstGetData(0)) Then
        GetDataSQL = l_rstGetData(0)
    Else
        Select Case l_rstGetData.Fields(0).Type
        Case adChar, adVarChar, adVarWChar, 201
            GetDataSQL = ""
        Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
            GetDataSQL = 0
        Case adDate, adDBDate, adDBTime, adDBTimeStamp
            GetDataSQL = 0
        Case Else
            GetDataSQL = False
        End Select
    End If

End If

l_rstGetData.Close
Set l_rstGetData = Nothing

l_conGetData.Close
Set l_conGetData = Nothing

Exit Function

Proc_GetData_Error:

GetDataSQL = ""

End Function


