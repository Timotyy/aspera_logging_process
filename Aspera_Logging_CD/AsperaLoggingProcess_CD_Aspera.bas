Attribute VB_Name = "AsperaLoggingProcess"
Option Explicit
Public g_strCetaConnection As String
Public g_strAsperaConnection As String
Public cnn As ADODB.Connection
Public g_strUserEmailAddress As String
Public g_strUserEmailName As String
Public g_strAdministratorEmailAddress As String
Public g_strAdministratorEmailName As String
Public g_strDADCInternalEmailAddress As String
Public g_strDADCInternalEmailName As String
Public g_strDADCExternalEmailAddress As String
Public g_strDADCExternalEmailName As String
Public g_strDADCSvenskExternalEmailAddress As String
Public g_strDADCSvenskExternalEmailName As String
Public g_strSkibblyEmailAddress As String
Public g_strSkibblyEmailName As String

Public g_strManagerEmailAddress As String
Public g_strManagerEmailName As String

Const g_strSMTPServer = "jca-aspera.jcatv.co.uk"
Const g_strSMTPUserName = "" ' "aria@freenetname.co.uk"
Const g_strSMTPPassword = "" '"Chorale1"
Const g_strEmailFooter = ""
Const g_strFullUserName = "VDMS Operations"

Public l_rstLoggedEvents As ADODB.Recordset, l_lngClipID As Long, l_lngLibraryID As Long, l_strPathToFile As String, l_strEmail As String
Public SQL As String, l_strCommandLine As String, l_strResult As String, l_lngFileHandle As Long, l_curFileSize As Currency
Public l_datNow As Date, l_strCetaClientCode As String, l_rstPermission As ADODB.Recordset, l_strClipFormat As String, l_strNewPathToFile As String
Public FSO As Scripting.FileSystemObject, l_strFilename As String, rstClip As ADODB.Recordset, l_lngOldLibraryID As Long, l_strFilePath As String
Public cnn2 As New ADODB.Connection, l_rstSession As ADODB.Recordset
Public Nowdate As Date, SessionID As String, fileName As String, TempResult As String, l_rstAsperaFiles As ADODB.Recordset
Public l_strAsperaStore As String, l_strStreamStore As String, l_strFlashStore As String, timewhen As Date, Count As Long
Public MediaData As MediaInfoData, l_strNewLocation  As String
Public emailmessage As String

Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Public Sub RunAsperaLogging()

Dim LockFlag As Long

Nowdate = DateAdd("d", -5, Now)

Set FSO = New Scripting.FileSystemObject

cnn.Open g_strCetaConnection

LockFlag = GetData("setting", "value", "name", "LockSystem")
If (LockFlag And 16) = 16 Then
    cnn.Close
    Exit Sub
End If

cnn2.Open g_strAsperaConnection

Set l_rstLoggedEvents = New ADODB.Recordset

SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailAddress';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strUserEmailAddress = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailName';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strUserEmailName = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'AdministratorEmailAddress';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strAdministratorEmailAddress = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'AdministratorEmailName';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strAdministratorEmailName = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'DADCInternalEmailAddress';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strDADCInternalEmailAddress = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'DADCInternalEmailName';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strDADCInternalEmailName = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'DADCExternalEmailAddress';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strDADCExternalEmailAddress = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'DADCExternalEmailName';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strDADCExternalEmailName = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'DADCSvenskExternalEmailAddress';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strDADCSvenskExternalEmailAddress = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'DADCSvenskExternalEmailName';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strDADCSvenskExternalEmailName = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'ManagerEmailAddress';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strManagerEmailAddress = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'ManagerEmailName';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strManagerEmailName = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'SkibblyEmailAddress';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strSkibblyEmailAddress = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close
SQL = "SELECT value FROM setting WHERE name = 'SkibblyEmailName';"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    g_strSkibblyEmailName = l_rstLoggedEvents("value")
End If
l_rstLoggedEvents.Close

Set l_rstAsperaFiles = New ADODB.Recordset

'Check through webdelivery

CheckWebDelivery

'Check through webdeliveryportal

CheckWebDeliveryPortal

'Check through jobdetailunbilled

CheckJobDetailUnbilled

'Finish off and close down.

Set l_rstLoggedEvents = Nothing

Set l_rstAsperaFiles = Nothing

cnn.Close
cnn2.Close

End Sub

Function GetAsperaData(lp_strTableName As String, lp_strFieldToReturn As String, lp_strFieldToSearch As String, lp_varCriterea As Variant, Optional lp_strFieldToSearch2 As String, Optional lp_varCriteria2 As Variant) As Variant
        '<EhHeader>
        On Error GoTo GetData_Err
        '</EhHeader>


    Dim l_strSQL As String
    l_strSQL = "SELECT " & lp_strFieldToReturn & " FROM " & lp_strTableName & " WHERE " & lp_strFieldToSearch & " = '" & (lp_varCriterea) & "'"
    If lp_strFieldToSearch2 <> "" Then
        l_strSQL = l_strSQL & " AND " & lp_strFieldToSearch2 & " = '" & (lp_varCriteria2) & "'"
    End If

    Dim l_rstGetData As New ADODB.Recordset
    Dim l_con As New ADODB.Connection

    l_con.Open g_strAsperaConnection
    l_rstGetData.Open l_strSQL, l_con, adOpenStatic, adLockReadOnly

    If Not l_rstGetData.EOF Then
    
        l_rstGetData.MoveLast
        If Not IsNull(l_rstGetData(lp_strFieldToReturn)) Then
            GetAsperaData = l_rstGetData(lp_strFieldToReturn)
            GoTo Proc_CloseDB
        End If

    Else
        GoTo Proc_CloseDB

    End If

    Select Case l_rstGetData.Fields(lp_strFieldToReturn).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
        GetAsperaData = ""
    Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
        GetAsperaData = 0
    Case adDate, adDBDate, adDBTime, adDBTimeStamp
        GetAsperaData = 0
    Case Else
        GetAsperaData = False
    End Select

Proc_CloseDB:

    On Error Resume Next

    l_rstGetData.Close
    Set l_rstGetData = Nothing

    l_con.Close
    Set l_con = Nothing


        '<EhFooter>
        Exit Function

GetData_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.GetData " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function
Function SetCetaData(lp_strTableName As String, lp_strFieldToEdit As String, lp_strFieldToSearch As String, lp_varCriterea As Variant, lp_varValue As Variant) As Variant

Dim l_strSQL As String, l_UpdateValue As String, c As ADODB.Connection

If UCase(lp_varValue) = "NULL" Or lp_varValue = "" Then
    l_UpdateValue = "NULL"
Else
    l_UpdateValue = "'" & lp_varValue & "'"
End If

l_strSQL = "UPDATE " & lp_strTableName & " SET " & lp_strFieldToEdit & " = " & l_UpdateValue & " WHERE " & lp_strFieldToSearch & " = '" & lp_varCriterea & "'"

Set c = New ADODB.Connection
c.Open g_strCetaConnection

c.Execute l_strSQL
c.Close
Set c = Nothing

End Function

Function QuoteSanitise(lp_strText) As String
        '<EhHeader>
        On Error GoTo QuoteSanitise_Err
        '</EhHeader>


        Dim l_strNewText As String

        Dim l_strOldText As String

        Dim l_intLoop    As Integer

         l_strNewText = ""
    
         If IsNull(lp_strText) Then
            l_strOldText = ""
            l_strNewText = l_strOldText
            lp_strText = ""
        Else
            l_strOldText = lp_strText
        End If
    
        If InStr(lp_strText, "'") <> 0 Then
        
            For l_intLoop = 1 To Len(l_strOldText)
            
                If Mid(l_strOldText, l_intLoop, 1) = "'" Then
                    l_strNewText = l_strNewText & "''"
                Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
                End If
            
            Next

        Else
            l_strNewText = lp_strText
        End If
    
        If Right(l_strNewText, 1) = "/" Or Right(l_strNewText, 1) = "\" Then
            l_strNewText = Left(l_strNewText, Len(l_strNewText) - 1)
        End If
    
        QuoteSanitise = l_strNewText
    

        '<EhFooter>
        Exit Function

QuoteSanitise_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.QuoteSanitise " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function

Sub SendSMTPMail(ByVal lp_strEmailToAddress As String, ByVal lp_strEmailToName As String, ByVal lp_strSubject As String, ByVal lp_strBody As String, ByVal lp_strAttachment As String, _
ByVal lp_blnDontShowErrors As Boolean, ByVal lp_strCopyToEmailAddress As String, ByVal lp_strCopyToName As String, Optional ByVal lp_strBCCAddress As String, _
Optional lp_blnNoGlobalFooter As Boolean, Optional lp_strEmailFromAddress As String)

Dim SQL As String, cnn2 As ADODB.Connection

If lp_blnNoGlobalFooter = True Then
    'Nothing
Else
    lp_strBody = lp_strBody & vbCrLf & vbCrLf & g_strEmailFooter
End If

SQL = "INSERT INTO emailmessage (emailTo, emailCC, emailBCC, emailSubject, emailBodyPlain, emailAttachmentPath, emailFrom, cuser, muser, SendViaService) VALUES ("
SQL = SQL & "'" & QuoteSanitise(lp_strEmailToAddress) & "', "
SQL = SQL & IIf(lp_strCopyToEmailAddress <> "", "'" & QuoteSanitise(lp_strCopyToEmailAddress) & "', ", "Null, ")
SQL = SQL & IIf(lp_strBCCAddress <> "", "'" & QuoteSanitise(lp_strBCCAddress) & "', ", "Null, ")
SQL = SQL & "'" & QuoteSanitise(lp_strSubject) & "', "
SQL = SQL & "'" & QuoteSanitise(lp_strBody) & "', "
SQL = SQL & IIf(lp_strAttachment <> "", "'" & QuoteSanitise(lp_strAttachment) & "', ", "Null, ")
SQL = SQL & IIf(lp_strEmailFromAddress <> "", "'" & QuoteSanitise(lp_strEmailFromAddress) & "', ", "Null, ")
SQL = SQL & "'Aspera', "
SQL = SQL & "'Aspera', "
SQL = SQL & "1);"

'On Error Resume Next
Debug.Print SQL
Set cnn2 = New ADODB.Connection
cnn2.Open g_strCetaConnection
cnn2.Execute SQL
cnn2.Close
Set cnn2 = Nothing
'On Error GoTo 0

End Sub

Function GetData(lp_strTableName As String, lp_strFieldToReturn As String, lp_strFieldToSearch As String, lp_varCriterea As Variant, Optional lp_blnReturnTrueNulls As Boolean) As Variant
    
    Dim l_strSQL As String
    
    On Error Resume Next
    l_strSQL = "SELECT " & lp_strFieldToReturn & " FROM " & lp_strTableName & " WHERE " & lp_strFieldToSearch & " = '" & (lp_varCriterea) & "'"

    Dim l_rstGetData As New ADODB.Recordset
    Dim l_con As New ADODB.Connection

    l_con.Open g_strCetaConnection
    l_rstGetData.Open l_strSQL, l_con, adOpenStatic, adLockReadOnly

    If Not l_rstGetData.EOF Then
    
        If Not IsNull(l_rstGetData(lp_strFieldToReturn)) Then
            GetData = l_rstGetData(lp_strFieldToReturn)
            GoTo Proc_CloseDB
        ElseIf lp_blnReturnTrueNulls = True Then
            GetData = Null
            GoTo Proc_CloseDB
        End If

    Else
        GoTo Proc_CloseDB

    End If

    Select Case l_rstGetData.Fields(lp_strFieldToReturn).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
        GetData = ""
    Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
        GetData = 0
    Case adDate, adDBDate, adDBTime, adDBTimeStamp
        GetData = 0
    Case Else
        GetData = False
    End Select

Proc_CloseDB:

    l_rstGetData.Close
    Set l_rstGetData = Nothing

    l_con.Close
    Set l_con = Nothing

End Function

Function SetData(lp_strTableName As String, lp_strFieldToEdit As String, lp_strFieldToSearch As String, lp_varCriterea As Variant, lp_varValue As Variant) As Variant

Dim l_strSQL As String, l_UpdateValue As String, c As ADODB.Connection

If UCase(lp_varValue) = "NULL" Or lp_varValue = "" Then
    l_UpdateValue = "NULL"
Else
    l_UpdateValue = "'" & QuoteSanitise(lp_varValue) & "'"
End If

l_strSQL = "UPDATE " & lp_strTableName & " SET " & lp_strFieldToEdit & " = " & l_UpdateValue & " WHERE " & lp_strFieldToSearch & " = '" & QuoteSanitise(lp_varCriterea) & "'"

Set c = New ADODB.Connection
c.Open g_strCetaConnection

c.Execute l_strSQL
c.Close
Set c = Nothing

End Function

Public Function FormatSQLDate(lDate As Variant, Optional lp_blnMySQL As Boolean)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If IsNull(lDate) Then
        FormatSQLDate = Null
        Exit Function
    End If
    If lDate = "" Then
        FormatSQLDate = Null
        Exit Function
    End If
    If Not IsDate(lDate) Then
        FormatSQLDate = Null
        Exit Function
    
    End If
    
    If lp_blnMySQL = True Then
        FormatSQLDate = Year(lDate) & "/" & Month(lDate) & "/" & Day(lDate) & " " & Format(Hour(lDate), "00") & ":" & Format(Minute(lDate), "00") & ":" & Format(Second(lDate), "00")
    Else
        FormatSQLDate = Month(lDate) & "/" & Day(lDate) & "/" & Year(lDate) & " " & Hour(lDate) & ":" & Minute(lDate) & ":" & Second(lDate)
    End If
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetNextSequence(lp_strSequenceName As String) As Long
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String, c As ADODB.Connection
    Dim l_lngGetNextSequence  As Long
    l_strSQL = "SELECT * FROM sequence WHERE sequencename = '" & lp_strSequenceName & "';"
    
    Set c = New ADODB.Connection
    c.Open g_strCetaConnection
    Dim l_rsSequence As ADODB.Recordset
    Set l_rsSequence = New ADODB.Recordset
    l_rsSequence.Open l_strSQL, c, 3, 3
    
    If Not l_rsSequence.EOF Then
        l_rsSequence.MoveFirst
        l_lngGetNextSequence = l_rsSequence("sequencevalue")
                
        l_strSQL = "UPDATE sequence SET sequencevalue = '" & l_lngGetNextSequence + 1 & "', muser = 'ASPERA', mdate = '" & FormatSQLDate(Now()) & "' WHERE sequencename = '" & lp_strSequenceName & "';"
        
        c.Execute l_strSQL
        
    Else
        
        Dim l_strSequenceNumber As String
PROC_Input_Number:
        l_strSequenceNumber = InputBox("There is currently no default value for a sequence of " & lp_strSequenceName & ". Please specify the initial value.", "No default value")
        If Not IsNumeric(l_strSequenceNumber) Then
            MsgBox "Please enter a valid numeric, positive integer.", vbExclamation
            GoTo PROC_Input_Number
        Else
            l_rsSequence.AddNew
            l_rsSequence("sequencename") = lp_strSequenceName
            l_rsSequence("sequencevalue") = Val(l_strSequenceNumber) + 1
            l_rsSequence("muser") = "ASPERA"
            l_rsSequence("mdate") = Now()
            l_rsSequence.Update
            l_lngGetNextSequence = Val(l_strSequenceNumber)
        End If
    End If
    
    l_rsSequence.Close
    Set l_rsSequence = Nothing
    
    c.Close
    Set c = Nothing
    GetNextSequence = l_lngGetNextSequence
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Public Function GetAlias(lp_strText As String) As String

'make sure we don't get balnks
If lp_strText = "" Then

    'return a blank
    GetAlias = ""
    
    'and exit
    Exit Function
    
End If

Dim i&
For i& = 1 To Len(lp_strText)
    If Mid$(lp_strText, i&, 1) = Chr$(34) Then
        Mid$(lp_strText, i&, 1) = Chr$(39)
    End If
Next

'get the alias of some text from the system, i.e. if there is some text
'in the spreadsheet, match it to an item in the alias table
Dim l_dynAlias As ADODB.Recordset
Dim l_sql$

l_sql$ = "SELECT * FROM bbcalias WHERE typedstring = '" & QuoteSanitise(lp_strText) & "'"
'create a recordset to get the value out
Set l_dynAlias = New ADODB.Recordset
l_dynAlias.Open l_sql$, cnn, 3, 3

'check if there is a match
If l_dynAlias.RecordCount > 0 Then

    'there is a match, so use the alias instead of the spreadsheet text
    GetAlias = l_dynAlias("bbcAlias")
    
End If

'close the recordset
l_dynAlias.Close: Set l_dynAlias = Nothing

End Function

Function FlipSlash(lp_strText) As String
        '<EhHeader>
        On Error GoTo QuoteSanitise_Err
        '</EhHeader>


        Dim l_strNewText As String

        Dim l_strOldText As String

        Dim l_intLoop    As Integer

        l_strNewText = ""
    
        If IsNull(lp_strText) Then
            l_strOldText = ""
            l_strNewText = l_strOldText
            lp_strText = ""
        Else
            l_strOldText = lp_strText
        End If
    
        If InStr(lp_strText, "/") <> 0 Then
        
            For l_intLoop = 1 To Len(l_strOldText)
            
                If Mid(l_strOldText, l_intLoop, 1) = "/" Then
                    l_strNewText = l_strNewText & "\"
                Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
                End If
            
            Next

        Else
            l_strNewText = lp_strText
        End If
    
        FlipSlash = l_strNewText

        '<EhFooter>
        Exit Function

QuoteSanitise_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.QuoteSanitise " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function

Public Sub CheckWebDelivery()

Dim l_rstSession As ADODB.Recordset, l_rsDADCTracker As ADODB.Recordset
Dim l_curFolderSize As Currency, RunningTime As Long, File_Duration As String, File_Seconds As Long
Dim l_strSQL As String
Dim l_ImageDimensions As ImgDimType, Temp As String
Dim l_rstID As ADODB.Recordset, LastID As Long, AltLocation As String, l_blnWarpError As Boolean, l_blnWarpError2 As Boolean, l_lngCounter As Long
Dim l_lngTranscodeSpecID As Long, l_strTranscodeSystem As String, l_strAutotranscodeBitrate As String, l_strAutotranscodeHoriz As String
Dim l_lngClipBitRate As Long
Dim l_blnMoveToStreamstore As Boolean, l_blnMoveToFlashstore As Boolean, l_blnAutoProxyTranscode As Boolean, l_blnAutoThumbnail As Boolean
Dim l_blnCannotMoveToStreamstore As Boolean, l_blnCannotMoveToFlashstore As Boolean
Dim l_strNewAltLocation As String, l_strNewAltLocationField As String, l_strDatedSubfolder As String, l_strAltLocation As String, l_strAltLocationTail As String
Dim l_lngWorkflowVariantID As Long

SQL = "SELECT * FROM webdelivery "
SQL = SQL & "WHERE (asperatoken IS NOT NULL AND asperatoken <> '') "
SQL = SQL & "AND finalstatus is null "
SQL = SQL & "OR (finalstatus <> 'completed' "
SQL = SQL & "AND (finalstatus <> 'cancelled' or (finalstatus = 'cancelled' and timewhen > '" & FormatSQLDate(Nowdate) & "')) "
SQL = SQL & "AND (finalstatus <> 'error' OR (finalstatus = 'error' and timewhen > '" & FormatSQLDate(Nowdate) & "')));"

l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    Debug.Print "Webdelivery " & l_rstLoggedEvents.RecordCount & " records"
    l_lngCounter = 1
    l_rstLoggedEvents.MoveFirst
    Do While Not l_rstLoggedEvents.EOF
        Debug.Print l_lngCounter
        l_lngCounter = l_lngCounter + 1
        l_blnMoveToStreamstore = False
        l_blnMoveToFlashstore = False
        l_blnAutoProxyTranscode = False
        l_blnAutoThumbnail = False
        l_blnCannotMoveToStreamstore = False
        l_blnCannotMoveToFlashstore = False
        l_strResult = GetAsperaData("fasp_sessions", "status", "token", l_rstLoggedEvents("asperatoken"))
        If l_strResult = "" Then l_strResult = GetAsperaData("fasp_sessions", "status", "cookie", l_rstLoggedEvents("asperatoken"))
        If l_strResult <> "" Then
            
            If l_strResult = "error" Then
                If l_rstLoggedEvents("accesstype") = "Aspera Download" Then
                    SessionID = GetAsperaData("fasp_sessions", "session_id", "token", l_rstLoggedEvents("asperatoken"))
                    fileName = GetData("events", "clipfilename", "eventID", l_rstLoggedEvents("clipID"))
                    TempResult = GetAsperaData("fasp_files", "status", "session_id", SessionID, "file_basename", QuoteSanitise(fileName))
                    If TempResult = "completed" Then l_strResult = TempResult
                End If
            End If
                
            l_rstLoggedEvents("finalstatus") = l_strResult
            l_rstLoggedEvents.Update
            If l_strResult = "completed" And l_rstLoggedEvents("accesstype") = "Aspera Upload" And GetData("events", "clipformat", "eventID", l_rstLoggedEvents("clipID")) <> "Uploaded Folder" Then
                l_strCetaClientCode = GetData("company", "cetaclientcode", "companyID", l_rstLoggedEvents("companyID"))
                l_lngClipID = l_rstLoggedEvents("clipID")
                l_lngLibraryID = GetData("events", "libraryID", "eventID", l_lngClipID)
                If l_lngLibraryID <> 0 Then
                    l_strPathToFile = GetData("library", "subtitle", "libraryID", l_lngLibraryID)
                    If Trim(" " & GetData("events", "altlocation", "eventID", l_lngClipID)) <> "" Then
                        l_strPathToFile = l_strPathToFile & "\" & GetData("events", "altlocation", "eventID", l_lngClipID)
                    End If
                    l_strPathToFile = l_strPathToFile & "\" & GetData("events", "clipfilename", "eventID", l_lngClipID)
                    l_strAltLocation = GetData("events", "altlocation", "eventID", l_lngClipID)
                    If Len(l_strAltLocation) > Len(Trim(l_rstLoggedEvents("companyID")) & "\") Then
                        l_strAltLocationTail = Mid(AltLocation, Len(Trim(l_rstLoggedEvents("companyID"))) + 2)
                    Else
                        l_strAltLocationTail = ""
                    End If
                    API_OpenFile Replace(l_strPathToFile, "\\", "\\?\UNC\"), l_lngFileHandle, l_curFileSize
                    API_CloseFile l_lngFileHandle
                    SetData "events", "bigfilesize", "eventID", l_lngClipID, l_curFileSize
                    SetData "events", "soundlay", "eventID", l_lngClipID, FormatSQLDate(Now)
                    l_rstLoggedEvents("bigfilesize") = l_curFileSize
                    l_rstLoggedEvents.Update
                    If InStr(l_strCetaClientCode, "/invisibleuploads") <= 0 Then
                        SetData "events", "hidefromweb", "eventID", l_lngClipID, 0
                    End If
                    If InStr(l_strCetaClientCode, "/uploadflatfee") <= 0 Then SetData "jobdetailunbilled", "quantity", "asperatoken", l_rstLoggedEvents("asperatoken"), Int(l_curFileSize / 1024 / 1024 / 1024 + 0.999)
                    If l_rstLoggedEvents("mediainfolookup") <> 0 Then
                        MediaData = GetMediaInfoOnFile(Replace(l_strPathToFile, "\\", "\\?\UNC\"))
                        If MediaData.txtFormat = "TTML" Then
                            SetData "events", "clipformat", "eventID", l_lngClipID, "ITT Subtitles"
'                        ElseIf MediaData.txtFormat = "JPEG Image" Or MediaData.txtFormat = "PNG Image" Or MediaData.txtFormat = "BMP Image" Or MediaData.txtFormat = "GIF Image" Then
'                            SetData "events", "clipformat", "eventID", l_lngClipID, MediaData.txtFormat
'                            If getImgDim(l_strPathToFile, l_ImageDimensions, Temp) = True Then
'                                SetData "events", "clipverticalpixels", "eventID", l_lngClipID, l_ImageDimensions.height
'                                SetData "events", "cliphorizontalpixels", "eventID", l_lngClipID, l_ImageDimensions.width
'                            End If
                        ElseIf MediaData.txtFormat <> "" Then
                            SetData "events", "clipformat", "eventID", l_lngClipID, MediaData.txtFormat
                            SetData "events", "clipaudiocodec", "eventID", l_lngClipID, MediaData.txtAudioCodec
                            SetData "events", "clipframerate", "eventID", l_lngClipID, MediaData.txtFrameRate
                            SetData "events", "cbrvbr", "eventID", l_lngClipID, MediaData.txtCBRVBR
                            SetData "events", "cliphorizontalpixels", "eventID", l_lngClipID, MediaData.txtWidth
                            SetData "events", "clipverticalpixels", "eventID", l_lngClipID, MediaData.txtHeight
                            SetData "events", "videobitrate", "eventID", l_lngClipID, MediaData.txtVideoBitrate
                            SetData "events", "interlace", "eventID", l_lngClipID, MediaData.txtInterlace
                            SetData "events", "timecodestart", "eventID", l_lngClipID, MediaData.txtTimecodestart
                            SetData "events", "timecodestop", "eventID", l_lngClipID, MediaData.txtTimecodestop
                            SetData "events", "fd_length", "eventID", l_lngClipID, MediaData.txtDuration
                            
                            File_Duration = Trim(" " & MediaData.txtDuration)
                            If Len(File_Duration) <> 11 Then File_Duration = "00:00:00:00"
                            RunningTime = 60 * Val(Mid(File_Duration, 1, 2))
                            RunningTime = RunningTime + Val(Mid(File_Duration, 4, 2))
                            File_Seconds = Val(Mid(File_Duration, 7, 2))
                            If File_Seconds > 29 Then RunningTime = RunningTime + 1
                            If RunningTime = 0 Then RunningTime = 1
                            'response.write " " & duration & ", " & runningtime
                            SetData "jobdetailunbilled", "runningtime", "asperatoken", l_rstLoggedEvents("asperatoken"), RunningTime

                            SetData "events", "audiobitrate", "eventID", l_lngClipID, MediaData.txtAudioBitrate
                            SetData "events", "trackcount", "eventID", l_lngClipID, MediaData.txtTrackCount
                            SetData "events", "channelcount", "eventID", l_lngClipID, MediaData.txtChannelCount
                            If Val(MediaData.txtAudioBitrate) + Val(MediaData.txtVideoBitrate) <> 0 Then
                                SetData "events", "clipbitrate", "eventID", l_lngClipID, Val(MediaData.txtAudioBitrate) + Val(MediaData.txtVideoBitrate)
                            Else
                                SetData "events", "clipbitrate", "eventID", l_lngClipID, Val(MediaData.txtOverallBitrate)
                            End If
                            SetData "events", "aspectratio", "eventID", l_lngClipID, MediaData.txtAspect
                            If MediaData.txtAspect = "4:3" Then
                                SetData "events", "fourbythreeflag", "eventID", l_lngClipID, 1
                            Else
                                SetData "events", "fourbythreeflag", "eventID", l_lngClipID, 0
                            End If
                            SetData "events", "geometriclinearity", "eventID", l_lngClipID, MediaData.txtGeometry
                            SetData "events", "clipcodec", "eventID", l_lngClipID, MediaData.txtVideoCodec
                            SetData "events", "colorspace", "eventID", l_lngClipID, MediaData.txtColorSpace
                            SetData "events", "chromasubsampling", "eventID", l_lngClipID, MediaData.txtChromaSubsmapling
                            If Trim(" " & GetData("events", "eventtype", "eventID", l_lngClipID)) = "" Then SetData "events", "eventtype", "eventID", l_lngClipID, GetData("xref", "format", "description", MediaData.txtVideoCodec)
                            SetData "events", "mediastreamtype", "eventID", l_lngClipID, MediaData.txtStreamType
                            If MediaData.txtSeriesTitle <> "" Then
                                SetData "events", "eventtitle", "eventID", l_lngClipID, MediaData.txtSeriesTitle
                                SetData "events", "eventseries", "eventID", l_lngClipID, MediaData.txtSeriesNumber
                                SetData "events", "eventsubtitle", "eventID", l_lngClipID, MediaData.txtProgrammeTitle
                                SetData "events", "eventepisode", "eventID", l_lngClipID, MediaData.txtEpisodeNumber
                                SetData "events", "notes", "eventID", l_lngClipID, MediaData.txtSynopsis
                            End If
                            SetData "events", "lastmediainfoquery", "eventID", l_lngClipID, FormatSQLDate(Now)
                        Else
                            l_strFilename = GetData("events", "clipfilename", "eventID", l_lngClipID)
                            Select Case UCase(Right(l_strFilename, 3))
                                Case "ISO"
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "DVD ISO Image"
                                Case "PDF"
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "PDF File"
                                Case "PSD"
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "PSD Still Image"
                                Case "SCC"
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "SCC File (EIA 608)"
                                Case "STL"
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "STL Subtitles"
                                Case "TIF"
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "TIF Still Image"
                                Case "DOC", "DOCX"
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "Word File"
                                Case "XLS", "XLSX"
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "XLS File"
                                Case "XML"
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "XML File"
                                Case "RAR"
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "RAR Archive"
                                Case "ZIP"
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "ZIP Archive"
                                Case "TAR"
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "TAR Archive"
                                Case Else
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "Other"
                            End Select
                        End If
                    End If
                    l_strClipFormat = GetData("events", "clipformat", "eventID", l_lngClipID)
                    l_strFilename = GetData("events", "clipfilename", "eventID", l_lngClipID)
                    l_lngClipBitRate = Val(Trim(" " & GetData("events", "clipbitrate", "eventID", l_lngClipID)))
                    If l_strFilename = ".DS_Store" Then
                        'Just get rid of it :-)
                        SQL = "INSERT INTO event_file_request (eventID, sourcelibraryID, event_file_Request_typeID, NewLibraryID, RequestName, Requestdate) VALUES ("
                        SQL = SQL & l_lngClipID & ", "
                        SQL = SQL & GetData("events", "libraryID", "eventID", l_lngClipID) & ", "
                        SQL = SQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Delete") & ", "
                        SQL = SQL & GetData("events", "libraryID", "eventID", l_lngClipID) & ", "
                        SQL = SQL & "'Aspera', Getdate()');"
                        Debug.Print SQL
                        cnn.Execute SQL
                    End If
                    If InStr(l_strCetaClientCode, "/MoveAfterUpload") > 0 And l_strFilename <> ".DS_Store" Then
                        'There needs to be a move of uploaded file to another store.
                        l_strNewAltLocation = "ceta\Ceta\" & l_rstLoggedEvents("companyID") & "\Uploaded"
                        l_strDatedSubfolder = Format(Now, "YYYYMMDDHHNNSS")
                        If InStr(l_strCetaClientCode, "/MoveAfterUploadFolder") > 0 Then
                            l_strNewAltLocationField = Mid(l_strCetaClientCode, InStr(l_strCetaClientCode, "/MoveAfterUploadFolder") + 23)
                            If InStr(l_strNewAltLocationField, "/") > 0 Then
                                l_strNewAltLocationField = Trim(" " & Left(l_strNewAltLocationField, InStr(l_strNewAltLocationField, "/") - 1))
                            End If
                            If Len(l_strNewAltLocationField) > 0 Then
                                If Left(l_strNewAltLocationField, 1) = "{" Then
                                    l_strNewAltLocationField = Mid(l_strNewAltLocationField, 2, Len(l_strNewAltLocationField) - 2)
                                    If Trim(" " & GetData("events", l_strNewAltLocationField, "eventID", l_lngClipID)) <> "" Then
                                        l_strNewAltLocation = l_strNewAltLocation & "\" & Replace(Replace(Replace(Replace(Trim(" " & GetData("events", l_strNewAltLocationField, "eventID", l_lngClipID)), " ", "_"), ":", "_"), "/", "_"), "\", "_") & "\" & l_strDatedSubfolder
                                    End If
                                Else
                                    l_strNewAltLocation = l_strNewAltLocation & "\" & l_strNewAltLocationField & "\" & l_strDatedSubfolder
                                End If
                            End If
                        End If
                        If l_strAltLocationTail <> "" Then
                            l_strNewAltLocation = l_strNewAltLocation & "\" & l_strAltLocationTail
                        End If
                        Sleep 60000
                        SQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, NewLibraryID, NewAltLocation, bigfilesize, RequestDate, RequestName, Urgent) VALUES ("
                        SQL = SQL & l_lngClipID & ", "
                        SQL = SQL & GetData("events", "libraryID", "eventID", l_lngClipID) & ", "
                        SQL = SQL & GetData("Event_File_Request_Type", "Event_File_Request_TypeID", "description", "Move") & ", "
                        SQL = SQL & Val(Mid(l_strCetaClientCode, InStr(l_strCetaClientCode, "/MoveAfterUpload") + 17)) & ", "
                        SQL = SQL & "'" & l_strNewAltLocation & "', "
                        SQL = SQL & l_curFileSize & ", "
                        SQL = SQL & "Getdate(), 'Aspera', 1)"
                        Debug.Print SQL
                        cnn.Execute SQL
                    End If
                    If InStr(l_strCetaClientCode, "/MakeTrackerEntry") > 0 And l_strFilename <> ".DS_Store" Then
                        'Make a Generic Tracker Line for this file, and possibly add a Workflow Variant ID to it also.
                        If InStr(l_strCetaClientCode, "/MakeTrackerWorkflowVariantID") > 0 Then
                            l_lngCounter = InStr(l_strCetaClientCode, "/MakeTrackerWorkflowVariantID")
                            l_lngWorkflowVariantID = Val(Mid(l_strCetaClientCode, l_lngCounter + 30))
                        Else
                            l_lngWorkflowVariantID = 0
                        End If
                        SQL = "INSERT INTO tracker_item (companyID, originaleventID, itemreference, itemfilename" & IIf(l_lngWorkflowVariantID <> 0, ", WorkflowVariantID", "") & ") VALUES ("
                        SQL = SQL & l_rstLoggedEvents("companyID") & ", "
                        SQL = SQL & l_lngClipID & ", "
                        SQL = SQL & "'" & QuoteSanitise(GetData("events", "clipreference", "eventID", l_lngClipID)) & "', "
                        SQL = SQL & "'" & QuoteSanitise(GetData("events", "clipfilename", "eventID", l_lngClipID)) & "'"
                        If l_lngWorkflowVariantID <> 0 Then
                            SQL = SQL & ", " & l_lngWorkflowVariantID
                        End If
                        SQL = SQL & ");"
                        Debug.Print SQL
                        cnn.Execute SQL
                    End If
                    If (l_rstLoggedEvents("processafterupload") = 2 Or (l_strClipFormat = "Flash MP4" And l_lngClipBitRate < 3000)) And l_lngLibraryID <> 722001 And l_strFilename <> ".DS_Store" Then
                        'Check whether there is already a file on the flashstore of this name and company, and if not, move it.
                        If Trim(" " & GetDataSQL("SELECT eventID FROM events WHERE libraryID = 276356 AND altlocation = '" & GetData("events", "altlocation", "eventID", l_lngClipID) & "' AND clipfilename = '" & l_strFilename & "' AND companyID = " & l_rstLoggedEvents("companyID") & " AND system_deleted = 0")) = "" Then
                            'Move it to the Flashstore
                            If Right(LCase(l_strFilename), 4) = ".mp4" Or Right(LCase(l_strFilename), 4) = ".flv" Or Right(LCase(l_strFilename), 4) = ".mov" Then
                                SetData "events", "clipsoundformat", "eventID", l_lngClipID, "UK_3_" & GetData("company", "marketcode", "companyID", l_rstLoggedEvents("companyID"))
                                SQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, bigfilesize, RequestName, RequesterEmail) VALUES ("
                                SQL = SQL & l_lngClipID & ", "
                                SQL = SQL & l_lngLibraryID & ", "
                                SQL = SQL & GetData("event_file_request_type", "event_file_Request_typeID", "description", "Move") & ", "
                                SQL = SQL & "722001, "
                                SQL = SQL & "'" & GetData("events", "companyID", "eventID", l_lngClipID) & "', "
                                SQL = SQL & l_curFileSize & ", "
                                SQL = SQL & "'ContentDelivery', NULL);"
                                Debug.Print SQL
                                cnn.Execute SQL
                            End If
                            l_blnMoveToFlashstore = True
                        Else
                            l_blnCannotMoveToFlashstore = True
                        End If
                    End If
                End If
                emailmessage = ""
                If l_strFilename <> ".DS_Store" Then
                    emailmessage = emailmessage & "User: " & GetData("contact", "name", "contactID", l_rstLoggedEvents("contactID")) & ", Company: " & GetData("company", "name", "companyID", l_rstLoggedEvents("companyID")) & vbCrLf
                    emailmessage = emailmessage & "Upload Completed: " & Now & vbCrLf
                    emailmessage = emailmessage & "File Delivered: " & GetData("events", "clipfilename", "eventID", l_rstLoggedEvents("clipID")) & vbCrLf
                    emailmessage = emailmessage & "Title: " & GetData("events", "eventtitle", "eventID", l_rstLoggedEvents("clipID"))
                    If GetData("events", "eventsubtitle", "eventID", l_rstLoggedEvents("clipID")) <> "" Then emailmessage = emailmessage & ", Subtitle: " & GetData("events", "eventsubtitle", "eventID", l_rstLoggedEvents("clipID"))
                    If GetData("events", "eventepisode", "eventID", l_rstLoggedEvents("clipID")) <> "" Then emailmessage = emailmessage & ", Episode: " & GetData("events", "eventepisode", "eventID", l_rstLoggedEvents("clipID")) & vbCrLf
                    If l_blnCannotMoveToStreamstore = True Then
                        emailmessage = emailmessage & vbCrLf & "This file could not be automatically moved to the Streamstore, due to a file of the same name already being there." & vbCrLf
                    ElseIf l_blnMoveToStreamstore = True Then
                        emailmessage = emailmessage & vbCrLf & "This file will be automatically made available as a playable proxy file." & vbCrLf
                    End If
                    If l_blnCannotMoveToFlashstore = True Then
                        emailmessage = emailmessage & vbCrLf & "This file could not be automatically moved to the Flashstore, due to a file of the same name already being there." & vbCrLf
                    ElseIf l_blnMoveToFlashstore = True Then
                        emailmessage = emailmessage & vbCrLf & "This file will be automatically made available as a playable proxy file." & vbCrLf
                    End If
                    If Trim(" " & GetDataSQL("SELECT event_file_requestID FROM event_file_request WHERE eventID = " & l_lngClipID & " AND event_file_request_typeID = 2 AND RequestComplete IS NOT NULL")) <> "" Then
                        emailmessage = emailmessage & vbCrLf & "This file was automatically moved to " & GetData("library", "barcode", "libraryID", Val(Mid(l_strCetaClientCode, InStr(l_strCetaClientCode, "/MoveAfterUpload") + 17))) & "." & vbCrLf
                    ElseIf Trim(" " & GetDataSQL("SELECT event_file_requestID FROM event_file_request WHERE eventID = " & l_lngClipID & " AND event_file_request_typeID = 2 AND RequestFailed IS NOT NULL")) <> "" Then
                        emailmessage = emailmessage & vbCrLf & "This file should have been moved to " & GetData("library", "barcode", "libraryID", Val(Mid(l_strCetaClientCode, InStr(l_strCetaClientCode, "/MoveAfterUpload") + 17))) & ", but the move failed." & vbCrLf
                    End If
                    Debug.Print emailmessage
                    If InStr(l_strCetaClientCode, "/emailgroupuploads") > 0 Then
                        SendSMTPMail GetData("contact", "email", "contactID", l_rstLoggedEvents("contactID")), GetData("contact", "name", "contactID", l_rstLoggedEvents("contactID")), "Aspera Upload Completed", emailmessage, "", True, g_strUserEmailAddress, g_strFullUserName
                        l_strSQL = "SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", l_rstLoggedEvents("companyID")) & "' AND trackermessageID = 39;"
                        Set l_rsDADCTracker = New ADODB.Recordset
                        l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
                        If l_rsDADCTracker.RecordCount > 0 Then
                            l_rsDADCTracker.MoveFirst
                            While Not l_rsDADCTracker.EOF
                                SendSMTPMail l_rsDADCTracker("email"), l_rsDADCTracker("fullname"), "Aspera Upload Completed", emailmessage, "", True, "", "", g_strAdministratorEmailAddress
                                l_rsDADCTracker.MoveNext
                            Wend
                        End If
                        l_rsDADCTracker.Close
                    ElseIf InStr(l_strCetaClientCode, "/emailuploads") > 0 Then
                        SendSMTPMail GetData("contact", "email", "contactID", l_rstLoggedEvents("contactID")), GetData("contact", "name", "contactID", l_rstLoggedEvents("contactID")), "Aspera Upload Completed", emailmessage, "", True, g_strUserEmailAddress, g_strFullUserName
                        If Trim(" " & GetData("company", "email", "companyID", l_rstLoggedEvents("companyID"))) <> "" Then
                            SendSMTPMail GetData("company", "email", "companyID", l_rstLoggedEvents("companyID")), "Upload Notifications List", "Aspera Upload Completed", emailmessage, "", True, GetData("company", "jcacontactemail", "companyID", l_rstLoggedEvents("companyID")), "", g_strAdministratorEmailAddress
                        End If
                    Else
                        SendSMTPMail GetData("contact", "email", "contactID", l_rstLoggedEvents("contactID")), GetData("contact", "name", "contactID", l_rstLoggedEvents("contactID")), "Aspera Upload Completed", emailmessage, "", True, g_strUserEmailAddress, g_strFullUserName, g_strAdministratorEmailAddress, False, g_strUserEmailAddress
                    End If
                End If
            ElseIf l_strResult = "completed" And l_rstLoggedEvents("accesstype") = "Aspera Upload" And GetData("events", "clipformat", "eventID", l_rstLoggedEvents("clipID")) = "Uploaded Folder" Then
                l_strCetaClientCode = GetData("company", "cetaclientcode", "companyID", l_rstLoggedEvents("companyID"))
                emailmessage = ""
                emailmessage = emailmessage & "User: " & GetData("contact", "name", "contactID", l_rstLoggedEvents("contactID")) & ", Company: " & GetData("company", "name", "companyID", l_rstLoggedEvents("companyID")) & vbCrLf
                emailmessage = emailmessage & "Upload Completed: " & Now & vbCrLf
                emailmessage = emailmessage & "Folder Delivered: " & GetData("events", "clipfilename", "eventID", l_rstLoggedEvents("clipID")) & vbCrLf
                emailmessage = emailmessage & "Title: " & GetData("events", "eventtitle", "eventID", l_rstLoggedEvents("clipID"))
                If GetData("events", "eventsubtitle", "eventID", l_rstLoggedEvents("clipID")) <> "" Then emailmessage = emailmessage & ", Subtitle: " & GetData("events", "eventsubtitle", "eventID", l_rstLoggedEvents("clipID"))
                If GetData("events", "eventepisode", "eventID", l_rstLoggedEvents("clipID")) <> "" Then emailmessage = emailmessage & ", Episode: " & GetData("events", "eventepisode", "eventID", l_rstLoggedEvents("clipID")) & vbCrLf
                l_lngClipID = l_rstLoggedEvents("clipID")
                If InStr(l_strCetaClientCode, "/showuploadedfolders") > 0 Then
                    SetData "events", "clipformat", "eventID", l_lngClipID, "Folder"
                    SetData "events", "hidefromweb", "eventID", l_lngClipID, 0
                End If
                l_lngLibraryID = GetData("events", "libraryID", "eventID", l_lngClipID)
                If l_lngLibraryID <> 0 Then
                    SQL = "SELECT * FROM fasp_sessions WHERE cookie = '" & l_rstLoggedEvents("asperatoken") & "' OR token = '" & l_rstLoggedEvents("asperatoken") & "' ORDER BY created_at;"
                    Set l_rstSession = New ADODB.Recordset
                    l_rstSession.Open SQL, cnn2, 3, 3
                    If Not l_rstSession.EOF Then
                        l_rstSession.MoveFirst
                        l_curFolderSize = 0
                        Do While Not l_rstSession.EOF
                            If GetData("webdelivery", "webdeliveryID", "asperatoken", l_rstSession("session_id")) = 0 Then
                                'New Upload detected
                                l_rstAsperaFiles.Open "SELECT * FROM fasp_files WHERE session_id = '" & l_rstSession("session_id") & "';", cnn2, 3, 3
                                If Not l_rstAsperaFiles.EOF Then
                                    l_rstAsperaFiles.MoveFirst
                                    l_strDatedSubfolder = Format(Now, "YYYYMMDDHHNNSS")
                                    Do While Not l_rstAsperaFiles.EOF
                                        fileName = l_rstAsperaFiles("file_basename")
                                        If GetData("events", "libraryID", "eventID", l_rstLoggedEvents("clipID")) = 447261 Then
                                            TempResult = Mid(l_rstAsperaFiles("file_fullpath"), 15)
                                        Else
                                            TempResult = Mid(l_rstAsperaFiles("file_fullpath"), 37)
                                        End If
                                        Count = InStr(TempResult, fileName)
                                        If Count > 2 Then
                                            AltLocation = FlipSlash(Left(TempResult, Count - 2))
                                        Else
                                            AltLocation = ""
                                        End If
                                        If Len(AltLocation) > Len(Trim(l_rstLoggedEvents("companyID")) & "\") Then
                                            l_strAltLocationTail = Mid(AltLocation, Len(Trim(l_rstLoggedEvents("companyID"))) + 2)
                                        Else
                                            l_strAltLocationTail = ""
                                        End If
                                        
                                        'Then carry on and log the file in and update it's MediaInfo.
                                        SQL = "SELECT eventID, bigfilesize, soundlay FROM events WHERE companyID = " & l_rstLoggedEvents("companyID") & " AND libraryID = " & GetData("events", "libraryID", "eventID", l_rstLoggedEvents("clipID")) & " AND altlocation = '" & AltLocation & "' AND clipfilename = '" & fileName & "' AND system_deleted = 0;"
                                        Set l_rstID = New ADODB.Recordset
                                        l_rstID.Open SQL, cnn, 3, 3
                                        If l_rstID.EOF Then
                                            l_rstID.Close
                                            LastID = CopyEventToLibraryID(l_rstLoggedEvents("clipID"), GetData("events", "libraryID", "eventID", l_rstLoggedEvents("clipID")))
                                            SetData "events", "altlocation", "eventID", LastID, AltLocation
                                            SetData "events", "clipfilename", "eventID", LastID, fileName
                                            Count = 0
                                            While InStr(Mid(fileName, Count + 1), ".") > 0
                                                Count = Count + InStr(Mid(fileName, Count + 1), ".")
                                            Wend
                                            If Count > 0 Then
                                                SetData "events", "clipreference", "eventID", LastID, Left(fileName, Count - 1)
                                            Else
                                                SetData "events", "clipreference", "eventID", LastID, fileName
                                            End If
                                            SetData "events", "internalreference", "eventID", LastID, GetNextSequence("internalreference")
                                            SetData "events", "cdate", "eventID", LastID, FormatSQLDate(Now)
                                            SetData "events", "cuser", "eventID", LastID, "ASPERA"
                                            SetData "events", "mdate", "eventID", LastID, FormatSQLDate(Now)
                                            SetData "events", "muser", "eventID", LastID, "ASPERA"
                                            SetData "events", "soundlay", "eventID", LastID, FormatSQLDate(Now)
                                            
                                            l_strPathToFile = GetData("library", "subtitle", "libraryID", GetData("events", "libraryID", "eventID", l_rstLoggedEvents("clipID")))
                                            l_strPathToFile = l_strPathToFile & "\" & AltLocation
                                            l_strPathToFile = l_strPathToFile & "\" & fileName
                                            API_OpenFile Replace(l_strPathToFile, "\\", "\\?\UNC\"), l_lngFileHandle, l_curFileSize
                                            API_CloseFile l_lngFileHandle
                                            SetData "events", "bigfilesize", "eventID", LastID, l_curFileSize
                                            l_curFolderSize = l_curFolderSize + l_curFileSize
                                            
                                            MediaData = GetMediaInfoOnFile(Replace(l_strPathToFile, "\\", "\\?\UNC\"))
                                            If MediaData.txtFormat = "TTML" Then
                                                SetData "events", "clipformat", "eventID", LastID, "ITT Subtitles"
'                                            ElseIf MediaData.txtFormat = "JPEG Image" Or MediaData.txtFormat = "PNG Image" Or MediaData.txtFormat = "BMP Image" Or MediaData.txtFormat = "GIF Image" Then
'                                                SetData "events", "clipformat", "eventID", LastID, MediaData.txtFormat
'                                                If getImgDim(l_strPathToFile, l_ImageDimensions, Temp) = True Then
'                                                    SetData "events", "clipverticalpixels", "eventID", LastID, l_ImageDimensions.height
'                                                    SetData "events", "cliphorizontalpixels", "eventID", LastID, l_ImageDimensions.width
'                                                End If
                                            ElseIf MediaData.txtFormat <> "" Then
                                                SetData "events", "clipformat", "eventID", LastID, MediaData.txtFormat
                                                SetData "events", "clipaudiocodec", "eventID", LastID, MediaData.txtAudioCodec
                                                SetData "events", "clipframerate", "eventID", LastID, MediaData.txtFrameRate
                                                SetData "events", "cbrvbr", "eventID", LastID, MediaData.txtCBRVBR
                                                SetData "events", "cliphorizontalpixels", "eventID", LastID, MediaData.txtWidth
                                                SetData "events", "clipverticalpixels", "eventID", LastID, MediaData.txtHeight
                                                SetData "events", "videobitrate", "eventID", LastID, MediaData.txtVideoBitrate
                                                SetData "events", "interlace", "eventID", LastID, MediaData.txtInterlace
                                                SetData "events", "timecodestart", "eventID", LastID, MediaData.txtTimecodestart
                                                SetData "events", "timecodestop", "eventID", LastID, MediaData.txtTimecodestop
                                                SetData "events", "fd_length", "eventID", LastID, MediaData.txtDuration
                                                SetData "events", "audiobitrate", "eventID", LastID, MediaData.txtAudioBitrate
                                                SetData "events", "trackcount", "eventID", LastID, MediaData.txtTrackCount
                                                SetData "events", "channelcount", "eventID", LastID, MediaData.txtChannelCount
                                                If Val(MediaData.txtAudioBitrate) + Val(MediaData.txtVideoBitrate) <> 0 Then
                                                    SetData "events", "clipbitrate", "eventID", LastID, Val(MediaData.txtAudioBitrate) + Val(MediaData.txtVideoBitrate)
                                                Else
                                                    SetData "events", "clipbitrate", "eventID", LastID, Val(MediaData.txtOverallBitrate)
                                                End If
                                                SetData "events", "aspectratio", "eventID", LastID, MediaData.txtAspect
                                                If MediaData.txtAspect = "4:3" Then
                                                    SetData "events", "fourbythreeflag", "eventID", LastID, 1
                                                Else
                                                    SetData "events", "fourbythreeflag", "eventID", LastID, 0
                                                End If
                                                SetData "events", "geometriclinearity", "eventID", LastID, MediaData.txtGeometry
                                                SetData "events", "clipcodec", "eventID", LastID, MediaData.txtVideoCodec
                                                SetData "events", "colorspace", "eventID", LastID, MediaData.txtColorSpace
                                                SetData "events", "chromasubsampling", "eventID", LastID, MediaData.txtChromaSubsmapling
                                                If Trim(" " & GetData("events", "eventtype", "eventID", LastID)) = "" Then SetData "events", "eventtype", "eventID", LastID, GetData("xref", "format", "description", MediaData.txtVideoCodec)
                                                SetData "events", "mediastreamtype", "eventID", LastID, MediaData.txtStreamType
                                                If MediaData.txtSeriesTitle <> "" Then
                                                    SetData "events", "eventtitle", "eventID", LastID, MediaData.txtSeriesTitle
                                                    SetData "events", "eventseries", "eventID", LastID, MediaData.txtSeriesNumber
                                                    SetData "events", "eventsubtitle", "eventID", LastID, MediaData.txtProgrammeTitle
                                                    SetData "events", "eventepisode", "eventID", LastID, MediaData.txtEpisodeNumber
                                                    SetData "events", "notes", "eventID", LastID, MediaData.txtSynopsis
                                                End If
                                                SetData "events", "lastmediainfoquery", "eventID", LastID, FormatSQLDate(Now)
                                            Else
                                                Select Case UCase(Right(fileName, 3))
                                                    Case "ISO"
                                                        SetData "events", "clipformat", "eventID", LastID, "DVD ISO Image"
                                                    Case "PDF"
                                                        SetData "events", "clipformat", "eventID", LastID, "PDF File"
                                                    Case "PSD"
                                                        SetData "events", "clipformat", "eventID", LastID, "PSD Still Image"
                                                    Case "SCC"
                                                        SetData "events", "clipformat", "eventID", LastID, "SCC File (EIA 608)"
                                                    Case "TIF"
                                                        SetData "events", "clipformat", "eventID", LastID, "TIF Still Image"
                                                    Case "DOC"
                                                        SetData "events", "clipformat", "eventID", LastID, "Word File"
                                                    Case "XLS"
                                                        SetData "events", "clipformat", "eventID", LastID, "XLS File"
                                                    Case "XML"
                                                        SetData "events", "clipformat", "eventID", LastID, "XML File"
                                                    Case "RAR"
                                                        SetData "events", "clipformat", "eventID", LastID, "RAR Archive"
                                                    Case "ZIP"
                                                        SetData "events", "clipformat", "eventID", LastID, "ZIP Archive"
                                                    Case "TAR"
                                                        SetData "events", "clipformat", "eventID", LastID, "TAR Archive"
                                                    Case Else
                                                        SetData "events", "clipformat", "eventID", LastID, "Other"
                                                End Select
                                            End If
                                            cnn.Execute "INSERT INTO eventhistory (eventID, datesaved, username, description) VALUES (" & LastID & ", getdate(), 'ASPERA', 'Aspera Upload');"
                                        Else
                                            LastID = l_rstID("eventID")
                                            l_rstID("bigfilesize") = Trim(" " & l_rstAsperaFiles("size"))
                                            l_rstID("soundlay") = FormatSQLDate(Now)
                                            l_rstID.Update
                                            l_rstID.Close
                                            l_curFolderSize = l_curFolderSize + l_rstAsperaFiles("size")
                                        End If
                                        
                                        If InStr(l_strCetaClientCode, "/invisibleuploads") <= 0 Then
                                            SetData "events", "hidefromweb", "eventID", LastID, 0
                                        End If
                                        
                                        SQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipid, accesstype, finalstatus, asperatoken, processafterupload) VALUES ("
                                        SQL = SQL & l_rstLoggedEvents("contactID") & ", "
                                        SQL = SQL & l_rstLoggedEvents("companyID") & ", "
                                        SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                                        SQL = SQL & LastID & ", "
                                        SQL = SQL & "'Aspera Upload within Folder', "
                                        SQL = SQL & "'Completed', "
                                        SQL = SQL & "'" & l_rstSession("session_id") & "', "
                                        SQL = SQL & "0);"
                                        
                                        cnn.Execute SQL
                                        
                                        SendSMTPMail g_strUserEmailAddress, g_strUserEmailName, "File Upload within a Folder Upload", fileName & " has been uploaded and is on the " & GetData("library", "barcode", "libraryID", GetData("events", "libraryID", "eventID", l_rstLoggedEvents("clipID"))) & " in the " & AltLocation & " folder.", "", False, g_strManagerEmailAddress, g_strManagerEmailName, g_strAdministratorEmailAddress
                                        
                                        l_strClipFormat = GetData("events", "clipformat", "eventID", LastID)
                                        l_strFilename = GetData("events", "clipfilename", "eventID", LastID)
                                        l_lngClipBitRate = Val(Trim(" " & GetData("events", "clipbitrate", "eventID", LastID)))
                                        If l_strFilename = "c" Then
                                            'Just get rid of it :-)
                                            SQL = "INSERT INTO event_file_request (eventID, sourcelibraryID, event_file_Request_typeID, NewLibraryID, RequestName, Requestdate) VALUES ("
                                            SQL = SQL & l_lngClipID & ", "
                                            SQL = SQL & GetData("events", "libraryID", "eventID", l_lngClipID) & ", "
                                            SQL = SQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Delete") & ", "
                                            SQL = SQL & GetData("events", "libraryID", "eventID", l_lngClipID) & ", "
                                            SQL = SQL & "'Aspera', Getdate()');"
                                            Debug.Print SQL
                                            cnn.Execute SQL
                                        End If
                                        If InStr(l_strCetaClientCode, "/MoveAfterUpload") > 0 And l_strFilename <> ".DS_Store" Then
                                            'There needs to be a move of uploaded file to another store.
                                            l_strNewAltLocation = "ceta\Ceta\" & l_rstLoggedEvents("companyID") & "\Uploaded"
                                            If InStr(l_strCetaClientCode, "/MoveAfterUploadFolder") > 0 Then
                                                l_strNewAltLocationField = Mid(l_strCetaClientCode, InStr(l_strCetaClientCode, "/MoveAfterUploadFolder") + 23)
                                                If InStr(l_strNewAltLocationField, "/") > 0 Then
                                                    l_strNewAltLocationField = Trim(" " & Left(l_strNewAltLocationField, InStr(l_strNewAltLocationField, "/") - 1))
                                                End If
                                                If Len(l_strNewAltLocationField) > 0 Then
                                                    If Left(l_strNewAltLocationField, 1) = "{" Then
                                                        l_strNewAltLocationField = Mid(l_strNewAltLocationField, 2, Len(l_strNewAltLocationField) - 2)
                                                        If Trim(" " & GetData("events", l_strNewAltLocationField, "eventID", l_lngClipID)) <> "" Then
                                                            l_strNewAltLocation = l_strNewAltLocation & "\" & Replace(Replace(Replace(Replace(Trim(" " & GetData("events", l_strNewAltLocationField, "eventID", l_lngClipID)), " ", "_"), ":", "_"), "/", "_"), "\", "_") & "\" & l_strDatedSubfolder
                                                        End If
                                                    Else
                                                        l_strNewAltLocation = l_strNewAltLocation & "\" & l_strNewAltLocationField & "\" & l_strDatedSubfolder
                                                    End If
                                                End If
                                            End If
                                            If l_strAltLocationTail <> "" Then
                                                l_strNewAltLocation = l_strNewAltLocation & "\" & l_strAltLocationTail
                                            End If
                                            Sleep 60000
                                            SQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, NewLibraryID, NewAltLocation, bigfilesize, RequestDate, RequestName, Urgent) VALUES ("
                                            SQL = SQL & LastID & ", "
                                            SQL = SQL & GetData("events", "libraryID", "eventID", LastID) & ", "
                                            SQL = SQL & GetData("Event_File_Request_Type", "Event_File_Request_TypeID", "description", "Move") & ", "
                                            SQL = SQL & Val(Mid(l_strCetaClientCode, InStr(l_strCetaClientCode, "/MoveAfterUpload") + 17)) & ", "
                                            SQL = SQL & "'" & l_strNewAltLocation & "', "
                                            SQL = SQL & l_curFileSize & ", "
                                            SQL = SQL & "Getdate(), 'Aspera', 1)"
                                            Debug.Print SQL
                                            cnn.Execute SQL
                                        End If
                                        If InStr(l_strCetaClientCode, "/MakeTrackerEntry") > 0 And l_strFilename <> ".DS_Store" Then
                                            'Make a Generic Tracker Line for this file, and possibly add a Workflow Variant ID to it also.
                                            If InStr(l_strCetaClientCode, "/MakeTrackerWorkflowVariantID") > 0 Then
                                                l_lngCounter = InStr(l_strCetaClientCode, "/MakeTrackerWorkflowVariantID")
                                                l_lngWorkflowVariantID = Val(Mid(l_strCetaClientCode, l_lngCounter + 30))
                                            Else
                                                l_lngWorkflowVariantID = 0
                                            End If
                                            SQL = "INSERT INTO tracker_item (companyID, originaleventID, itemreference, itemfilename" & IIf(l_lngWorkflowVariantID <> 0, ", WorkflowVariantID", "") & ") VALUES ("
                                            SQL = SQL & l_rstLoggedEvents("companyID") & ", "
                                            SQL = SQL & LastID & ", "
                                            SQL = SQL & "'" & QuoteSanitise(GetData("events", "clipreference", "eventID", LastID)) & "', "
                                            SQL = SQL & "'" & QuoteSanitise(GetData("events", "clipfilename", "eventID", LastID)) & "'"
                                            If l_lngWorkflowVariantID <> 0 Then
                                                SQL = SQL & ", " & l_lngWorkflowVariantID
                                            End If
                                            SQL = SQL & ");"
                                            Debug.Print SQL
                                            cnn.Execute SQL
                                        End If
                                        If (l_rstLoggedEvents("processafterupload") = 2 Or (l_strClipFormat = "Flash MP4" And l_lngClipBitRate < 3000)) And l_lngLibraryID <> 722001 And l_strFilename <> ".DS_Store" Then
                                            'Check whether there is already a file on the flashstore of this name and company, and if not, move it.
                                            If Trim(" " & GetDataSQL("SELECT eventID FROM events WHERE libraryID = 276356 AND altlocation = '" & GetData("events", "companyID", "eventID", l_lngClipID) & "' AND clipfilename = '" & l_strFilename & "' AND companyID = " & l_rstLoggedEvents("companyID") & " AND system_deleted = 0")) = "" Then
                                                'Move it to the Flashstore
                                                If Right(LCase(l_strFilename), 4) = ".mp4" Or Right(LCase(l_strFilename), 4) = ".flv" Or Right(LCase(l_strFilename), 4) = ".mov" Then
                                                    SetData "events", "clipsoundformat", "eventID", l_lngClipID, "UK_3_" & GetData("company", "marketcode", "companyID", l_rstLoggedEvents("companyID"))
                                                    SQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, bigfilesize, RequestName, RequesterEmail) VALUES ("
                                                    SQL = SQL & LastID & ", "
                                                    SQL = SQL & l_lngLibraryID & ", "
                                                    SQL = SQL & GetData("event_file_request_type", "event_file_Request_typeID", "description", "Move") & ", "
                                                    SQL = SQL & "722001, "
                                                    SQL = SQL & "'" & GetData("events", "companyID", "eventID", LastID) & "', "
                                                    SQL = SQL & l_curFileSize & ", "
                                                    SQL = SQL & "'ContentDelivery', NULL);"
                                                    Debug.Print SQL
                                                    cnn.Execute SQL
                                                End If
                                                l_blnMoveToFlashstore = True
                                            Else
                                                l_blnCannotMoveToFlashstore = True
                                            End If
                                        End If
                                        l_rstAsperaFiles.MoveNext
                                    Loop
                                End If
                                l_rstAsperaFiles.Close
                            End If
                            l_rstSession.MoveNext
                        Loop
                    End If
                    l_rstSession.Close
                    If l_curFolderSize > 0 Then
                        If Int(l_curFolderSize / 1024 / 1024 / 1024 + 0.999) > 0 Then
                            emailmessage = emailmessage & vbCrLf & "Total Folder Size = " & Int(l_curFolderSize / 1024 / 1024 / 1024 + 0.999) & "GB" & vbCrLf
                        ElseIf Int(l_curFolderSize / 1024 / 1024 + 0.999) > 0 Then
                            emailmessage = emailmessage & vbCrLf & "Total Folder Size = " & Int(l_curFolderSize / 1024 / 1024 + 0.999) & "MB" & vbCrLf
                        ElseIf Int(l_curFolderSize / 1024 + 0.999) > 0 Then
                            emailmessage = emailmessage & vbCrLf & "Total Folder Size = " & Int(l_curFolderSize / 1024 + 0.999) & "KB" & vbCrLf
                        Else
                            emailmessage = emailmessage & vbCrLf & "Total Folder Size = " & Int(l_curFolderSize) & "Bytes" & vbCrLf
                        End If
                    Else
                        emailmessage = emailmessage & vbCrLf & "Total Folder Size = " & Int(l_curFolderSize) & "Bytes" & vbCrLf
                    End If
                    If InStr(l_strCetaClientCode, "/uploadflatfee") <= 0 Then SetData "jobdetailunbilled", "quantity", "asperatoken", l_rstLoggedEvents("asperatoken"), Int(l_curFolderSize / 1024 / 1024 / 1024 + 0.999)
                    l_rstLoggedEvents("bigfilesize") = l_curFolderSize
                    l_rstLoggedEvents.Update
                    Debug.Print emailmessage
                    If InStr(l_strCetaClientCode, "/emailgroupuploads") > 0 Then
                        SendSMTPMail GetData("contact", "email", "contactID", l_rstLoggedEvents("contactID")), GetData("contact", "name", "contactID", l_rstLoggedEvents("contactID")), "Aspera Folder Upload Completed", emailmessage, "", True, g_strUserEmailAddress, g_strFullUserName
                        l_strSQL = "SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", l_rstLoggedEvents("companyID")) & "' AND trackermessageID = 39;"
                        Set l_rsDADCTracker = New ADODB.Recordset
                        l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
                        If l_rsDADCTracker.RecordCount > 0 Then
                            l_rsDADCTracker.MoveFirst
                            While Not l_rsDADCTracker.EOF
                                SendSMTPMail l_rsDADCTracker("email"), l_rsDADCTracker("fullname"), "Aspera Folder Upload Completed", emailmessage, "", True, "", "", g_strAdministratorEmailAddress
                                l_rsDADCTracker.MoveNext
                            Wend
                        End If
                        l_rsDADCTracker.Close
                    ElseIf InStr(l_strCetaClientCode, "/emailuploads") > 0 Then
                        SendSMTPMail GetData("contact", "email", "contactID", l_rstLoggedEvents("contactID")), GetData("contact", "name", "contactID", l_rstLoggedEvents("contactID")), "Aspera Folder Upload Completed", emailmessage, "", True, g_strUserEmailAddress, g_strFullUserName
                        If Trim(" " & GetData("company", "email", "companyID", l_rstLoggedEvents("companyID"))) <> "" Then
                            SendSMTPMail GetData("company", "email", "companyID", l_rstLoggedEvents("companyID")), "Upload Notifications List", "Aspera Folder Upload Completed", emailmessage, "", True, GetData("company", "jcacontactemail", "companyID", l_rstLoggedEvents("companyID")), "", g_strAdministratorEmailAddress
                        End If
                    Else
                        SendSMTPMail GetData("contact", "email", "contactID", l_rstLoggedEvents("contactID")), GetData("contact", "name", "contactID", l_rstLoggedEvents("contactID")), "Aspera Folder Upload Completed", emailmessage, "", True, g_strUserEmailAddress, g_strFullUserName, g_strAdministratorEmailAddress, False, g_strUserEmailAddress
                    End If
                    If InStr(l_strCetaClientCode, "/showuploadedfolders") > 0 Then
                        SQL = "UPDATE events SET bigfilesize = " & l_curFolderSize & ", soundlay = '" & FormatSQLDate(Now) & "' WHERE eventID = " & l_rstLoggedEvents("clipID") & ";"
                    Else
                        SQL = "UPDATE events SET system_deleted = 1 WHERE eventID = " & l_rstLoggedEvents("clipID") & ";"
                    End If
                    cnn.Execute SQL
                    Set l_rstSession = Nothing
                End If
            ElseIf l_strResult = "completed" And l_rstLoggedEvents("accesstype") <> "Aspera Upload" Then
                l_rstLoggedEvents("bigfilesize") = GetData("events", "bigfilesize", "eventID", l_rstLoggedEvents("clipID"))
                l_rstLoggedEvents.Update
                SendSMTPMail g_strUserEmailAddress, g_strUserEmailName, "Aspera Download Completed", "Clip: " & GetData("events", "clipfilename", "eventID", l_rstLoggedEvents("clipID")) & vbCrLf & "Completed: " & Now, "", True, g_strAdministratorEmailAddress, g_strAdministratorEmailName
            End If
        ElseIf l_rstLoggedEvents("timewhen") < DateAdd("n", -10, Now) Then
            l_rstLoggedEvents("finalstatus") = "error"
            l_rstLoggedEvents.Update
        End If
        l_rstLoggedEvents.MoveNext
    Loop
End If
l_rstLoggedEvents.Close

End Sub

Public Sub CheckWebDeliveryPortal()

Dim l_rstSession As ADODB.Recordset, l_rsDADCTracker As ADODB.Recordset, l_rsCC As ADODB.Recordset
Dim l_curFolderSize As Currency, RunningTime As Long, File_Duration As String, File_Seconds As Long
Dim l_strSQL As String, BBCMGClipOrderNumber As Long, Counter As Long
Dim l_ImageDimensions As ImgDimType, Temp As String, l_strCCaddress As String
Dim l_rstID As ADODB.Recordset, LastID As Long, AltLocation As String, l_blnWarpError As Boolean, l_blnWarpError2 As Boolean, l_lngCounter As Long
Dim l_lngTranscodeSpecID As Long, l_strAutotranscodeBitrate As String, l_strAutotranscodeHoriz As String
Dim l_lngClipBitRate As Long
Dim l_blnMoveToStreamstore As Boolean, l_blnMoveToFlashstore As Boolean, l_blnAutoProxyTranscode As Boolean, l_blnAutoThumbnail As Boolean
Dim l_blnCannotMoveToStreamstore As Boolean, l_blnCannotMoveToFlashstore As Boolean
Dim l_lngTrackerID As Long, l_rstItemChoice As ADODB.Recordset, l_strStageField As String, l_blnConclusionValue As Boolean
Dim l_strNewAltLocation As String, l_strNewAltLocationField As String, l_strDatedSubfolder As String, l_strAltLocation As String, l_strAltLocationTail As String
Dim l_lngWorkflowVariantID As Long
   
SQL = "SELECT * FROM webdeliveryportal "
SQL = SQL & "WHERE (asperatoken IS NOT NULL AND asperatoken <> '') "
SQL = SQL & "AND finalstatus is null "
SQL = SQL & "OR (finalstatus <> 'completed' "
SQL = SQL & "AND (finalstatus <> 'cancelled' or (finalstatus = 'cancelled' and timewhen > '" & FormatSQLDate(Nowdate) & "')) "
SQL = SQL & "AND (finalstatus <> 'error' OR (finalstatus = 'error' and timewhen > '" & FormatSQLDate(Nowdate) & "')));"
l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    Debug.Print "Webdeliveryportal " & l_rstLoggedEvents.RecordCount & " records"
    l_lngCounter = 1
    l_rstLoggedEvents.MoveFirst
    Do While Not l_rstLoggedEvents.EOF
        Debug.Print l_lngCounter
        l_lngCounter = l_lngCounter + 1
        l_blnMoveToStreamstore = False
        l_blnMoveToFlashstore = False
        l_blnAutoProxyTranscode = False
        l_blnAutoThumbnail = False
        l_blnCannotMoveToStreamstore = False
        l_blnCannotMoveToFlashstore = False
        timewhen = l_rstLoggedEvents("timewhen")
        l_strResult = GetAsperaData("fasp_sessions", "status", "token", l_rstLoggedEvents("asperatoken"))
        If l_strResult = "" Then l_strResult = GetAsperaData("fasp_sessions", "status", "cookie", l_rstLoggedEvents("asperatoken"))
        If l_strResult <> "" Then
            
            If l_strResult = "error" Or l_strResult = "inactive" Or l_strResult = "cancelled" Then
                If l_rstLoggedEvents("accesstype") = "Aspera Download" Then
                    SessionID = GetAsperaData("fasp_sessions", "session_id", "token", l_rstLoggedEvents("asperatoken"))
                    fileName = GetData("events", "clipfilename", "eventID", l_rstLoggedEvents("clipID"))
                    TempResult = GetAsperaData("fasp_files", "status", "session_id", SessionID, "file_basename", QuoteSanitise(fileName))
                    If TempResult = "completed" Then l_strResult = TempResult
                End If
            End If
                
            l_rstLoggedEvents("finalstatus") = l_strResult
            l_rstLoggedEvents.Update
            If l_strResult = "completed" And l_rstLoggedEvents("accesstype") = "Aspera Upload" And GetData("events", "clipformat", "eventID", l_rstLoggedEvents("clipID")) <> "Uploaded Folder" Then
                l_strCetaClientCode = GetData("company", "cetaclientcode", "companyID", l_rstLoggedEvents("companyID"))
                l_lngClipID = l_rstLoggedEvents("clipID")
                l_lngLibraryID = GetData("events", "libraryID", "eventID", l_lngClipID)
                If l_lngLibraryID <> 0 Then
                    l_strPathToFile = GetData("library", "subtitle", "libraryID", l_lngLibraryID)
                    If Trim(" " & GetData("events", "altlocation", "eventID", l_lngClipID)) <> "" Then
                        l_strPathToFile = l_strPathToFile & "\" & GetData("events", "altlocation", "eventID", l_lngClipID)
                    End If
                    l_strPathToFile = l_strPathToFile & "\" & GetData("events", "clipfilename", "eventID", l_lngClipID)
                    l_strAltLocation = GetData("events", "altlocation", "eventID", l_lngClipID)
                    If Len(l_strAltLocation) > Len(Trim(l_rstLoggedEvents("companyID")) & "\") Then
                        l_strAltLocationTail = Mid(AltLocation, Len(Trim(l_rstLoggedEvents("companyID"))) + 2)
                    Else
                        l_strAltLocationTail = ""
                    End If
                    API_OpenFile Replace(l_strPathToFile, "\\", "\\?\UNC\"), l_lngFileHandle, l_curFileSize
                    API_CloseFile l_lngFileHandle
                    SetData "events", "bigfilesize", "eventID", l_lngClipID, l_curFileSize
                    SetData "events", "soundlay", "eventID", l_lngClipID, FormatSQLDate(Now)
                    l_rstLoggedEvents("bigfilesize") = l_curFileSize
                    l_rstLoggedEvents.Update
                    If InStr(l_strCetaClientCode, "/invisibleuploads") <= 0 Then
                        SetData "events", "hidefromweb", "eventID", l_lngClipID, 0
                    End If
                    SetData "jobdetailunbilled", "quantity", "asperatoken", l_rstLoggedEvents("asperatoken"), Int(l_curFileSize / 1024 / 1024 / 1024 + 0.999)
                    If l_rstLoggedEvents("mediainfolookup") <> 0 Then
                        MediaData = GetMediaInfoOnFile(Replace(l_strPathToFile, "\\", "\\?\UNC\"))
                        If MediaData.txtFormat = "TTML" Then
                            SetData "events", "clipformat", "eventID", l_lngClipID, "ITT Subtitles"
'                        ElseIf MediaData.txtFormat = "JPEG Image" Or MediaData.txtFormat = "PNG Image" Or MediaData.txtFormat = "BMP Image" Or MediaData.txtFormat = "GIF Image" Then
'                            SetData "events", "clipformat", "eventID", l_lngClipID, MediaData.txtFormat
'                            If getImgDim(l_strPathToFile, l_ImageDimensions, Temp) = True Then
'                                SetData "events", "clipverticalpixels", "eventID", l_lngClipID, l_ImageDimensions.height
'                                SetData "events", "cliphorizontalpixels", "eventID", l_lngClipID, l_ImageDimensions.width
'                            End If
                        ElseIf MediaData.txtFormat <> "" Then
                            SetData "events", "clipformat", "eventID", l_lngClipID, MediaData.txtFormat
                            SetData "events", "clipaudiocodec", "eventID", l_lngClipID, MediaData.txtAudioCodec
                            SetData "events", "clipframerate", "eventID", l_lngClipID, MediaData.txtFrameRate
                            SetData "events", "cbrvbr", "eventID", l_lngClipID, MediaData.txtCBRVBR
                            SetData "events", "cliphorizontalpixels", "eventID", l_lngClipID, MediaData.txtWidth
                            SetData "events", "clipverticalpixels", "eventID", l_lngClipID, MediaData.txtHeight
                            SetData "events", "videobitrate", "eventID", l_lngClipID, MediaData.txtVideoBitrate
                            SetData "events", "interlace", "eventID", l_lngClipID, MediaData.txtInterlace
                            SetData "events", "timecodestart", "eventID", l_lngClipID, MediaData.txtTimecodestart
                            SetData "events", "timecodestop", "eventID", l_lngClipID, MediaData.txtTimecodestop
                            SetData "events", "fd_length", "eventID", l_lngClipID, MediaData.txtDuration
                            
                            File_Duration = Trim(" " & MediaData.txtDuration)
                            If Len(File_Duration) <> 11 Then File_Duration = "00:00:00:00"
                            RunningTime = 60 * Val(Mid(File_Duration, 1, 2))
                            RunningTime = RunningTime + Val(Mid(File_Duration, 4, 2))
                            File_Seconds = Val(Mid(File_Duration, 7, 2))
                            If File_Seconds > 29 Then RunningTime = RunningTime + 1
                            If RunningTime = 0 Then RunningTime = 1
                            'response.write " " & duration & ", " & runningtime
                            SetData "jobdetailunbilled", "runningtime", "asperatoken", l_rstLoggedEvents("asperatoken"), RunningTime

                            SetData "events", "audiobitrate", "eventID", l_lngClipID, MediaData.txtAudioBitrate
                            SetData "events", "trackcount", "eventID", l_lngClipID, MediaData.txtTrackCount
                            SetData "events", "channelcount", "eventID", l_lngClipID, MediaData.txtChannelCount
                            If Val(MediaData.txtAudioBitrate) + Val(MediaData.txtVideoBitrate) <> 0 Then
                                SetData "events", "clipbitrate", "eventID", l_lngClipID, Val(MediaData.txtAudioBitrate) + Val(MediaData.txtVideoBitrate)
                            Else
                                SetData "events", "clipbitrate", "eventID", l_lngClipID, Val(MediaData.txtOverallBitrate)
                            End If
                            SetData "events", "aspectratio", "eventID", l_lngClipID, MediaData.txtAspect
                            If MediaData.txtAspect = "4:3" Then
                                SetData "events", "fourbythreeflag", "eventID", l_lngClipID, 1
                            Else
                                SetData "events", "fourbythreeflag", "eventID", l_lngClipID, 0
                            End If
                            SetData "events", "geometriclinearity", "eventID", l_lngClipID, MediaData.txtGeometry
                            SetData "events", "clipcodec", "eventID", l_lngClipID, MediaData.txtVideoCodec
                            SetData "events", "colorspace", "eventID", l_lngClipID, MediaData.txtColorSpace
                            SetData "events", "chromasubsampling", "eventID", l_lngClipID, MediaData.txtChromaSubsmapling
                            If Trim(" " & GetData("events", "eventtype", "eventID", l_lngClipID)) = "" Then SetData "events", "eventtype", "eventID", l_lngClipID, GetData("xref", "format", "description", MediaData.txtVideoCodec)
                            SetData "events", "mediastreamtype", "eventID", l_lngClipID, MediaData.txtStreamType
                            If MediaData.txtSeriesTitle <> "" Then
                                SetData "events", "eventtitle", "eventID", l_lngClipID, MediaData.txtSeriesTitle
                                SetData "events", "eventseries", "eventID", l_lngClipID, MediaData.txtSeriesNumber
                                SetData "events", "eventsubtitle", "eventID", l_lngClipID, MediaData.txtProgrammeTitle
                                SetData "events", "eventepisode", "eventID", l_lngClipID, MediaData.txtEpisodeNumber
                                SetData "events", "notes", "eventID", l_lngClipID, MediaData.txtSynopsis
                            End If
                            SetData "events", "lastmediainfoquery", "eventID", l_lngClipID, FormatSQLDate(Now)
                        Else
                            l_strFilename = GetData("events", "clipfilename", "eventID", l_lngClipID)
                            Select Case UCase(Right(l_strFilename, 3))
                                Case "ISO"
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "DVD ISO Image"
                                Case "PDF"
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "PDF File"
                                Case "PSD"
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "PSD Still Image"
                                Case "SCC"
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "SCC File (EIA 608)"
                                Case "STL"
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "STL Subtitles"
                                Case "TIF"
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "TIF Still Image"
                                Case "DOC", "DOCX"
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "Word File"
                                Case "XLS", "XLSX"
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "XLS File"
                                Case "XML"
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "XML File"
                                Case "RAR"
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "RAR Archive"
                                Case "ZIP"
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "ZIP Archive"
                                Case "TAR"
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "TAR Archive"
                                Case Else
                                    SetData "events", "clipformat", "eventID", l_lngClipID, "Other"
                            End Select
                        End If
                    End If
                    l_strClipFormat = GetData("events", "clipformat", "eventID", l_lngClipID)
                    l_strFilename = GetData("events", "clipfilename", "eventID", l_lngClipID)
                    l_lngClipBitRate = Val(Trim(" " & GetData("events", "clipbitrate", "eventID", l_lngClipID)))
                    
                    If l_strFilename = ".DS_Store" Then
                        'Just get rid of it :-)
                        SQL = "INSERT INTO event_file_request (eventID, sourcelibraryID, event_file_Request_typeID, NewLibraryID, RequestName, Requestdate) VALUES ("
                        SQL = SQL & l_lngClipID & ", "
                        SQL = SQL & GetData("events", "libraryID", "eventID", l_lngClipID) & ", "
                        SQL = SQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Delete") & ", "
                        SQL = SQL & GetData("events", "libraryID", "eventID", l_lngClipID) & ", "
                        SQL = SQL & "'Aspera', Getdate()');"
                        Debug.Print SQL
                        cnn.Execute SQL
                    End If
                    If InStr(l_strCetaClientCode, "/MoveAfterUpload") > 0 And l_strFilename <> ".DS_Store" Then
                        'There needs to be a move of uploaded file to another store.
                        l_strNewAltLocation = "ceta\Ceta\" & l_rstLoggedEvents("companyID") & "\Uploaded"
                        l_strDatedSubfolder = Format(Now, "YYYYMMDDHHNNSS")
                        If InStr(l_strCetaClientCode, "/MoveAfterUploadFolder") > 0 Then
                            l_strNewAltLocationField = Mid(l_strCetaClientCode, InStr(l_strCetaClientCode, "/MoveAfterUploadFolder") + 23)
                            If InStr(l_strNewAltLocationField, "/") > 0 Then
                                l_strNewAltLocationField = Trim(" " & Left(l_strNewAltLocationField, InStr(l_strNewAltLocationField, "/") - 1))
                            End If
                            If Len(l_strNewAltLocationField) > 0 Then
                                If Left(l_strNewAltLocationField, 1) = "{" Then
                                    l_strNewAltLocationField = Mid(l_strNewAltLocationField, 2, Len(l_strNewAltLocationField) - 2)
                                    If Trim(" " & GetData("events", l_strNewAltLocationField, "eventID", l_lngClipID)) <> "" Then
                                        l_strNewAltLocation = l_strNewAltLocation & "\" & Replace(Replace(Replace(Replace(Trim(" " & GetData("events", l_strNewAltLocationField, "eventID", l_lngClipID)), " ", "_"), ":", "_"), "/", "_"), "\", "_") & "\" & l_strDatedSubfolder
                                    End If
                                Else
                                    l_strNewAltLocation = l_strNewAltLocation & "\" & l_strNewAltLocationField & "\" & l_strDatedSubfolder
                                End If
                            End If
                        End If
                        If l_strAltLocationTail <> "" Then
                            l_strNewAltLocation = l_strNewAltLocation & "\" & l_strAltLocationTail
                        End If
                        Sleep 20000
                        SQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, NewLibraryID, NewAltLocation, bigfilesize, RequestDate, RequestName, Urgent) VALUES ("
                        SQL = SQL & l_lngClipID & ", "
                        SQL = SQL & GetData("events", "libraryID", "eventID", l_lngClipID) & ", "
                        SQL = SQL & GetData("Event_File_Request_Type", "Event_File_Request_TypeID", "description", "Move") & ", "
                        SQL = SQL & Val(Mid(l_strCetaClientCode, InStr(l_strCetaClientCode, "/MoveAfterUpload") + 17)) & ", "
                        SQL = SQL & "'" & l_strNewAltLocation & "', "
                        SQL = SQL & l_curFileSize & ", "
                        SQL = SQL & "Getdate(), 'Aspera', 1)"
                        cnn.Execute SQL
                    End If
                    If InStr(l_strCetaClientCode, "/MakeTrackerEntry") > 0 And l_strFilename <> ".DS_Store" Then
                        'Make a Generic Tracker Line for this file, and possibly add a Workflow Variant ID to it also.
                        If InStr(l_strCetaClientCode, "/MakeTrackerWorkflowVariantID") > 0 Then
                            l_lngCounter = InStr(l_strCetaClientCode, "/MakeTrackerWorkflowVariantID")
                            l_lngWorkflowVariantID = Val(Mid(l_strCetaClientCode, l_lngCounter + 30))
                        Else
                            l_lngWorkflowVariantID = 0
                        End If
                        SQL = "INSERT INTO tracker_item (companyID, originaleventID, itemreference, itemfilename" & IIf(l_lngWorkflowVariantID <> 0, ", WorkflowVariantID", "") & ") VALUES ("
                        SQL = SQL & l_rstLoggedEvents("companyID") & ", "
                        SQL = SQL & l_lngClipID & ", "
                        SQL = SQL & "'" & QuoteSanitise(GetData("events", "clipreference", "eventID", l_lngClipID)) & "', "
                        SQL = SQL & "'" & QuoteSanitise(GetData("events", "clipfilename", "eventID", l_lngClipID)) & "'"
                        If l_lngWorkflowVariantID <> 0 Then
                            SQL = SQL & ", " & l_lngWorkflowVariantID
                        End If
                        SQL = SQL & ");"
                        Debug.Print SQL
                        cnn.Execute SQL
                    End If
                    If (l_rstLoggedEvents("processafterupload") = 2 Or (l_strClipFormat = "Flash MP4" And l_lngClipBitRate < 3000)) And l_lngLibraryID <> 722001 And l_strFilename <> ".DS_Store" Then
                        'Check whether there is already a file on the streamstore of this name and company, and if not, move it.
                        If Trim(" " & GetDataSQL("SELECT eventID FROM events WHERE libraryID = 276356 AND altlocation = '" & GetData("events", "altlocation", "eventID", l_lngClipID) & "' AND clipfilename = '" & l_strFilename & "' AND companyID = " & l_rstLoggedEvents("companyID") & " AND system_deleted = 0")) = "" Then
                            'Move it to the Flashstore
                            If Right(LCase(l_strFilename), 4) = ".mp4" Or Right(LCase(l_strFilename), 4) = ".flv" Or Right(LCase(l_strFilename), 4) = ".mov" Then
                                SetData "events", "clipsoundformat", "eventID", l_lngClipID, "UK_3_" & GetData("company", "marketcode", "companyID", l_rstLoggedEvents("companyID"))
                                SQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, bigfilesize, RequestName, RequesterEmail) VALUES ("
                                SQL = SQL & l_lngClipID & ", "
                                SQL = SQL & l_lngLibraryID & ", "
                                SQL = SQL & GetData("event_file_request_type", "event_file_Request_typeID", "description", "Move") & ", "
                                SQL = SQL & "722001, "
                                SQL = SQL & "'" & GetData("events", "companyID", "eventID", l_lngClipID) & "', "
                                SQL = SQL & l_curFileSize & ", "
                                SQL = SQL & "'ContentDelivery', NULL);"
                                Debug.Print SQL
                                cnn.Execute SQL
                            End If
                            l_blnMoveToFlashstore = True
                        Else
                            l_blnCannotMoveToFlashstore = True
                        End If
                    End If
                    If GetData("portaluser", "CopyFilesAfterUpload", "portaluserID", l_rstLoggedEvents("portaluserID")) <> 0 And l_strFilename <> ".DS_Store" Then
                        'There needs to be a copy of uploaded file to another store
                        If Trim(" " & GetData("portaluser", "CopyFilesIfExtender", "portaluserID", l_rstLoggedEvents("portaluserID"))) <> "" Then
                            If UCase(Right(GetData("events", "clipfilename", "eventID", l_lngClipID), Len(GetData("portaluser", "CopyFilesIfExtender", "portaluserID", l_rstLoggedEvents("portaluserID"))))) = UCase((GetData("portaluser", "CopyFilesIfExtender", "portaluserID", l_rstLoggedEvents("portaluserID")))) Then
                                SQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, NewLibraryID, NewAltLocation, bigfilesize, RequestDate) VALUES ("
                                SQL = SQL & l_lngClipID & ", "
                                SQL = SQL & GetData("events", "libraryID", "eventID", l_lngClipID) & ", "
                                SQL = SQL & GetData("Event_File_Request_Type", "Event_File_Request_TypeID", "description", "Copy") & ", "
                                SQL = SQL & GetData("portaluser", "CopyFilesToLibraryID", "portaluserID", l_rstLoggedEvents("portaluserID")) & ", "
                                SQL = SQL & "'" & GetData("portaluser", "CopyFilesToFolder", "portaluserID", l_rstLoggedEvents("portaluserID")) & IIf(l_strAltLocationTail <> "", "\" & l_strAltLocationTail, "") & "', "
                                SQL = SQL & l_curFileSize & ", "
                                SQL = SQL & "Getdate())"
                                cnn.Execute SQL
                            End If
                        Else
                            SQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, NewLibraryID, NewAltLocation, bigfilesize, RequestDate) VALUES ("
                            SQL = SQL & l_lngClipID & ", "
                            SQL = SQL & GetData("events", "libraryID", "eventID", l_lngClipID) & ", "
                            SQL = SQL & GetData("Event_File_Request_Type", "Event_File_Request_TypeID", "description", "Copy") & ", "
                            SQL = SQL & GetData("portaluser", "CopyFilesToLibraryID", "portaluserID", l_rstLoggedEvents("portaluserID")) & ", "
                            SQL = SQL & "'" & GetData("portaluser", "CopyFilesToFolder", "portaluserID", l_rstLoggedEvents("portaluserID")) & IIf(l_strAltLocationTail <> "", "\" & l_strAltLocationTail, "") & "', "
                            SQL = SQL & l_curFileSize & ", "
                            SQL = SQL & "Getdate())"
                            cnn.Execute SQL
                        End If
                    End If
                End If
                If l_strFilename <> ".DS_Store" Then
                    emailmessage = ""
                    emailmessage = emailmessage & "User: " & l_rstLoggedEvents("fullname") & ", Company: " & GetData("company", "name", "companyID", l_rstLoggedEvents("companyID")) & vbCrLf
                    emailmessage = emailmessage & "Upload Completed: " & Now & vbCrLf
                    emailmessage = emailmessage & "File Delivered: " & GetData("events", "clipfilename", "eventID", l_rstLoggedEvents("clipID")) & vbCrLf
                    emailmessage = emailmessage & "Title: " & GetData("events", "eventtitle", "eventID", l_rstLoggedEvents("clipID"))
                    If GetData("events", "eventsubtitle", "eventID", l_rstLoggedEvents("clipID")) <> "" Then emailmessage = emailmessage & ", Subtitle: " & GetData("events", "eventsubtitle", "eventID", l_rstLoggedEvents("clipID"))
                    If GetData("events", "eventepisode", "eventID", l_rstLoggedEvents("clipID")) <> "" Then emailmessage = emailmessage & ", Episode: " & GetData("events", "eventepisode", "eventID", l_rstLoggedEvents("clipID")) & vbCrLf
                    If l_blnCannotMoveToStreamstore = True Then
                        emailmessage = emailmessage & vbCrLf & "This file could not be automatically moved to the Streamstore, due to a file of the same name already being there." & vbCrLf
                    ElseIf l_blnMoveToStreamstore = True Then
                        emailmessage = emailmessage & vbCrLf & "This file will be automatically made available as a playable Proxy file." & vbCrLf
                    End If
                    If l_blnCannotMoveToFlashstore = True Then
                        emailmessage = emailmessage & vbCrLf & "This file could not be automatically moved to the Flashstore, due to a file of the same name already being there." & vbCrLf
                    ElseIf l_blnMoveToFlashstore = True Then
                        emailmessage = emailmessage & vbCrLf & "This file will be automatically made available as a playable Proxy file." & vbCrLf
                    End If
                    If Trim(" " & GetDataSQL("SELECT event_file_requestID FROM event_file_request WHERE eventID = " & l_lngClipID & " AND event_file_request_typeID = 2 AND RequestComplete IS NOT NULL")) <> "" Then
                        emailmessage = emailmessage & vbCrLf & "This file was automatically moved to " & GetData("library", "barcode", "libraryID", Val(Mid(l_strCetaClientCode, InStr(l_strCetaClientCode, "/MoveAfterUpload") + 17))) & "." & vbCrLf
                    ElseIf Trim(" " & GetDataSQL("SELECT event_file_requestID FROM event_file_request WHERE eventID = " & l_lngClipID & " AND event_file_request_typeID = 2 AND RequestFailed IS NOT NULL")) <> "" Then
                        emailmessage = emailmessage & vbCrLf & "This file should have been moved to " & GetData("library", "barcode", "libraryID", Val(Mid(l_strCetaClientCode, InStr(l_strCetaClientCode, "/MoveAfterUpload") + 17))) & ", but the move failed." & vbCrLf
                    End If
                    Debug.Print emailmessage
                    If InStr(l_strCetaClientCode, "/emailgroupuploads") > 0 Then
                        l_strSQL = "SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", l_rstLoggedEvents("companyID")) & "' AND trackermessageID = 39;"
                        Set l_rsDADCTracker = New ADODB.Recordset
                        l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
                        If l_rsDADCTracker.RecordCount > 0 Then
                            l_rsDADCTracker.MoveFirst
                            While Not l_rsDADCTracker.EOF
                                SendSMTPMail l_rsDADCTracker("email"), l_rsDADCTracker("fullname"), "Aspera Upload Completed", emailmessage, "", True, "", "", g_strAdministratorEmailAddress
                                l_rsDADCTracker.MoveNext
                            Wend
                        End If
                        l_rsDADCTracker.Close
                        If Trim(" " & l_rstLoggedEvents("additionalinfo")) <> "" Then
                            SendSMTPMail l_rstLoggedEvents("additionalinfo"), l_rstLoggedEvents("fullname"), "Aspera Upload Completed", emailmessage, "", True, "", "", g_strAdministratorEmailAddress
                        Else
                            SendSMTPMail GetData("portaluser", "email", "portaluserID", l_rstLoggedEvents("portaluserID")), l_rstLoggedEvents("fullname"), "Aspera Upload Completed", emailmessage, "", True, "", "", g_strAdministratorEmailAddress
                        End If
                        Set l_rsDADCTracker = Nothing
                    ElseIf InStr(l_strCetaClientCode, "/emailuploads") > 0 Then
                        l_strCCaddress = ""
                        Set l_rsCC = New ADODB.Recordset
                        l_rsCC.Open "SELECT * FROM portaluserCC WHERE portaluserID = " & l_rstLoggedEvents("portaluserID"), cnn, 3, 3
                        If l_rsCC.RecordCount > 0 Then
                            l_rsCC.MoveFirst
                            Do While Not l_rsCC.EOF
                                If l_strCCaddress <> "" Then l_strCCaddress = l_strCCaddress & "; "
                                l_strCCaddress = l_strCCaddress & l_rsCC("ccaddress")
                                l_rsCC.MoveNext
                            Loop
                        End If
                        l_rsCC.Close
                        Set l_rsCC = Nothing
                        If Trim(" " & l_rstLoggedEvents("additionalinfo")) <> "" Then
                            SendSMTPMail l_rstLoggedEvents("additionalinfo"), l_rstLoggedEvents("fullname"), "Aspera Upload Completed", emailmessage, "", True, l_strCCaddress, ""
                            SendSMTPMail GetData("portaluser", "email", "portaluserID", l_rstLoggedEvents("portaluserID")), l_rstLoggedEvents("fullname"), "Aspera Upload Completed", emailmessage, "", True, GetData("portaluser", "administratoremail", "portaluserID", l_rstLoggedEvents("portaluserID")), "", g_strAdministratorEmailAddress
                        Else
                            SendSMTPMail GetData("portaluser", "email", "portaluserID", l_rstLoggedEvents("portaluserID")), l_rstLoggedEvents("fullname"), "Aspera Upload Completed", emailmessage, "", True, GetData("portaluser", "administratoremail", "portaluserID", l_rstLoggedEvents("portaluserID")) & IIf(l_strCCaddress <> "", ";" & l_strCCaddress, ""), "", g_strAdministratorEmailAddress
                        End If
                        If Trim(" " & GetData("company", "email", "companyID", l_rstLoggedEvents("companyID"))) <> "" Then
                            SendSMTPMail GetData("company", "email", "companyID", l_rstLoggedEvents("companyID")), "Upload Notifications List", "Aspera Upload Completed", emailmessage, "", True, Trim(" " & GetData("company", "jcacontactemail", "companyID", l_rstLoggedEvents("companyID"))), "", g_strAdministratorEmailAddress
                        End If
                    Else
                        l_strCCaddress = ""
                        Set l_rsCC = New ADODB.Recordset
                        l_rsCC.Open "SELECT * FROM portaluserCC WHERE portaluserID = " & l_rstLoggedEvents("portaluserID"), cnn, 3, 3
                        If l_rsCC.RecordCount > 0 Then
                            l_rsCC.MoveFirst
                            Do While Not l_rsCC.EOF
                                If l_strCCaddress <> "" Then l_strCCaddress = l_strCCaddress & "; "
                                l_strCCaddress = l_strCCaddress & l_rsCC("ccaddress")
                                l_rsCC.MoveNext
                            Loop
                        End If
                        l_rsCC.Close
                        Set l_rsCC = Nothing
                        If Trim(" " & l_rstLoggedEvents("additionalinfo")) <> "" Then
                            SendSMTPMail l_rstLoggedEvents("additionalinfo"), l_rstLoggedEvents("fullname"), "Aspera Upload Completed", emailmessage, "", True, "", "", g_strAdministratorEmailAddress
                        Else
                            SendSMTPMail GetData("portaluser", "email", "portaluserID", l_rstLoggedEvents("portaluserID")), l_rstLoggedEvents("fullname"), "Aspera Upload Completed", emailmessage, "", True, l_strCCaddress, "", g_strAdministratorEmailAddress
                        End If
                        If Trim(" " & GetData("company", "email", "companyID", l_rstLoggedEvents("companyID"))) <> "" Then
                            SendSMTPMail g_strUserEmailAddress, g_strUserEmailName, "Aspera Upload Completed", emailmessage, "", True, Trim(" " & GetData("company", "jcacontactemail", "companyID", l_rstLoggedEvents("companyID"))), "", g_strAdministratorEmailAddress
                        End If
                    End If
                End If
            ElseIf l_strResult = "completed" And l_rstLoggedEvents("accesstype") = "Aspera Upload" And GetData("events", "clipformat", "eventID", l_rstLoggedEvents("clipID")) = "Uploaded Folder" Then
                l_strCetaClientCode = GetData("company", "cetaclientcode", "companyID", l_rstLoggedEvents("companyID"))
                emailmessage = ""
                emailmessage = emailmessage & "User: " & GetData("portaluser", "fullname", "portaluserID", l_rstLoggedEvents("portaluserID")) & ", Company: " & GetData("company", "name", "companyID", l_rstLoggedEvents("companyID")) & vbCrLf
                emailmessage = emailmessage & "Upload Completed: " & Now & vbCrLf
                emailmessage = emailmessage & "Folder Delivered: " & GetData("events", "clipfilename", "eventID", l_rstLoggedEvents("clipID")) & vbCrLf
                emailmessage = emailmessage & "Title: " & GetData("events", "eventtitle", "eventID", l_rstLoggedEvents("clipID"))
                If GetData("events", "eventsubtitle", "eventID", l_rstLoggedEvents("clipID")) <> "" Then emailmessage = emailmessage & ", Subtitle: " & GetData("events", "eventsubtitle", "eventID", l_rstLoggedEvents("clipID"))
                If GetData("events", "eventepisode", "eventID", l_rstLoggedEvents("clipID")) <> "" Then emailmessage = emailmessage & ", Episode: " & GetData("events", "eventepisode", "eventID", l_rstLoggedEvents("clipID")) & vbCrLf
                l_lngClipID = l_rstLoggedEvents("clipID")
                If InStr(l_strCetaClientCode, "/showuploadedfolders") > 0 Then
                    SetData "events", "clipformat", "eventID", l_lngClipID, "Folder"
                    SetData "events", "hidefromweb", "eventID", l_lngClipID, 0
                End If
                l_lngLibraryID = GetData("events", "libraryID", "eventID", l_lngClipID)
                If l_lngLibraryID <> 0 Then
                    SQL = "SELECT * FROM fasp_sessions WHERE cookie = '" & l_rstLoggedEvents("asperatoken") & "' OR token = '" & l_rstLoggedEvents("asperatoken") & "' ORDER BY created_at;"
                    Set l_rstSession = New ADODB.Recordset
                    l_rstSession.Open SQL, cnn2, 3, 3
                    If Not l_rstSession.EOF Then
                        l_rstSession.MoveFirst
                        l_curFolderSize = 0
                        Do While Not l_rstSession.EOF
                            If GetData("webdeliveryportal", "webdeliveryportalID", "asperatoken", l_rstSession("session_id")) = 0 Then
                                'New Upload detected
                                l_rstAsperaFiles.Open "SELECT * FROM fasp_files WHERE session_id = '" & l_rstSession("session_id") & "';", cnn2, 3, 3
                                If Not l_rstAsperaFiles.EOF Then
                                    l_rstAsperaFiles.MoveFirst
                                    l_strDatedSubfolder = Format(Now, "YYYYMMDDHHNNSS")
                                    Do While Not l_rstAsperaFiles.EOF
                                        fileName = l_rstAsperaFiles("file_basename")
                                        If GetData("events", "libraryID", "eventID", l_rstLoggedEvents("clipID")) = 447261 Then
                                            TempResult = Mid(l_rstAsperaFiles("file_fullpath"), 15)
                                        Else
                                            TempResult = Mid(l_rstAsperaFiles("file_fullpath"), 53)
                                        End If
                                        Count = InStr(TempResult, fileName)
                                        If Count > 2 Then
                                            AltLocation = FlipSlash(Left(TempResult, Count - 2))
                                        Else
                                            AltLocation = ""
                                        End If
                                        If Len(AltLocation) > Len(Trim(l_rstLoggedEvents("companyID")) & "\") Then
                                            l_strAltLocationTail = Mid(AltLocation, Len(Trim(l_rstLoggedEvents("companyID"))) + 2)
                                        Else
                                            l_strAltLocationTail = ""
                                        End If
                                        
                                        'Then carry on and log the file in and update it's MediaInfo.
                                        SQL = "SELECT eventID, bigfilesize, soundlay FROM events WHERE companyID = " & l_rstLoggedEvents("companyID") & " AND libraryID = " & GetData("events", "libraryID", "eventID", l_rstLoggedEvents("clipID")) & " AND altlocation = '" & AltLocation & "' AND clipfilename = '" & fileName & "' AND system_deleted = 0;"
                                        Set l_rstID = New ADODB.Recordset
                                        l_rstID.Open SQL, cnn, 3, 3
                                        If l_rstID.EOF Then
                                            l_rstID.Close
                                            LastID = CopyEventToLibraryID(l_rstLoggedEvents("clipID"), GetData("events", "libraryID", "eventID", l_rstLoggedEvents("clipID")))
                                            SetData "events", "altlocation", "eventID", LastID, AltLocation
                                            SetData "events", "clipfilename", "eventID", LastID, fileName
                                            Count = 0
                                            While InStr(Mid(fileName, Count + 1), ".") > 0
                                                Count = Count + InStr(Mid(fileName, Count + 1), ".")
                                            Wend
                                            If Count > 0 Then
                                                SetData "events", "clipreference", "eventID", LastID, Left(fileName, Count - 1)
                                            Else
                                                SetData "events", "clipreference", "eventID", LastID, fileName
                                            End If
                                            SetData "events", "internalreference", "eventID", LastID, GetNextSequence("internalreference")
                                            SetData "events", "cdate", "eventID", LastID, FormatSQLDate(Now)
                                            SetData "events", "cuser", "eventID", LastID, "ASPERA"
                                            SetData "events", "mdate", "eventID", LastID, FormatSQLDate(Now)
                                            SetData "events", "muser", "eventID", LastID, "ASPERA"
                                            SetData "events", "soundlay", "eventID", LastID, FormatSQLDate(Now)
                                            
                                            l_strPathToFile = GetData("library", "subtitle", "libraryID", GetData("events", "libraryID", "eventID", l_rstLoggedEvents("clipID")))
                                            l_strPathToFile = l_strPathToFile & "\" & AltLocation
                                            l_strPathToFile = l_strPathToFile & "\" & fileName
                                            API_OpenFile Replace(l_strPathToFile, "\\", "\\?\UNC\"), l_lngFileHandle, l_curFileSize
                                            API_CloseFile l_lngFileHandle
                                            SetData "events", "bigfilesize", "eventID", LastID, l_curFileSize
                                            l_curFolderSize = l_curFolderSize + l_curFileSize
                                            
                                            MediaData = GetMediaInfoOnFile(Replace(l_strPathToFile, "\\", "\\?\UNC\"))
                                            If MediaData.txtFormat = "TTML" Then
                                                SetData "events", "clipformat", "eventID", LastID, "ITT Subtitles"
'                                            ElseIf MediaData.txtFormat = "JPEG Image" Or MediaData.txtFormat = "PNG Image" Or MediaData.txtFormat = "BMP Image" Or MediaData.txtFormat = "GIF Image" Then
'                                                SetData "events", "clipformat", "eventID", LastID, MediaData.txtFormat
'                                                If getImgDim(l_strPathToFile, l_ImageDimensions, Temp) = True Then
'                                                    SetData "events", "clipverticalpixels", "eventID", LastID, l_ImageDimensions.height
'                                                    SetData "events", "cliphorizontalpixels", "eventID", LastID, l_ImageDimensions.width
'                                                End If
                                            ElseIf MediaData.txtFormat <> "" Then
                                                SetData "events", "clipformat", "eventID", LastID, MediaData.txtFormat
                                                SetData "events", "clipaudiocodec", "eventID", LastID, MediaData.txtAudioCodec
                                                SetData "events", "clipframerate", "eventID", LastID, MediaData.txtFrameRate
                                                SetData "events", "cbrvbr", "eventID", LastID, MediaData.txtCBRVBR
                                                SetData "events", "cliphorizontalpixels", "eventID", LastID, MediaData.txtWidth
                                                SetData "events", "clipverticalpixels", "eventID", LastID, MediaData.txtHeight
                                                SetData "events", "videobitrate", "eventID", LastID, MediaData.txtVideoBitrate
                                                SetData "events", "interlace", "eventID", LastID, MediaData.txtInterlace
                                                SetData "events", "timecodestart", "eventID", LastID, MediaData.txtTimecodestart
                                                SetData "events", "timecodestop", "eventID", LastID, MediaData.txtTimecodestop
                                                SetData "events", "fd_length", "eventID", LastID, MediaData.txtDuration
                                                SetData "events", "audiobitrate", "eventID", LastID, MediaData.txtAudioBitrate
                                                SetData "events", "trackcount", "eventID", LastID, MediaData.txtTrackCount
                                                SetData "events", "channelcount", "eventID", LastID, MediaData.txtChannelCount
                                                If Val(MediaData.txtAudioBitrate) + Val(MediaData.txtVideoBitrate) <> 0 Then
                                                    SetData "events", "clipbitrate", "eventID", LastID, Val(MediaData.txtAudioBitrate) + Val(MediaData.txtVideoBitrate)
                                                Else
                                                    SetData "events", "clipbitrate", "eventID", LastID, Val(MediaData.txtOverallBitrate)
                                                End If
                                                SetData "events", "aspectratio", "eventID", LastID, MediaData.txtAspect
                                                If MediaData.txtAspect = "4:3" Then
                                                    SetData "events", "fourbythreeflag", "eventID", LastID, 1
                                                Else
                                                    SetData "events", "fourbythreeflag", "eventID", LastID, 0
                                                End If
                                                SetData "events", "geometriclinearity", "eventID", LastID, MediaData.txtGeometry
                                                SetData "events", "clipcodec", "eventID", LastID, MediaData.txtVideoCodec
                                                SetData "events", "colorspace", "eventID", LastID, MediaData.txtColorSpace
                                                SetData "events", "chromasubsampling", "eventID", LastID, MediaData.txtChromaSubsmapling
                                                If Trim(" " & GetData("events", "eventtype", "eventID", LastID)) = "" Then SetData "events", "eventtype", "eventID", LastID, GetData("xref", "format", "description", MediaData.txtVideoCodec)
                                                SetData "events", "mediastreamtype", "eventID", LastID, MediaData.txtStreamType
                                                If MediaData.txtSeriesTitle <> "" Then
                                                    SetData "events", "eventtitle", "eventID", LastID, MediaData.txtSeriesTitle
                                                    SetData "events", "eventseries", "eventID", LastID, MediaData.txtSeriesNumber
                                                    SetData "events", "eventsubtitle", "eventID", LastID, MediaData.txtProgrammeTitle
                                                    SetData "events", "eventepisode", "eventID", LastID, MediaData.txtEpisodeNumber
                                                    SetData "events", "notes", "eventID", LastID, MediaData.txtSynopsis
                                                End If
                                                SetData "events", "lastmediainfoquery", "eventID", LastID, FormatSQLDate(Now)
                                            Else
                                                Select Case UCase(Right(fileName, 3))
                                                    Case "ISO"
                                                        SetData "events", "clipformat", "eventID", LastID, "DVD ISO Image"
                                                    Case "PDF"
                                                        SetData "events", "clipformat", "eventID", LastID, "PDF File"
                                                    Case "PSD"
                                                        SetData "events", "clipformat", "eventID", LastID, "PSD Still Image"
                                                    Case "SCC"
                                                        SetData "events", "clipformat", "eventID", LastID, "SCC File (EIA 608)"
                                                    Case "TIF"
                                                        SetData "events", "clipformat", "eventID", LastID, "TIF Still Image"
                                                    Case "DOC"
                                                        SetData "events", "clipformat", "eventID", LastID, "Word File"
                                                    Case "XLS"
                                                        SetData "events", "clipformat", "eventID", LastID, "XLS File"
                                                    Case "XML"
                                                        SetData "events", "clipformat", "eventID", LastID, "XML File"
                                                    Case "RAR"
                                                        SetData "events", "clipformat", "eventID", LastID, "RAR Archive"
                                                    Case "ZIP"
                                                        SetData "events", "clipformat", "eventID", LastID, "ZIP Archive"
                                                    Case "TAR"
                                                        SetData "events", "clipformat", "eventID", LastID, "TAR Archive"
                                                    Case Else
                                                        SetData "events", "clipformat", "eventID", LastID, "Other"
                                                End Select
                                            End If
                                            cnn.Execute "INSERT INTO eventhistory (eventID, datesaved, username, description) VALUES (" & LastID & ", getdate(), 'ASPERA', 'Aspera Upload');"
                                        Else
                                            LastID = l_rstID("eventID")
                                            l_rstID("bigfilesize") = Trim(" " & l_rstAsperaFiles("size"))
                                            l_rstID("soundlay") = FormatSQLDate(Now)
                                            l_rstID.Update
                                            l_rstID.Close
                                            l_curFolderSize = l_curFolderSize + l_rstAsperaFiles("size")
                                        End If
                                        
                                        If InStr(l_strCetaClientCode, "/invisibleuploads") <= 0 Then
                                            SetData "events", "hidefromweb", "eventID", LastID, 0
                                        End If
                                        
                                        SQL = "INSERT INTO webdeliveryportal (portaluserID, fullname, additionalinfo, companyID, timewhen, clipid, clipdetails, accesstype, finalstatus, asperatoken, processafterupload) VALUES ("
                                        SQL = SQL & l_rstLoggedEvents("portaluserID") & ", "
                                        SQL = SQL & "'" & QuoteSanitise(l_rstLoggedEvents("fullname")) & "', "
                                        SQL = SQL & "'" & QuoteSanitise(l_rstLoggedEvents("additionalinfo")) & "', "
                                        SQL = SQL & l_rstLoggedEvents("companyID") & ", "
                                        SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                                        SQL = SQL & LastID & ", "
                                        SQL = SQL & "'" & QuoteSanitise(fileName) & "', "
                                        SQL = SQL & "'Aspera Upload within Folder', "
                                        SQL = SQL & "'Completed', "
                                        SQL = SQL & "'" & l_rstSession("session_id") & "', "
                                        SQL = SQL & "0);"
                                        
                                        cnn.Execute SQL
                                        
                                        SendSMTPMail g_strUserEmailAddress, g_strUserEmailName, "File Upload within a Folder Upload", fileName & " has been uploaded and is on the " & GetData("library", "barcode", "libraryID", GetData("events", "libraryID", "eventID", l_rstLoggedEvents("clipID"))) & " in the " & AltLocation & " folder.", "", False, g_strManagerEmailAddress, g_strManagerEmailName, g_strAdministratorEmailAddress
                                        
                                        l_strClipFormat = GetData("events", "clipformat", "eventID", LastID)
                                        l_strFilename = GetData("events", "clipfilename", "eventID", LastID)
                                        l_lngClipBitRate = Val(Trim(" " & GetData("events", "clipbitrate", "eventID", LastID)))
                                        If l_strFilename = ".DS_Store" Then
                                            'Just get rid of it :-)
                                            SQL = "INSERT INTO event_file_request (eventID, sourcelibraryID, event_file_Request_typeID, NewLibraryID, RequestName, Requestdate) VALUES ("
                                            SQL = SQL & l_lngClipID & ", "
                                            SQL = SQL & GetData("events", "libraryID", "eventID", l_lngClipID) & ", "
                                            SQL = SQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Delete") & ", "
                                            SQL = SQL & GetData("events", "libraryID", "eventID", l_lngClipID) & ", "
                                            SQL = SQL & "'Aspera', Getdate());"
                                            Debug.Print SQL
                                            cnn.Execute SQL
                                        End If
                                        If InStr(l_strCetaClientCode, "/MoveAfterUpload") > 0 And l_strFilename <> ".DS_Store" Then
                                            'There needs to be a move of uploaded file to another store.
                                            l_strNewAltLocation = "ceta\Ceta\" & l_rstLoggedEvents("companyID") & "\Uploaded"
                                            If InStr(l_strCetaClientCode, "/MoveAfterUploadFolder") > 0 Then
                                                l_strNewAltLocationField = Mid(l_strCetaClientCode, InStr(l_strCetaClientCode, "/MoveAfterUploadFolder") + 23)
                                                If InStr(l_strNewAltLocationField, "/") > 0 Then
                                                    l_strNewAltLocationField = Trim(" " & Left(l_strNewAltLocationField, InStr(l_strNewAltLocationField, "/") - 1))
                                                End If
                                                If Len(l_strNewAltLocationField) > 0 Then
                                                    If Left(l_strNewAltLocationField, 1) = "{" Then
                                                        l_strNewAltLocationField = Mid(l_strNewAltLocationField, 2, Len(l_strNewAltLocationField) - 2)
                                                        If Trim(" " & GetData("events", l_strNewAltLocationField, "eventID", l_lngClipID)) <> "" Then
                                                            l_strNewAltLocation = l_strNewAltLocation & "\" & Replace(Replace(Replace(Replace(Trim(" " & GetData("events", l_strNewAltLocationField, "eventID", l_lngClipID)), " ", "_"), ":", "_"), "/", "_"), "\", "_") & "\" & l_strDatedSubfolder
                                                        End If
                                                    Else
                                                        l_strNewAltLocation = l_strNewAltLocation & "\" & l_strNewAltLocationField & "\" & l_strDatedSubfolder
                                                    End If
                                                End If
                                            End If
                                            If l_strAltLocationTail <> "" Then
                                                l_strNewAltLocation = l_strNewAltLocation & "\" & l_strAltLocationTail
                                            End If
                                            Sleep 60000
                                            SQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, NewLibraryID, NewAltLocation, bigfilesize, RequestDate, RequestName, Urgent) VALUES ("
                                            SQL = SQL & LastID & ", "
                                            SQL = SQL & GetData("events", "libraryID", "eventID", LastID) & ", "
                                            SQL = SQL & GetData("Event_File_Request_Type", "Event_File_Request_TypeID", "description", "Move") & ", "
                                            SQL = SQL & Val(Mid(l_strCetaClientCode, InStr(l_strCetaClientCode, "/MoveAfterUpload") + 17)) & ", "
                                            SQL = SQL & "'" & l_strNewAltLocation & "', "
                                            SQL = SQL & l_curFileSize & ", "
                                            SQL = SQL & "Getdate(), 'Aspera', 1)"
                                            Debug.Print SQL
                                            cnn.Execute SQL
                                        End If
                                        If InStr(l_strCetaClientCode, "/MakeTrackerEntry") > 0 And l_strFilename <> ".DS_Store" Then
                                            'Make a Generic Tracker Line for this file, and possibly add a Workflow Variant ID to it also.
                                            If InStr(l_strCetaClientCode, "/MakeTrackerWorkflowVariantID") > 0 Then
                                                l_lngCounter = InStr(l_strCetaClientCode, "/MakeTrackerWorkflowVariantID")
                                                l_lngWorkflowVariantID = Val(Mid(l_strCetaClientCode, l_lngCounter + 30))
                                            Else
                                                l_lngWorkflowVariantID = 0
                                            End If
                                            SQL = "INSERT INTO tracker_item (companyID, originaleventID, itemreference, itemfilename" & IIf(l_lngWorkflowVariantID <> 0, ", WorkflowVariantID", "") & ") VALUES ("
                                            SQL = SQL & l_rstLoggedEvents("companyID") & ", "
                                            SQL = SQL & LastID & ", "
                                            SQL = SQL & "'" & QuoteSanitise(GetData("events", "clipreference", "eventID", LastID)) & "', "
                                            SQL = SQL & "'" & QuoteSanitise(GetData("events", "clipfilename", "eventID", LastID)) & "'"
                                            If l_lngWorkflowVariantID <> 0 Then
                                                SQL = SQL & ", " & l_lngWorkflowVariantID
                                            End If
                                            SQL = SQL & ");"
                                            Debug.Print SQL
                                            cnn.Execute SQL
                                        End If
                                        If GetData("portaluser", "CopyFilesAfterUpload", "portaluserID", l_rstLoggedEvents("portaluserID")) <> 0 And l_strFilename <> ".DS_Store" Then
                                            If Trim(" " & GetData("portaluser", "CopyFilesIfExtender", "portaluserID", l_rstLoggedEvents("portaluserID"))) <> "" Then
                                                If UCase(Right(fileName, Len(GetData("portaluser", "CopyFilesIfExtender", "portaluserID", l_rstLoggedEvents("portaluserID"))))) = UCase((GetData("portaluser", "CopyFilesIfExtender", "portaluserID", l_rstLoggedEvents("portaluserID")))) Then
                                                    SQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, NewLibraryID, NewAltLocation, RequestDate) VALUES ("
                                                    SQL = SQL & LastID & ", "
                                                    SQL = SQL & GetData("events", "libraryID", "eventID", LastID) & ", "
                                                    SQL = SQL & GetData("Event_File_Request_Type", "Event_File_Request_TypeID", "description", "Copy") & ", "
                                                    SQL = SQL & GetData("portaluser", "CopyFilesToLibraryID", "portaluserID", l_rstLoggedEvents("portaluserID")) & ", "
                                                    SQL = SQL & "'" & GetData("portaluser", "CopyFilesToFolder", "portaluserID", l_rstLoggedEvents("portaluserID")) & IIf(l_strAltLocationTail <> "", "\" & l_strAltLocationTail, "") & "', Getdate())"
                                                    cnn.Execute SQL
                                                End If
                                            Else
                                                SQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, NewLibraryID, NewAltLocation, RequestDate) VALUES ("
                                                SQL = SQL & LastID & ", "
                                                SQL = SQL & GetData("events", "libraryID", "eventID", LastID) & ", "
                                                SQL = SQL & GetData("Event_File_Request_Type", "Event_File_Request_TypeID", "description", "Copy") & ", "
                                                SQL = SQL & GetData("portaluser", "CopyFilesToLibraryID", "portaluserID", l_rstLoggedEvents("portaluserID")) & ", "
                                                SQL = SQL & "'" & GetData("portaluser", "CopyFilesToFolder", "portaluserID", l_rstLoggedEvents("portaluserID")) & IIf(l_strAltLocationTail <> "", "\" & l_strAltLocationTail, "") & "', Getdate())"
                                                cnn.Execute SQL
                                            End If
                                        End If
                                        If (l_rstLoggedEvents("processafterupload") = 2 Or (l_strClipFormat = "Flash MP4" And l_lngClipBitRate < 3000)) And l_lngLibraryID <> 722001 And l_strFilename <> ".DS_Store" Then
                                            'Check whether there is already a file on the flashstore of this name and company, and if not, move it.
                                            If Trim(" " & GetDataSQL("SELECT eventID FROM events WHERE libraryID = 276356 AND altlocation = '" & GetData("events", "companyID", "eventID", l_lngClipID) & "' AND clipfilename = '" & l_strFilename & "' AND companyID = " & l_rstLoggedEvents("companyID") & " AND system_deleted = 0")) = "" Then
                                                'Move it to the Flashstore
                                                If Right(LCase(l_strFilename), 4) = ".mp4" Or Right(LCase(l_strFilename), 4) = ".flv" Or Right(LCase(l_strFilename), 4) = ".mov" Then
                                                    SetData "events", "clipsoundformat", "eventID", l_lngClipID, "UK_3_" & GetData("company", "marketcode", "companyID", l_rstLoggedEvents("companyID"))
                                                    SQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, bigfilesize, RequestName, RequesterEmail) VALUES ("
                                                    SQL = SQL & LastID & ", "
                                                    SQL = SQL & l_lngLibraryID & ", "
                                                    SQL = SQL & GetData("event_file_request_type", "event_file_Request_typeID", "description", "Move") & ", "
                                                    SQL = SQL & "722001, "
                                                    SQL = SQL & "'" & GetData("events", "companyID", "eventID", LastID) & "', "
                                                    SQL = SQL & l_curFileSize & ", "
                                                    SQL = SQL & "'ContentDelivery', NULL);"
                                                    Debug.Print SQL
                                                    cnn.Execute SQL
                                                End If
                                                l_blnMoveToFlashstore = True
                                            Else
                                                l_blnCannotMoveToFlashstore = True
                                            End If
                                        End If
                                        l_rstAsperaFiles.MoveNext
                                    Loop
                                End If
                                l_rstAsperaFiles.Close
                            End If
                            l_rstSession.MoveNext
                        Loop
                    End If
                    l_rstSession.Close
                    If l_curFolderSize > 0 Then
                        If Int(l_curFolderSize / 1024 / 1024 / 1024 + 0.999) > 0 Then
                            emailmessage = emailmessage & vbCrLf & "Total Folder Size = " & Int(l_curFolderSize / 1024 / 1024 / 1024 + 0.999) & "GB" & vbCrLf
                        ElseIf Int(l_curFolderSize / 1024 / 1024 + 0.999) > 0 Then
                            emailmessage = emailmessage & vbCrLf & "Total Folder Size = " & Int(l_curFolderSize / 1024 / 1024 + 0.999) & "MB" & vbCrLf
                        ElseIf Int(l_curFolderSize / 1024 + 0.999) > 0 Then
                            emailmessage = emailmessage & vbCrLf & "Total Folder Size = " & Int(l_curFolderSize / 1024 + 0.999) & "KB" & vbCrLf
                        Else
                            emailmessage = emailmessage & vbCrLf & "Total Folder Size = " & Int(l_curFolderSize) & "Bytes" & vbCrLf
                        End If
                    Else
                        emailmessage = emailmessage & vbCrLf & "Total Folder Size = " & Int(l_curFolderSize) & "Bytes" & vbCrLf
                    End If
                    SetData "jobdetailunbilled", "quantity", "asperatoken", l_rstLoggedEvents("asperatoken"), Int(l_curFolderSize / 1024 / 1024 / 1024 + 0.999)
                    Debug.Print emailmessage
                    l_rstLoggedEvents("bigfilesize") = l_curFolderSize
                    l_rstLoggedEvents.Update
                    
                    If InStr(l_strCetaClientCode, "/emailgroupuploads") > 0 Then
                        l_strSQL = "SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", l_rstLoggedEvents("companyID")) & "' AND trackermessageID = 39;"
                        Set l_rsDADCTracker = New ADODB.Recordset
                        l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
                        If l_rsDADCTracker.RecordCount > 0 Then
                            l_rsDADCTracker.MoveFirst
                            While Not l_rsDADCTracker.EOF
                                SendSMTPMail l_rsDADCTracker("email"), l_rsDADCTracker("fullname"), "Aspera Upload Completed", emailmessage, "", True, "", "", g_strAdministratorEmailAddress
                                l_rsDADCTracker.MoveNext
                            Wend
                        End If
                        l_rsDADCTracker.Close
                        If Trim(" " & l_rstLoggedEvents("additionalinfo")) <> "" Then
                            SendSMTPMail l_rstLoggedEvents("additionalinfo"), l_rstLoggedEvents("fullname"), "Aspera Upload Completed", emailmessage, "", True, "", "", g_strAdministratorEmailAddress
                        Else
                            SendSMTPMail GetData("portaluser", "email", "portaluserID", l_rstLoggedEvents("portaluserID")), l_rstLoggedEvents("fullname"), "Aspera Upload Completed", emailmessage, "", True, "", "", g_strAdministratorEmailAddress
                        End If
                        Set l_rsDADCTracker = Nothing
                    ElseIf InStr(l_strCetaClientCode, "/emailuploads") > 0 Then
                    l_strCCaddress = ""
                        Set l_rsCC = New ADODB.Recordset
                        l_rsCC.Open "SELECT * FROM portaluserCC WHERE portaluserID = " & l_rstLoggedEvents("portaluserID"), cnn, 3, 3
                        If l_rsCC.RecordCount > 0 Then
                            l_rsCC.MoveFirst
                            Do While Not l_rsCC.EOF
                                If l_strCCaddress <> "" Then l_strCCaddress = l_strCCaddress & "; "
                                l_strCCaddress = l_strCCaddress & l_rsCC("ccaddress")
                                l_rsCC.MoveNext
                            Loop
                        End If
                        l_rsCC.Close
                        Set l_rsCC = Nothing
                        If Trim(" " & l_rstLoggedEvents("additionalinfo")) <> "" Then
                            SendSMTPMail l_rstLoggedEvents("additionalinfo"), l_rstLoggedEvents("fullname"), "Aspera Upload Completed", emailmessage, "", True, l_strCCaddress, ""
                            SendSMTPMail GetData("portaluser", "email", "portaluserID", l_rstLoggedEvents("portaluserID")), l_rstLoggedEvents("fullname"), "Aspera Upload Completed", emailmessage, "", True, GetData("portaluser", "administratoremail", "portaluserID", l_rstLoggedEvents("portaluserID")), "", g_strAdministratorEmailAddress
                        Else
                            SendSMTPMail GetData("portaluser", "email", "portaluserID", l_rstLoggedEvents("portaluserID")), l_rstLoggedEvents("fullname"), "Aspera Upload Completed", emailmessage, "", True, GetData("portaluser", "administratoremail", "portaluserID", l_rstLoggedEvents("portaluserID")) & IIf(l_strCCaddress <> "", ";" & l_strCCaddress, ""), "", g_strAdministratorEmailAddress
                        End If
                        If Trim(" " & GetData("company", "email", "companyID", l_rstLoggedEvents("companyID"))) <> "" Then
                            SendSMTPMail GetData("company", "email", "companyID", l_rstLoggedEvents("companyID")), "Upload Notifications List", "Aspera Upload Completed", emailmessage, "", True, Trim(" " & GetData("company", "jcacontactemail", "companyID", l_rstLoggedEvents("companyID"))), "", g_strAdministratorEmailAddress
                        End If
                    Else
                        l_strCCaddress = ""
                        Set l_rsCC = New ADODB.Recordset
                        l_rsCC.Open "SELECT * FROM portaluserCC WHERE portaluserID = " & l_rstLoggedEvents("portaluserID"), cnn, 3, 3
                        If l_rsCC.RecordCount > 0 Then
                            l_rsCC.MoveFirst
                            Do While Not l_rsCC.EOF
                                If l_strCCaddress <> "" Then l_strCCaddress = l_strCCaddress & "; "
                                l_strCCaddress = l_strCCaddress & l_rsCC("ccaddress")
                                l_rsCC.MoveNext
                            Loop
                        End If
                        l_rsCC.Close
                        Set l_rsCC = Nothing
                        If Trim(" " & l_rstLoggedEvents("additionalinfo")) <> "" Then
                            SendSMTPMail l_rstLoggedEvents("additionalinfo"), l_rstLoggedEvents("fullname"), "Aspera Upload Completed", emailmessage, "", True, "", "", g_strAdministratorEmailAddress
                        Else
                            SendSMTPMail GetData("portaluser", "email", "portaluserID", l_rstLoggedEvents("portaluserID")), l_rstLoggedEvents("fullname"), "Aspera Upload Completed", emailmessage, "", True, l_strCCaddress, "", g_strAdministratorEmailAddress
                        End If
                        If Trim(" " & GetData("company", "email", "companyID", l_rstLoggedEvents("companyID"))) <> "" Then
                            SendSMTPMail g_strUserEmailAddress, g_strUserEmailName, "Aspera Upload Completed", emailmessage, "", True, Trim(" " & GetData("company", "jcacontactemail", "companyID", l_rstLoggedEvents("companyID"))), "", g_strAdministratorEmailAddress
                        End If
                    End If
                    
                    If InStr(l_strCetaClientCode, "/showuploadedfolders") > 0 Then
                        SQL = "UPDATE events SET bigfilesize = " & l_curFolderSize & ", soundlay = '" & FormatSQLDate(Now) & "' WHERE eventID = " & l_rstLoggedEvents("clipID") & ";"
                    Else
                        SQL = "UPDATE events SET system_deleted = 1 WHERE eventID = " & l_rstLoggedEvents("clipID") & ";"
                    End If
                    cnn.Execute SQL
                    Set l_rstSession = Nothing
                End If
            ElseIf l_strResult = "completed" And l_rstLoggedEvents("accesstype") <> "Aspera Upload" Then
                l_rstLoggedEvents("bigfilesize") = GetData("events", "bigfilesize", "eventID", l_rstLoggedEvents("clipID"))
                l_rstLoggedEvents.Update
                l_strCetaClientCode = GetData("company", "cetaclientcode", "companyID", l_rstLoggedEvents("companyID"))
                If InStr(l_strCetaClientCode, "/trackerfiledeliveries") > 0 Then
                    l_lngTrackerID = Val(Trim(" " & GetDataSQL("SELECT tracker_itemID FROM tracker_item WHERE companyID = " & l_rstLoggedEvents("companyID") & " AND itemreference = '" & GetData("events", "clipreference", "eventID", l_rstLoggedEvents("clipID")) & "'")))
                    If l_lngTrackerID <> 0 Then
                        l_strStageField = Trim(" " & GetDataSQL("SELECT itemfield FROM tracker_itemchoice WHERE companyID = " & l_rstLoggedEvents("companyID") & " AND itemheading = 'Delivered'"))
                        If l_strStageField <> "" Then
                            SetData "tracker_item", l_strStageField, "tracker_itemID", l_lngTrackerID, Format(Now, "YYYY-MM-DD HH:NN:SS")
                            SetData "tracker_item", "mdate", "tracker_itemID", l_lngTrackerID, Format(Now, "YYYY-MM-DD HH:NN:SS")
                            SetData "tracker_item", "muser", "tracker_itemID", l_lngTrackerID, "Aspera"
                            Set l_rstItemChoice = New ADODB.Recordset
                            If Val(Trim(" " & GetDataSQL("SELECT stage1conclusion FROM tracker_itemchoice WHERE companyID = " & l_rstLoggedEvents("companyID") & " AND itemheading = 'Delivered'"))) <> 0 _
                            Or Val(Trim(" " & GetDataSQL("SELECT stage1NOTconclusion FROM tracker_itemchoice WHERE companyID = " & l_rstLoggedEvents("companyID") & " AND itemheading = 'Delivered'"))) <> 0 Then
                                l_blnConclusionValue = True
                                l_rstItemChoice.Open "SELECT * FROM tracker_itemchoice WHERE companyID = " & l_rstLoggedEvents("companyID") & " AND stage1conclusion <> 0", cnn, 3, 3
                                If l_rstItemChoice.RecordCount > 0 Then
                                    l_rstItemChoice.MoveFirst
                                    Do While Not l_rstItemChoice.EOF
                                        If IsNull(GetData("tracker_item", l_rstItemChoice("itemfield"), "tracker_itemID", l_lngTrackerID, True)) Then
                                            l_blnConclusionValue = False
                                        End If
                                        l_rstItemChoice.MoveNext
                                    Loop
                                End If
                                l_rstItemChoice.Close
                                l_rstItemChoice.Open "SELECT * FROM tracker_itemchoice WHERE companyID = " & l_rstLoggedEvents("companyID") & " AND stage1NOTconclusion <> 0", cnn, 3, 3
                                If l_rstItemChoice.RecordCount > 0 Then
                                    l_rstItemChoice.MoveFirst
                                    Do While Not l_rstItemChoice.EOF
                                        If Not IsNull(GetData("tracker_item", l_rstItemChoice("itemfield"), "tracker_itemID", l_lngTrackerID, True)) Then
                                            l_blnConclusionValue = False
                                        End If
                                        l_rstItemChoice.MoveNext
                                    Loop
                                End If
                                l_rstItemChoice.Close
                                SetData "tracker_item", "conclusionfield1", "tracker_itemID", l_lngTrackerID, IIf(l_blnConclusionValue = True, 1, 0)
                            End If
                            If Val(Trim(" " & GetDataSQL("SELECT stage2conclusion FROM tracker_itemchoice WHERE companyID = " & l_rstLoggedEvents("companyID") & " AND itemheading = 'Delivered'"))) <> 0 _
                            Or Val(Trim(" " & GetDataSQL("SELECT stage2NOTconclusion FROM tracker_itemchoice WHERE companyID = " & l_rstLoggedEvents("companyID") & " AND itemheading = 'Delivered'"))) <> 0 Then
                                l_blnConclusionValue = True
                                l_rstItemChoice.Open "SELECT * FROM tracker_itemchoice WHERE companyID = " & l_rstLoggedEvents("companyID") & " AND stage2conclusion <> 0", cnn, 3, 3
                                If l_rstItemChoice.RecordCount > 0 Then
                                    l_rstItemChoice.MoveFirst
                                    Do While Not l_rstItemChoice.EOF
                                        If IsNull(GetData("tracker_item", l_rstItemChoice("itemfield"), "tracker_itemID", l_lngTrackerID, True)) Then
                                            l_blnConclusionValue = False
                                        End If
                                        l_rstItemChoice.MoveNext
                                    Loop
                                End If
                                l_rstItemChoice.Close
                                l_rstItemChoice.Open "SELECT * FROM tracker_itemchoice WHERE companyID = " & l_rstLoggedEvents("companyID") & " AND stage2NOTconclusion <> 0", cnn, 3, 3
                                If l_rstItemChoice.RecordCount > 0 Then
                                    l_rstItemChoice.MoveFirst
                                    Do While Not l_rstItemChoice.EOF
                                        If Not IsNull(GetData("tracker_item", l_rstItemChoice("itemfield"), "tracker_itemID", l_lngTrackerID, True)) Then
                                            l_blnConclusionValue = False
                                        End If
                                        l_rstItemChoice.MoveNext
                                    Loop
                                End If
                                l_rstItemChoice.Close
                                SetData "tracker_item", "conclusionfield2", "tracker_itemID", l_lngTrackerID, IIf(l_blnConclusionValue = True, 1, 0)
                            End If
                            If Val(Trim(" " & GetDataSQL("SELECT stage3conclusion FROM tracker_itemchoice WHERE companyID = " & l_rstLoggedEvents("companyID") & " AND itemheading = 'Delivered'"))) <> 0 _
                            Or Val(Trim(" " & GetDataSQL("SELECT stage3NOTconclusion FROM tracker_itemchoice WHERE companyID = " & l_rstLoggedEvents("companyID") & " AND itemheading = 'Delivered'"))) <> 0 Then
                                l_blnConclusionValue = True
                                l_rstItemChoice.Open "SELECT * FROM tracker_itemchoice WHERE companyID = " & l_rstLoggedEvents("companyID") & " AND stage3conclusion <> 0", cnn, 3, 3
                                If l_rstItemChoice.RecordCount > 0 Then
                                    l_rstItemChoice.MoveFirst
                                    Do While Not l_rstItemChoice.EOF
                                        If IsNull(GetData("tracker_item", l_rstItemChoice("itemfield"), "tracker_itemID", l_lngTrackerID, True)) Then
                                            l_blnConclusionValue = False
                                        End If
                                        l_rstItemChoice.MoveNext
                                    Loop
                                End If
                                l_rstItemChoice.Close
                                l_rstItemChoice.Open "SELECT * FROM tracker_itemchoice WHERE companyID = " & l_rstLoggedEvents("companyID") & " AND stage3NOTconclusion <> 0", cnn, 3, 3
                                If l_rstItemChoice.RecordCount > 0 Then
                                    l_rstItemChoice.MoveFirst
                                    Do While Not l_rstItemChoice.EOF
                                        If Not IsNull(GetData("tracker_item", l_rstItemChoice("itemfield"), "tracker_itemID", l_lngTrackerID, True)) Then
                                            l_blnConclusionValue = False
                                        End If
                                        l_rstItemChoice.MoveNext
                                    Loop
                                End If
                                l_rstItemChoice.Close
                                SetData "tracker_item", "conclusionfield3", "tracker_itemID", l_lngTrackerID, IIf(l_blnConclusionValue = True, 1, 0)
                            End If
                            If Val(Trim(" " & GetDataSQL("SELECT stage4conclusion FROM tracker_itemchoice WHERE companyID = " & l_rstLoggedEvents("companyID") & " AND itemheading = 'Delivered'"))) <> 0 _
                            Or Val(Trim(" " & GetDataSQL("SELECT stage4NOTconclusion FROM tracker_itemchoice WHERE companyID = " & l_rstLoggedEvents("companyID") & " AND itemheading = 'Delivered'"))) <> 0 Then
                                l_blnConclusionValue = True
                                l_rstItemChoice.Open "SELECT * FROM tracker_itemchoice WHERE companyID = " & l_rstLoggedEvents("companyID") & " AND stage4conclusion <> 0", cnn, 3, 3
                                If l_rstItemChoice.RecordCount > 0 Then
                                    l_rstItemChoice.MoveFirst
                                    Do While Not l_rstItemChoice.EOF
                                        If IsNull(GetData("tracker_item", l_rstItemChoice("itemfield"), "tracker_itemID", l_lngTrackerID, True)) Then
                                            l_blnConclusionValue = False
                                        End If
                                        l_rstItemChoice.MoveNext
                                    Loop
                                End If
                                l_rstItemChoice.Close
                                l_rstItemChoice.Open "SELECT * FROM tracker_itemchoice WHERE companyID = " & l_rstLoggedEvents("companyID") & " AND stage4NOTconclusion <> 0", cnn, 3, 3
                                If l_rstItemChoice.RecordCount > 0 Then
                                    l_rstItemChoice.MoveFirst
                                    Do While Not l_rstItemChoice.EOF
                                        If Not IsNull(GetData("tracker_item", l_rstItemChoice("itemfield"), "tracker_itemID", l_lngTrackerID, True)) Then
                                            l_blnConclusionValue = False
                                        End If
                                        l_rstItemChoice.MoveNext
                                    Loop
                                End If
                                l_rstItemChoice.Close
                                SetData "tracker_item", "conclusionfield4", "tracker_itemID", l_lngTrackerID, IIf(l_blnConclusionValue = True, 1, 0)
                            End If
                            If Val(Trim(" " & GetDataSQL("SELECT conclusion FROM tracker_itemchoice WHERE companyID = " & l_rstLoggedEvents("companyID") & " AND itemheading = 'Delivered'"))) <> 0 Then
                                l_blnConclusionValue = True
                                l_rstItemChoice.Open "SELECT * FROM tracker_itemchoice WHERE companyID = " & l_rstLoggedEvents("companyID") & " AND conclusion <> 0", cnn, 3, 3
                                If l_rstItemChoice.RecordCount > 0 Then
                                    l_rstItemChoice.MoveFirst
                                    Do While Not l_rstItemChoice.EOF
                                        If IsNull(GetData("tracker_item", l_rstItemChoice("itemfield"), "tracker_itemID", l_lngTrackerID, True)) Then
                                            l_blnConclusionValue = False
                                        End If
                                        l_rstItemChoice.MoveNext
                                    Loop
                                End If
                                l_rstItemChoice.Close
                                SetData "tracker_item", "readytobill", "tracker_itemID", l_lngTrackerID, IIf(l_blnConclusionValue = True, 1, 0)
                            End If
                            Set l_rstItemChoice = Nothing
                        End If
                    End If
                End If
                If InStr(l_strCetaClientCode, "/emailskibblydownloads") > 0 Then
                    emailmessage = ""
                    emailmessage = emailmessage & "User: " & GetData("portaluser", "fullname", "portaluserID", l_rstLoggedEvents("portaluserID")) & _
                    ", Company: " & GetData("skibblycustomer", "customername", "skibblycustomerID", GetData("portaluser", "skibblycustomerID", "portaluserID", l_rstLoggedEvents("portaluserID"))) & vbCrLf
                    emailmessage = emailmessage & "Download Completed: " & Now & vbCrLf
                    emailmessage = emailmessage & "File Delivered: " & l_rstLoggedEvents("clipdetails") & vbCrLf
                    SendSMTPMail g_strSkibblyEmailAddress, g_strSkibblyEmailName, "Aspera Download Completed", emailmessage, "", True, g_strUserEmailAddress, g_strUserEmailName, g_strAdministratorEmailAddress
                Else
                    SendSMTPMail g_strUserEmailAddress, g_strUserEmailName, "Aspera Download Completed", "Clip: " & l_rstLoggedEvents("clipdetails") & vbCrLf & "Completed: " & Now, "", True, g_strAdministratorEmailAddress, g_strAdministratorEmailName
                End If
                If InStr(l_strCetaClientCode, "/BBCMGSystem") > 0 Then
                    Counter = InStrRev(GetData("events", "clipreference", "eventID", l_rstLoggedEvents("clipID")), "_")
                    BBCMGClipOrderNumber = Val(Mid(GetData("events", "clipreference", "eventID", l_rstLoggedEvents("clipID")), Counter + 1))
                    If BBCMGClipOrderNumber <> 0 Then
                        SetData "BBCMG_Clip_Order", "Order_Status_ID", "Clip_Order_Id", BBCMGClipOrderNumber, GetData("BBCMG_Order_Status", "Order_Status_Id", "Order_Status", "Delivery Complete")
                        SetData "BBCMG_Clip_Tracker", "DateDeliveryComplete", "Clip_Order_Id", BBCMGClipOrderNumber, Format(Now, "yyyy-mm-dd")
                        SetData "BBCMG_Clip_Tracker", "TimeDeliveryComplete", "Clip_Order_Id", BBCMGClipOrderNumber, Format(Now, "hh:nn:ss")
                        SetData "BBCMG_Clip_Tracker", "ItemReference", "Clip_Order_Id", BBCMGClipOrderNumber, GetData("events", "clipreference", "eventID", l_rstLoggedEvents("clipID"))
                        SetData "BBCMG_Clip_Tracker", "ItemFilename", "Clip_Order_Id", BBCMGClipOrderNumber, GetData("events", "clipfilename", "eventID", l_rstLoggedEvents("clipID"))
                    End If
                End If
                If InStr(l_strCetaClientCode, "/oneshot") > 0 Then
                    Set l_rstPermission = New ADODB.Recordset
                    SQL = "SELECT * FROM portalpermission WHERE eventID = " & l_rstLoggedEvents("clipID") & " AND portaluserID = " & l_rstLoggedEvents("portaluserID") & ";"
                    Debug.Print SQL
                    l_rstPermission.Open SQL, cnn, 3, 3
                    
                    If l_rstPermission.RecordCount > 0 Then
                        
                        SQL = "INSERT INTO portalpermissionexpired (eventID, portaluserID, fullname, mediareference, approval, assignedby, dateassigned, dateexpired) VALUES ("
                        SQL = SQL & l_rstLoggedEvents("clipID") & ", "
                        SQL = SQL & l_rstLoggedEvents("portaluserID") & ", "
                        SQL = SQL & "'" & QuoteSanitise(l_rstPermission("fullname")) & "', "
                        SQL = SQL & "'" & QuoteSanitise(l_rstPermission("mediareference")) & "', "
                        SQL = SQL & "'" & QuoteSanitise(l_rstPermission("approval")) & "', "
                        SQL = SQL & "'" & QuoteSanitise(l_rstPermission("assignedby")) & "', "
                        SQL = SQL & "'" & FormatSQLDate(l_rstPermission("dateassigned")) & "', "
                        SQL = SQL & "getdate());"
                        Debug.Print SQL
                        cnn.Execute SQL
                    
                    End If
                    l_rstPermission.Close
                                        
                    SQL = "DELETE FROM portalpermission WHERE eventID = " & l_rstLoggedEvents("clipID") & " AND portaluserID = " & l_rstLoggedEvents("portaluserID") & ";"
                    Debug.Print SQL
                    cnn.Execute SQL
                    
                    If InStr(l_strCetaClientCode, "/oneshotkill") > 0 Then
                        SQL = "SELECT * FROM portalpermission WHERE eventID = " & l_rstLoggedEvents("clipID") & ";"
                        Debug.Print SQL
                        l_rstPermission.Open SQL, cnn, 3, 3
                        
                        If l_rstPermission.RecordCount <= 0 Then
                            Set rstClip = New ADODB.Recordset
                            rstClip.Open "SELECT * FROM events WHERE eventID = " & l_rstLoggedEvents("clipID") & ";", cnn, 3, 3
                            If rstClip.RecordCount = 1 Then
                                l_lngOldLibraryID = rstClip("libraryID")
                                If l_lngOldLibraryID = 722000 Then
                                    rstClip("hidefromweb") = -1
                                    rstClip.Update
                                    
                                    l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, RequestName, RequesterEmail) VALUES ("
                                    l_strSQL = l_strSQL & l_rstLoggedEvents("clipID") & ", "
                                    l_strSQL = l_strSQL & l_lngOldLibraryID & ", "
                                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Delete") & ", "
                                    l_strSQL = l_strSQL & l_lngOldLibraryID & ", "
                                    l_strSQL = l_strSQL & "'Aspera_Log', Null);"
                                    Debug.Print l_strSQL
                                    cnn.Execute l_strSQL
                                    
                                    l_strEmail = "ClipID: " & l_rstLoggedEvents("clipID") & ", Full Filename: " & l_strFilePath
                                    SendSMTPMail g_strAdministratorEmailAddress, g_strAdministratorEmailName, "Portal Clip Permission Expiry: Clip Delete Request Submitted", l_strEmail, "", True, "", ""
                                    
                                End If
                            End If
                            rstClip.Close
                            Set rstClip = Nothing
                        End If
                    End If
                    
                    Set l_rstPermission = Nothing
                End If
            ElseIf l_strResult = "running" And timewhen < Nowdate Then
                l_rstLoggedEvents("finalstatus") = "error"
                l_rstLoggedEvents.Update
            End If
        ElseIf l_rstLoggedEvents("timewhen") < DateAdd("n", -10, Now) Then
            l_rstLoggedEvents("finalstatus") = "error"
            l_rstLoggedEvents.Update
        End If
        l_rstLoggedEvents.MoveNext
    Loop
End If
l_rstLoggedEvents.Close

End Sub

Public Sub CheckJobDetailUnbilled()

Dim l_lngCounter As Long

SQL = "SELECT * FROM jobdetailunbilled "
SQL = SQL & "WHERE (asperatoken IS NOT NULL AND asperatoken <> '') "
SQL = SQL & "AND finalstatus is null "
SQL = SQL & "OR (finalstatus <> 'completed' "
SQL = SQL & "AND (finalstatus <> 'cancelled' or (finalstatus = 'cancelled' and completeddate > '" & FormatSQLDate(Nowdate) & "')) "
SQL = SQL & "AND (finalstatus <> 'error' OR (finalstatus = 'error' and completeddate > '" & FormatSQLDate(Nowdate) & "')));"

l_rstLoggedEvents.Open SQL, cnn, 3, 3
If l_rstLoggedEvents.RecordCount > 0 Then
    Debug.Print "Jobdetailunbilled " & l_rstLoggedEvents.RecordCount & " records"
    l_lngCounter = 1
    l_rstLoggedEvents.MoveFirst
    While Not l_rstLoggedEvents.EOF
        Debug.Print l_lngCounter
        l_lngCounter = l_lngCounter + 1
        l_strResult = GetAsperaData("fasp_sessions", "status", "token", l_rstLoggedEvents("asperatoken"))
        If l_strResult = "" Then l_strResult = GetAsperaData("fasp_sessions", "status", "cookie", l_rstLoggedEvents("asperatoken"))
        
        If l_strResult = "error" Then
            If Left(l_rstLoggedEvents("description"), 22) = "Portal Aspera Download" Or Left(l_rstLoggedEvents("description"), 21) = "Admin Aspera Download" Then
                SessionID = GetAsperaData("fasp_sessions", "session_id", "token", l_rstLoggedEvents("asperatoken"))
                fileName = GetData("events", "clipfilename", "eventID", l_rstLoggedEvents("clipID"))
                TempResult = GetAsperaData("fasp_files", "status", "session_id", SessionID, "file_basename", QuoteSanitise(fileName))
                If TempResult = "completed" Then l_strResult = TempResult
            End If
        End If
            
        If l_strResult <> "" Then
            timewhen = l_rstLoggedEvents("completeddate")
            If l_strResult = "running" And timewhen < Nowdate Then
                l_rstLoggedEvents("finalstatus") = "error"
                l_rstLoggedEvents.Update
            Else
                l_rstLoggedEvents("finalstatus") = l_strResult
                l_rstLoggedEvents.Update
            End If
        ElseIf l_rstLoggedEvents("completeddate") < DateAdd("n", -10, Now) Then
            l_rstLoggedEvents("finalstatus") = "error"
            l_rstLoggedEvents.Update
        End If
        l_rstLoggedEvents.MoveNext
    Wend
End If
l_rstLoggedEvents.Close

End Sub

Public Function CopyEventToLibraryID(lp_lngEventID As Long, lp_lngLibraryID As Long) As Long

'copy the info from one job to another
Dim l_rstOriginalEvent As ADODB.Recordset
Dim l_rstNewEvent As ADODB.Recordset, l_lngNewEventID As Long
Dim c As ADODB.Connection
Dim l_rstTemp As ADODB.Recordset, l_lngTechrevID As Long

'get the original job's details
Dim l_strSQL As String

Set c = New ADODB.Connection
c.Open g_strCetaConnection

l_strSQL = "INSERT INTO events ("
l_strSQL = l_strSQL & "eventmediatype, libraryID, companyID) VALUES ("
l_strSQL = l_strSQL & "'FILE', " & lp_lngLibraryID & ", 1);SELECT SCOPE_IDENTITY();"

Debug.Print l_strSQL
Set l_rstOriginalEvent = New ADODB.Recordset
Set l_rstOriginalEvent = c.Execute(l_strSQL)

l_lngNewEventID = l_rstOriginalEvent.NextRecordset().Fields(0).Value

l_strSQL = "SELECT * FROM events WHERE eventID = '" & lp_lngEventID & "';"
l_rstOriginalEvent.Open l_strSQL, c, adOpenDynamic, adLockBatchOptimistic

'allow adding of a new job
l_strSQL = "SELECT * FROM events WHERE eventID = '" & l_lngNewEventID & "';"
Set l_rstNewEvent = New ADODB.Recordset
l_rstNewEvent.Open l_strSQL, c, adOpenDynamic, adLockOptimistic

Dim l_intLoop As Integer
Dim l_lngConflict As Long
    
If l_rstOriginalEvent.EOF Then Exit Function

l_rstNewEvent("libraryID") = lp_lngLibraryID

For l_intLoop = 0 To l_rstOriginalEvent.Fields.Count - 1
    If l_rstOriginalEvent.Fields(l_intLoop).Name <> "libraryID" And l_rstOriginalEvent.Fields(l_intLoop).Name <> "eventID" Then
    
        If Not IsNull(l_rstOriginalEvent(l_intLoop)) Then
            l_rstNewEvent(l_intLoop) = l_rstOriginalEvent(l_intLoop)
        End If
    
    End If
Next

l_rstNewEvent("mdate") = Null
l_rstNewEvent("muser") = Null

l_rstNewEvent.Update

'MsgBox g_lngLastID, vbExclamation

'close the original record
l_rstOriginalEvent.Close

'close the new record
l_rstNewEvent.Close

Set l_rstOriginalEvent = Nothing
Set l_rstNewEvent = Nothing

CopyEventToLibraryID = l_lngNewEventID

End Function

Function GetDataSQL(lp_strSQL As String)

On Error GoTo Proc_GetData_Error

Dim l_rstGetData As New ADODB.Recordset
Dim l_conGetData As New ADODB.Connection

l_conGetData.Open g_strCetaConnection
l_rstGetData.Open lp_strSQL, l_conGetData, 3, 3

If Not l_rstGetData.EOF Then
    
    If Not IsNull(l_rstGetData(0)) Then
        GetDataSQL = l_rstGetData(0)
    Else
        Select Case l_rstGetData.Fields(0).Type
        Case adChar, adVarChar, adVarWChar, 201
            GetDataSQL = ""
        Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
            GetDataSQL = 0
        Case adDate, adDBDate, adDBTime, adDBTimeStamp
            GetDataSQL = 0
        Case Else
            GetDataSQL = False
        End Select
    End If

End If

l_rstGetData.Close
Set l_rstGetData = Nothing

l_conGetData.Close
Set l_conGetData = Nothing

Exit Function

Proc_GetData_Error:

GetDataSQL = ""

End Function


