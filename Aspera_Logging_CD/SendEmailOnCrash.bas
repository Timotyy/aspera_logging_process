Attribute VB_Name = "SendEmailOnCrash"
Option Explicit

Const g_strSMTPServer = "jca-aspera.jcatv.co.uk"
Const g_strSMTPUserName = "" ' "aria@freenetname.co.uk"
Const g_strSMTPPassword = "" '"Chorale1"
Const g_strEmailFooter = ""
Const g_strFullUserName = "RRsat Europe Operations"

Public Sub SendEmailOnCrash()

SendSMTPMail "IT_Alerts_UK_Prv@rrmedia.com", "IT Alerts", "Aspera Logging has crashed", "Aspera Logging crashed at " & Now, "", True, "", "", "", True, "uk.ops@rrmedia.com"

End Sub

Public Sub main()

App.LogEvent "Aspera Logging Had Creashed"
SendEmailOnCrash

End Sub

Sub SendSMTPMail(ByVal lp_strEmailToAddress As String, ByVal lp_strEmailToName As String, ByVal lp_strSubject As String, ByVal lp_strBody As String, ByVal lp_strAttachment As String, _
ByVal lp_blnDontShowErrors As Boolean, ByVal lp_strCopyToEmailAddress As String, ByVal lp_strCopyToName As String, Optional ByVal lp_strBCCAddress As String, _
Optional lp_blnNoGlobalFooter As Boolean, Optional lp_strEmailFromAddress As String)

On Error GoTo SentSMTPMail_Error

If g_strSMTPServer = "" Then
    If lp_blnDontShowErrors <> True Then MsgBox "There is no SMTP server set up. Please configure one before trying to send emails", vbExclamation
    Exit Sub
End If

Dim poSendmail As clsSendMail

Set poSendmail = New clsSendMail

If Not poSendmail.IsValidEmailAddress(lp_strEmailToAddress) Then
    If lp_blnDontShowErrors <> True Then MsgBox "This is not a valid email address. Unable to send mail", vbExclamation
    GoTo Close_SMTP
End If

'If g_optTrySMTPPing = 1 Then
'    If Not poSendmail.Ping(g_strSMTPServer) Then
'        If lp_blnDontShowErrors <> True Then MsgBox "Can not ping the SMTP server. Unable to send mail", vbExclamation
'        GoTo Close_SMTP
'    End If
'End If
'

If lp_blnNoGlobalFooter = True Then
    'Nothing
Else
    lp_strBody = lp_strBody & vbCrLf & vbCrLf & g_strEmailFooter
End If

Screen.MousePointer = vbHourglass

poSendmail.SMTPHost = g_strSMTPServer

'If g_strSMTPUserName <> "" Then
    poSendmail.UseAuthentication = False
'    poSendmail.Username = g_strSMTPUserName
'    poSendmail.Password = g_strSMTPPassword
'End If

poSendmail.Connect

poSendmail.From = lp_strEmailFromAddress
poSendmail.FromDisplayName = g_strFullUserName
poSendmail.Recipient = lp_strEmailToAddress
poSendmail.RecipientDisplayName = lp_strEmailToName
poSendmail.ReplyToAddress = lp_strEmailFromAddress
poSendmail.Subject = lp_strSubject
poSendmail.Attachment = lp_strAttachment     ' file attachment(s), optional
poSendmail.Message = lp_strBody

If lp_strCopyToEmailAddress <> "" Then
    poSendmail.CcRecipient = lp_strCopyToEmailAddress
    poSendmail.CcDisplayName = lp_strCopyToName
End If

If lp_strBCCAddress <> "" Then
    poSendmail.BccRecipient = lp_strBCCAddress
End If

poSendmail.Send

poSendmail.Disconnect

Screen.MousePointer = vbDefault

Close_SMTP:

Set poSendmail = Nothing

Screen.MousePointer = vbDefault

Exit Sub

SentSMTPMail_Error:

If lp_blnDontShowErrors = False Then
    MsgBox "Error: " & Err.Number & vbCrLf & vbCrLf & Err.Description, vbExclamation
End If

End Sub

