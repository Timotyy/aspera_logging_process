Attribute VB_Name = "AsperaLoggingUnusedFunctions"
Option Explicit

Public Sub CheckMainServerBBCMGUploads()

Dim l_ImageDimensions As ImgDimType, Temp As String, BBCMG_OrderID As Long, BBCMG_MediaKey As String
Dim l_rstID As ADODB.Recordset, LastID As Long, AltLocation As String, l_blnWarpError As Boolean, l_blnWarpError2 As Boolean, l_lngCounter As Long
 
Set l_rstID = New ADODB.Recordset

If Mid(g_strCetaConnection, 26, 6) = "RRTEST" Then
    SQL = "SELECT * FROM fasp_sessions WHERE (user = 'Aspera-BBCMG-UAT') AND operation = 'Upload' AND (status = 'completed' OR status = 'running') AND created_at > '" & FormatSQLDate(Nowdate, True) & "' ORDER BY created_at;"
Else
    SQL = "SELECT * FROM fasp_sessions WHERE (user = 'Aspera-BBCMG') AND operation = 'Upload' AND (status = 'completed' or status = 'running') AND created_at > '" & FormatSQLDate(Nowdate, True) & "' ORDER BY created_at;"
End If
l_rstLoggedEvents.Open SQL, cnn2, 3, 3
If Not l_rstLoggedEvents.EOF Then
    l_lngCounter = 0
    l_rstLoggedEvents.MoveFirst
    Do While Not l_rstLoggedEvents.EOF
        l_lngCounter = l_lngCounter + 1
        l_rstLoggedEvents.MoveNext
    Loop
    l_rstLoggedEvents.MoveFirst
    Debug.Print "BBCMG Delivery " & l_lngCounter & " records"
    l_lngCounter = 1
    l_rstLoggedEvents.MoveFirst
    While Not l_rstLoggedEvents.EOF
        Debug.Print l_lngCounter
        l_lngCounter = l_lngCounter + 1
        If GetData("webdelivery", "webdeliveryID", "asperatoken", l_rstLoggedEvents("session_id")) = 0 Then
            'New Upload detected
            l_rstAsperaFiles.Open "SELECT * FROM fasp_files WHERE session_id = '" & l_rstLoggedEvents("session_id") & "' AND status = 'completed';", cnn2, 3, 3
            If Not l_rstAsperaFiles.EOF Then
                l_rstAsperaFiles.MoveFirst
                While Not l_rstAsperaFiles.EOF
                    If FSO.FileExists(FlipSlash(l_rstAsperaFiles("file_fullpath"))) Then
                        fileName = l_rstAsperaFiles("file_basename")
                        TempResult = Mid(l_rstAsperaFiles("file_fullpath"), 35)
                        Count = InStr(TempResult, fileName)
                        If Count > 2 Then
                            AltLocation = FlipSlash(Left(TempResult, Count - 2))
                        Else
                            AltLocation = ""
                        End If
                        
                        'Then carry on and log the file in and update it's MediaInfo.
                        SQL = "SELECT eventID, bigfilesize, soundlay FROM events WHERE companyID = 924 AND libraryID = 650950 AND altlocation = '" & QuoteSanitise(AltLocation) & "' AND clipfilename = '" & QuoteSanitise(fileName) & "' AND system_deleted = 0;"
                        l_rstID.Open SQL, cnn, 3, 3
                        If l_rstID.EOF Then
                            l_rstID.Close
                            SQL = "INSERT INTO events (eventmediatype, libraryID, altlocation, clipfilename, companyID, companyname, eventtype, clipsoundformat, clipreference, internalreference, cdate, cuser, mdate, muser, soundlay, bigfilesize) VALUES ("
                            SQL = SQL & "'FILE', "
                            SQL = SQL & "650950, "
                            SQL = SQL & "'" & QuoteSanitise(AltLocation) & "', "
                            SQL = SQL & "'" & QuoteSanitise(fileName) & "', "
                            SQL = SQL & "924, "
                            SQL = SQL & "'BBC Worldwide Motion Gallery', "
                            SQL = SQL & "'Original Master', "
                            SQL = SQL & "'UK_1_BBCMG', "
                            Count = 0
                            While InStr(Mid(fileName, Count + 1), ".") > 0
                                Count = Count + InStr(Mid(fileName, Count + 1), ".")
                            Wend
                            If Count > 0 Then
                                SQL = SQL & "'" & QuoteSanitise(Left(fileName, Count - 1)) & "', "
                            Else
                                SQL = SQL & "'" & QuoteSanitise(fileName) & "', "
                            End If
                            SQL = SQL & GetNextSequence("internalreference") & ", "
                            SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                            SQL = SQL & "'ASPERA', "
                            SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                            SQL = SQL & "'ASPERA', "
                            SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                            SQL = SQL & "'" & l_rstAsperaFiles("size") & "');SELECT SCOPE_IDENTITY();"
                            
                            Set l_rstID = cnn.Execute(SQL)
                            LastID = l_rstID.NextRecordset().Fields(0).Value
                        Else
                            LastID = l_rstID("eventID")
                            l_rstID("bigfilesize") = Trim(" " & l_rstAsperaFiles("size"))
                            l_rstID("soundlay") = FormatSQLDate(Now)
                            l_rstID.Update
                            l_rstID.Close
                        End If
                        
                        If LastID <> 0 Then
                            l_strPathToFile = GetData("library", "subtitle", "libraryID", 650950)
                            l_strPathToFile = l_strPathToFile & "\" & AltLocation
                            l_strPathToFile = l_strPathToFile & "\" & fileName
                            MediaData = GetMediaInfoOnFile(l_strPathToFile)
                            If MediaData.txtFormat = "TTML" Then
                                SetData "events", "clipformat", "eventID", LastID, "ITT Subtitles"
                            ElseIf MediaData.txtFormat <> "" Then
                                SetData "events", "clipformat", "eventID", LastID, MediaData.txtFormat
                                SetData "events", "clipaudiocodec", "eventID", LastID, MediaData.txtAudioCodec
                                SetData "events", "clipframerate", "eventID", LastID, MediaData.txtFrameRate
                                SetData "events", "cbrvbr", "eventID", LastID, MediaData.txtCBRVBR
                                SetData "events", "cliphorizontalpixels", "eventID", LastID, MediaData.txtWidth
                                SetData "events", "clipverticalpixels", "eventID", LastID, MediaData.txtHeight
                                SetData "events", "videobitrate", "eventID", LastID, MediaData.txtVideoBitrate
                                SetData "events", "interlace", "eventID", LastID, MediaData.txtInterlace
                                SetData "events", "timecodestart", "eventID", LastID, MediaData.txtTimecodestart
                                SetData "events", "timecodestop", "eventID", LastID, MediaData.txtTimecodestop
                                SetData "events", "fd_length", "eventID", LastID, MediaData.txtDuration
                                SetData "events", "audiobitrate", "eventID", LastID, MediaData.txtAudioBitrate
                                SetData "events", "trackcount", "eventID", LastID, MediaData.txtTrackCount
                                SetData "events", "channelcount", "eventID", LastID, MediaData.txtChannelCount
                                If Val(MediaData.txtAudioBitrate) + Val(MediaData.txtVideoBitrate) <> 0 Then
                                    SetData "events", "clipbitrate", "eventID", LastID, Val(MediaData.txtAudioBitrate) + Val(MediaData.txtVideoBitrate)
                                Else
                                    SetData "events", "clipbitrate", "eventID", LastID, Val(MediaData.txtOverallBitrate)
                                End If
                                SetData "events", "aspectratio", "eventID", LastID, MediaData.txtAspect
                                If MediaData.txtAspect = "4:3" Then
                                    SetData "events", "fourbythreeflag", "eventID", LastID, 1
                                Else
                                    SetData "events", "fourbythreeflag", "eventID", LastID, 0
                                End If
                                SetData "events", "geometriclinearity", "eventID", LastID, MediaData.txtGeometry
                                SetData "events", "clipcodec", "eventID", LastID, MediaData.txtVideoCodec
                                SetData "events", "colorspace", "eventID", LastID, MediaData.txtColorSpace
                                SetData "events", "chromasubsampling", "eventID", LastID, MediaData.txtChromaSubsmapling
                                If Trim(" " & GetData("events", "eventtype", "eventID", LastID)) = "" Then SetData "events", "eventtype", "eventID", LastID, GetData("xref", "format", "description", MediaData.txtVideoCodec)
                                SetData "events", "mediastreamtype", "eventID", LastID, MediaData.txtStreamType
                                SetData "events", "lastmediainfoquery", "eventID", LastID, FormatSQLDate(Now)
                            Else
                                Select Case UCase(Right(fileName, 3))
                                    Case "ISO"
                                        SetData "events", "clipformat", "eventID", LastID, "DVD ISO Image"
                                    Case "PDF"
                                        SetData "events", "clipformat", "eventID", LastID, "PDF File"
                                    Case "PSD"
                                        SetData "events", "clipformat", "eventID", LastID, "PSD Still Image"
                                    Case "SCC"
                                        SetData "events", "clipformat", "eventID", LastID, "SCC File (EIA 608)"
                                    Case "TIF"
                                        SetData "events", "clipformat", "eventID", LastID, "TIF Still Image"
                                    Case "DOC", "DOCX"
                                        SetData "events", "clipformat", "eventID", LastID, "Word File"
                                    Case "XLS", "XLSX"
                                        SetData "events", "clipformat", "eventID", LastID, "XLS File"
                                    Case "XML"
                                        SetData "events", "clipformat", "eventID", LastID, "XML File"
                                    Case "RAR"
                                        SetData "events", "clipformat", "eventID", LastID, "RAR Archive"
                                    Case "ZIP"
                                        SetData "events", "clipformat", "eventID", LastID, "ZIP Archive"
                                    Case "TAR"
                                        SetData "events", "clipformat", "eventID", LastID, "TAR Archive"
                                    Case Else
                                        SetData "events", "clipformat", "eventID", LastID, "Other"
                                End Select
                            End If
                            cnn.Execute "INSERT INTO eventhistory (eventID, datesaved, username, description) VALUES (" & LastID & ", getdate(), 'BBCMG', 'BBCMG Upload');"
                        End If
                            
                        If LastID <> 0 And l_rstLoggedEvents("status") = "completed" Then
                            SQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipid, accesstype, finalstatus, asperatoken, processafterupload) VALUES ("
                            SQL = SQL & "2753, "
                            SQL = SQL & "924, "
                            SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                            SQL = SQL & LastID & ", "
                            SQL = SQL & "'Aspera Upload', "
                            SQL = SQL & "'Completed', "
                            SQL = SQL & "'" & l_rstLoggedEvents("session_id") & "', "
                            SQL = SQL & "0);"
                            cnn.Execute SQL
                        End If
                        SendSMTPMail g_strUserEmailAddress, g_strUserEmailName, "BBCMG Upload via Aspera Point-to-Point", fileName & " has been uploaded and is on the Landing Pad in the Aspera-BBCMG folder.", "", True, "bbcmg-VIE@visualdatamedia.com", "VDMS BBCMG Team", g_strAdministratorEmailAddress
                    End If
                    l_rstAsperaFiles.MoveNext
                Wend
            End If
            l_rstAsperaFiles.Close
        End If
        l_rstLoggedEvents.MoveNext
    Wend
End If
l_rstLoggedEvents.Close
Set l_rstID = Nothing

End Sub

Public Sub CheckMainServerTurnerUploads()

Dim l_ImageDimensions As ImgDimType, Temp As String
Dim l_rstID As ADODB.Recordset, LastID As Long, AltLocation As String, l_blnWarpError As Boolean, l_blnWarpError2 As Boolean, l_lngCounter As Long

Set l_rstID = New ADODB.Recordset

SQL = "SELECT * FROM fasp_sessions WHERE (user = 'Aspera-Turner') AND operation = 'Upload' AND status = 'completed' AND created_at > '" & FormatSQLDate(Nowdate, True) & "' ORDER BY created_at;"
l_rstLoggedEvents.Open SQL, cnn2, 3, 3
If Not l_rstLoggedEvents.EOF Then
    Debug.Print "Aspera Turner Uploads " & l_rstLoggedEvents.RecordCount & " records"
    l_lngCounter = 1
    l_rstLoggedEvents.MoveFirst
    While Not l_rstLoggedEvents.EOF
        Debug.Print l_lngCounter
        l_lngCounter = l_lngCounter + 1
        If GetData("webdelivery", "webdeliveryID", "asperatoken", l_rstLoggedEvents("session_id")) = 0 Then
            'New Upload detected
            l_rstAsperaFiles.Open "SELECT * FROM fasp_files WHERE session_id = '" & l_rstLoggedEvents("session_id") & "';", cnn2, 3, 3
            If Not l_rstAsperaFiles.EOF Then
                l_rstAsperaFiles.MoveFirst
                While Not l_rstAsperaFiles.EOF
                    fileName = l_rstAsperaFiles("file_basename")
                    If l_rstLoggedEvents("user") = "Aspera-Turner" Then
                        TempResult = Mid(l_rstAsperaFiles("file_fullpath"), 19)
                    End If
                    Count = InStr(TempResult, fileName)
                    If Count > 2 Then
                        AltLocation = FlipSlash(Left(TempResult, Count - 2))
                    Else
                        AltLocation = ""
                    End If
                    
                    'Then carry on and log the file in and update it's MediaInfo.
                    If l_rstLoggedEvents("user") = "Aspera-Turner" Then
'                        SQL = "SELECT eventID, bigfilesize, soundlay FROM events WHERE companyID = 1517 AND libraryID = 299871 AND altlocation = '" & QuoteSanitise(AltLocation) & "' AND clipfilename = '" & QuoteSanitise(fileName) & "';"
                        SQL = "SELECT eventID, bigfilesize, soundlay FROM events WHERE companyID = 1517 AND libraryID = 672233 AND altlocation = '" & QuoteSanitise(AltLocation) & "' AND clipfilename = '" & QuoteSanitise(fileName) & "';"
                    End If
                    l_rstID.Open SQL, cnn, 3, 3
                    If l_rstID.EOF Then
                        l_rstID.Close
                        SQL = "INSERT INTO events (eventmediatype, libraryID, altlocation, clipfilename, companyID, companyname, eventtype, clipsoundformat, clipreference, internalreference, cdate, cuser, mdate, muser, soundlay, bigfilesize) VALUES ("
                        SQL = SQL & "'FILE', "
'                        SQL = SQL & "299871, "
                        SQL = SQL & "672233, "
                        SQL = SQL & "'" & QuoteSanitise(AltLocation) & "', "
                        SQL = SQL & "'" & QuoteSanitise(fileName) & "', "
                        If l_rstLoggedEvents("user") = "Aspera-Turner" Then
                            SQL = SQL & "1517, "
                            SQL = SQL & "'Turner Clips', "
                            SQL = SQL & "'Original Master', "
                            SQL = SQL & "'UK_1_TURNER', "
                        End If
                        Count = 0
                        While InStr(Mid(fileName, Count + 1), ".") > 0
                            Count = Count + InStr(Mid(fileName, Count + 1), ".")
                        Wend
                        If Count > 0 Then
                            SQL = SQL & "'" & QuoteSanitise(Left(fileName, Count - 1)) & "', "
                        Else
                            SQL = SQL & "'" & QuoteSanitise(fileName) & "', "
                        End If
                        SQL = SQL & GetNextSequence("internalreference") & ", "
                        SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                        SQL = SQL & "'ASPERA', "
                        SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                        SQL = SQL & "'ASPERA', "
                        SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                        SQL = SQL & "'" & l_rstAsperaFiles("size") & "');SELECT SCOPE_IDENTITY();"
                        
                        Set l_rstID = cnn.Execute(SQL)
                        LastID = l_rstID.NextRecordset().Fields(0).Value
                    
                        If LastID <> 0 Then
                            l_strPathToFile = GetData("library", "subtitle", "libraryID", 672233)
                            l_strPathToFile = l_strPathToFile & "\" & AltLocation
                            l_strPathToFile = l_strPathToFile & "\" & fileName
                            MediaData = GetMediaInfoOnFile(l_strPathToFile)
                            If MediaData.txtFormat = "TTML" Then
                                SetData "events", "clipformat", "eventID", LastID, "ITT Subtitles"
'                            ElseIf MediaData.txtFormat = "JPEG Image" Or MediaData.txtFormat = "PNG Image" Or MediaData.txtFormat = "BMP Image" Or MediaData.txtFormat = "GIF Image" Then
'                                SetData "events", "clipformat", "eventID", LastID, MediaData.txtFormat
'                                If getImgDim(l_strPathToFile, l_ImageDimensions, Temp) = True Then
'                                    SetData "events", "clipverticalpixels", "eventID", LastID, l_ImageDimensions.height
'                                    SetData "events", "cliphorizontalpixels", "eventID", LastID, l_ImageDimensions.width
'                                End If
                            ElseIf MediaData.txtFormat <> "" Then
                                SetData "events", "clipformat", "eventID", LastID, MediaData.txtFormat
                                SetData "events", "clipaudiocodec", "eventID", LastID, MediaData.txtAudioCodec
                                SetData "events", "clipframerate", "eventID", LastID, MediaData.txtFrameRate
                                SetData "events", "cbrvbr", "eventID", LastID, MediaData.txtCBRVBR
                                SetData "events", "cliphorizontalpixels", "eventID", LastID, MediaData.txtWidth
                                SetData "events", "clipverticalpixels", "eventID", LastID, MediaData.txtHeight
                                SetData "events", "videobitrate", "eventID", LastID, MediaData.txtVideoBitrate
                                SetData "events", "interlace", "eventID", LastID, MediaData.txtInterlace
                                SetData "events", "timecodestart", "eventID", LastID, MediaData.txtTimecodestart
                                SetData "events", "timecodestop", "eventID", LastID, MediaData.txtTimecodestop
                                SetData "events", "fd_length", "eventID", LastID, MediaData.txtDuration
                                SetData "events", "audiobitrate", "eventID", LastID, MediaData.txtAudioBitrate
                                SetData "events", "trackcount", "eventID", LastID, MediaData.txtTrackCount
                                SetData "events", "channelcount", "eventID", LastID, MediaData.txtChannelCount
                                If Val(MediaData.txtAudioBitrate) + Val(MediaData.txtVideoBitrate) <> 0 Then
                                    SetData "events", "clipbitrate", "eventID", LastID, Val(MediaData.txtAudioBitrate) + Val(MediaData.txtVideoBitrate)
                                Else
                                    SetData "events", "clipbitrate", "eventID", LastID, Val(MediaData.txtOverallBitrate)
                                End If
                                SetData "events", "aspectratio", "eventID", LastID, MediaData.txtAspect
                                If MediaData.txtAspect = "4:3" Then
                                    SetData "events", "fourbythreeflag", "eventID", LastID, 1
                                Else
                                    SetData "events", "fourbythreeflag", "eventID", LastID, 0
                                End If
                                SetData "events", "geometriclinearity", "eventID", LastID, MediaData.txtGeometry
                                SetData "events", "clipcodec", "eventID", LastID, MediaData.txtVideoCodec
                                SetData "events", "colorspace", "eventID", LastID, MediaData.txtColorSpace
                                SetData "events", "chromasubsampling", "eventID", LastID, MediaData.txtChromaSubsmapling
                                If Trim(" " & GetData("events", "eventtype", "eventID", LastID)) = "" Then SetData "events", "eventtype", "eventID", LastID, GetData("xref", "format", "description", MediaData.txtVideoCodec)
                                SetData "events", "mediastreamtype", "eventID", LastID, MediaData.txtStreamType
                                SetData "events", "lastmediainfoquery", "eventID", LastID, FormatSQLDate(Now)
                            Else
                                Select Case UCase(Right(fileName, 3))
                                    Case "ISO"
                                        SetData "events", "clipformat", "eventID", LastID, "DVD ISO Image"
                                    Case "PDF"
                                        SetData "events", "clipformat", "eventID", LastID, "PDF File"
                                    Case "PSD"
                                        SetData "events", "clipformat", "eventID", LastID, "PSD Still Image"
                                    Case "SCC"
                                        SetData "events", "clipformat", "eventID", LastID, "SCC File (EIA 608)"
                                    Case "TIF"
                                        SetData "events", "clipformat", "eventID", LastID, "TIF Still Image"
                                    Case "DOC", "DOCX"
                                        SetData "events", "clipformat", "eventID", LastID, "Word File"
                                    Case "XLS", "XLSX"
                                        SetData "events", "clipformat", "eventID", LastID, "XLS File"
                                    Case "XML"
                                        SetData "events", "clipformat", "eventID", LastID, "XML File"
                                    Case "RAR"
                                        SetData "events", "clipformat", "eventID", LastID, "RAR Archive"
                                    Case "ZIP"
                                        SetData "events", "clipformat", "eventID", LastID, "ZIP Archive"
                                    Case "TAR"
                                        SetData "events", "clipformat", "eventID", LastID, "TAR Archive"
                                    Case Else
                                        SetData "events", "clipformat", "eventID", LastID, "Other"
                                End Select
                            End If
                            If l_rstLoggedEvents("user") = "Aspera-Turner" Then
                                cnn.Execute "INSERT INTO eventhistory (eventID, datesaved, username, description) VALUES (" & LastID & ", getdate(), 'Turner', 'Turner Upload');"
                            End If
                        End If
                    Else
                        LastID = l_rstID("eventID")
                        l_rstID("bigfilesize") = Trim(" " & l_rstAsperaFiles("size"))
                        l_rstID("soundlay") = FormatSQLDate(Now)
                        l_rstID.Update
                        l_rstID.Close
                    End If
                        
                    
                    SQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipid, accesstype, finalstatus, asperatoken, processafterupload) VALUES ("
                    If l_rstLoggedEvents("user") = "Aspera-Turner" Then
                        SQL = SQL & "2753, "
                        SQL = SQL & "1517, "
                    End If
                    SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                    SQL = SQL & LastID & ", "
                    SQL = SQL & "'Aspera Upload', "
                    SQL = SQL & "'Completed', "
                    SQL = SQL & "'" & l_rstLoggedEvents("session_id") & "', "
                    SQL = SQL & "0);"
                    
                    cnn.Execute SQL
                    
'                    If l_rstLoggedEvents("user") = "Aspera-Turner" Then
'                        SendSMTPMail g_strUserEmailAddress, g_strUserEmailName, "Turner Upload via Aspera", fileName & " has been uploaded and is on the Temp RAID in the Aspera-Turner folder.", "", True, g_strManagerEmailAddress, g_strManagerEmailName, g_strAdministratorEmailAddress
'                    End If
                    l_rstAsperaFiles.MoveNext
                Wend
            End If
            l_rstAsperaFiles.Close
        End If
        l_rstLoggedEvents.MoveNext
    Wend
End If
l_rstLoggedEvents.Close
Set l_rstID = Nothing

End Sub

Public Sub CheckMainServerDisneyNBCUUploads() 'Not BBCMG

Dim l_ImageDimensions As ImgDimType, Temp As String
Dim l_rstID As ADODB.Recordset, LastID As Long, AltLocation As String, l_blnWarpError As Boolean, l_blnWarpError2 As Boolean, l_lngCounter As Long

Set l_rstID = New ADODB.Recordset

SQL = "SELECT * FROM fasp_sessions WHERE (user = 'DISNEY-ASPERA-NBCU') AND operation = 'Upload' AND status = 'completed' AND created_at > '" & FormatSQLDate(Nowdate, True) & "' ORDER BY created_at;"
l_rstLoggedEvents.Open SQL, cnn2, 3, 3
If Not l_rstLoggedEvents.EOF Then
    Debug.Print "EPGDelivery " & l_rstLoggedEvents.RecordCount & " records"
    l_lngCounter = 1
    l_rstLoggedEvents.MoveFirst
    While Not l_rstLoggedEvents.EOF
        Debug.Print l_lngCounter
        l_lngCounter = l_lngCounter + 1
        If GetData("webdelivery", "webdeliveryID", "asperatoken", l_rstLoggedEvents("session_id")) = 0 Then
            'New Upload detected
            l_rstAsperaFiles.Open "SELECT * FROM fasp_files WHERE session_id = '" & l_rstLoggedEvents("session_id") & "';", cnn2, 3, 3
            If Not l_rstAsperaFiles.EOF Then
                l_rstAsperaFiles.MoveFirst
                While Not l_rstAsperaFiles.EOF
                    fileName = l_rstAsperaFiles("file_basename")
                    If l_rstLoggedEvents("user") = "DISNEY-ASPERA-NBCU" Then
                        TempResult = Mid(l_rstAsperaFiles("file_fullpath"), 25)
                    End If
                    Count = InStr(TempResult, fileName)
                    If Count > 2 Then
                        AltLocation = FlipSlash(Left(TempResult, Count - 2))
                    Else
                        AltLocation = ""
                    End If
                    
                    'Then carry on and log the file in and update it's MediaInfo.
                    If l_rstLoggedEvents("user") = "DISNEY-ASPERA-NBCU" Then
                        SQL = "SELECT eventID, bigfilesize, soundlay FROM events WHERE companyID = 1163 AND libraryID = 284349 AND altlocation = '" & QuoteSanitise(AltLocation) & "' AND clipfilename = '" & QuoteSanitise(fileName) & "';"
                    End If
                    l_rstID.Open SQL, cnn, 3, 3
                    If l_rstID.EOF Then
                        l_rstID.Close
                        SQL = "INSERT INTO events (eventmediatype, libraryID, altlocation, clipfilename, companyID, companyname, eventtype, clipsoundformat, clipreference, internalreference, cdate, cuser, mdate, muser, soundlay, bigfilesize) VALUES ("
                        SQL = SQL & "'FILE', "
                        SQL = SQL & "284349, "
                        SQL = SQL & "'" & QuoteSanitise(AltLocation) & "', "
                        SQL = SQL & "'" & QuoteSanitise(fileName) & "', "
                        If l_rstLoggedEvents("user") = "DISNEY-ASPERA-NBCU" Then
                            SQL = SQL & "1163, "
                            SQL = SQL & "'The Walt Disney Company Ltd.', "
                            SQL = SQL & "'Original Master', "
                            SQL = SQL & "'UK_1_DISNEY', "
                        End If
                        Count = 0
                        While InStr(Mid(fileName, Count + 1), ".") > 0
                            Count = Count + InStr(Mid(fileName, Count + 1), ".")
                        Wend
                        If Count > 0 Then
                            SQL = SQL & "'" & QuoteSanitise(Left(fileName, Count - 1)) & "', "
                        Else
                            SQL = SQL & "'" & QuoteSanitise(fileName) & "', "
                        End If
                        SQL = SQL & GetNextSequence("internalreference") & ", "
                        SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                        SQL = SQL & "'ASPERA', "
                        SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                        SQL = SQL & "'ASPERA', "
                        SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                        SQL = SQL & "'" & l_rstAsperaFiles("size") & "');SELECT SCOPE_IDENTITY();"
                        
                        Set l_rstID = cnn.Execute(SQL)
                        LastID = l_rstID.NextRecordset().Fields(0).Value
                    
                        If LastID <> 0 Then
                            l_strPathToFile = GetData("library", "subtitle", "libraryID", 284349)
                            l_strPathToFile = l_strPathToFile & "\" & AltLocation
                            l_strPathToFile = l_strPathToFile & "\" & fileName
                            MediaData = GetMediaInfoOnFile(l_strPathToFile)
                            If MediaData.txtFormat = "TTML" Then
                                SetData "events", "clipformat", "eventID", LastID, "ITT Subtitles"
'                            ElseIf MediaData.txtFormat = "JPEG Image" Or MediaData.txtFormat = "PNG Image" Or MediaData.txtFormat = "BMP Image" Or MediaData.txtFormat = "GIF Image" Then
'                                SetData "events", "clipformat", "eventID", LastID, MediaData.txtFormat
'                                If getImgDim(l_strPathToFile, l_ImageDimensions, Temp) = True Then
'                                    SetData "events", "clipverticalpixels", "eventID", LastID, l_ImageDimensions.height
'                                    SetData "events", "cliphorizontalpixels", "eventID", LastID, l_ImageDimensions.width
'                                End If
                            ElseIf MediaData.txtFormat <> "" Then
                                SetData "events", "clipformat", "eventID", LastID, MediaData.txtFormat
                                SetData "events", "clipaudiocodec", "eventID", LastID, MediaData.txtAudioCodec
                                SetData "events", "clipframerate", "eventID", LastID, MediaData.txtFrameRate
                                SetData "events", "cbrvbr", "eventID", LastID, MediaData.txtCBRVBR
                                SetData "events", "cliphorizontalpixels", "eventID", LastID, MediaData.txtWidth
                                SetData "events", "clipverticalpixels", "eventID", LastID, MediaData.txtHeight
                                SetData "events", "videobitrate", "eventID", LastID, MediaData.txtVideoBitrate
                                SetData "events", "interlace", "eventID", LastID, MediaData.txtInterlace
                                SetData "events", "timecodestart", "eventID", LastID, MediaData.txtTimecodestart
                                SetData "events", "timecodestop", "eventID", LastID, MediaData.txtTimecodestop
                                SetData "events", "fd_length", "eventID", LastID, MediaData.txtDuration
                                SetData "events", "audiobitrate", "eventID", LastID, MediaData.txtAudioBitrate
                                SetData "events", "trackcount", "eventID", LastID, MediaData.txtTrackCount
                                SetData "events", "channelcount", "eventID", LastID, MediaData.txtChannelCount
                                If Val(MediaData.txtAudioBitrate) + Val(MediaData.txtVideoBitrate) <> 0 Then
                                    SetData "events", "clipbitrate", "eventID", LastID, Val(MediaData.txtAudioBitrate) + Val(MediaData.txtVideoBitrate)
                                Else
                                    SetData "events", "clipbitrate", "eventID", LastID, Val(MediaData.txtOverallBitrate)
                                End If
                                SetData "events", "aspectratio", "eventID", LastID, MediaData.txtAspect
                                If MediaData.txtAspect = "4:3" Then
                                    SetData "events", "fourbythreeflag", "eventID", LastID, 1
                                Else
                                    SetData "events", "fourbythreeflag", "eventID", LastID, 0
                                End If
                                SetData "events", "geometriclinearity", "eventID", LastID, MediaData.txtGeometry
                                SetData "events", "clipcodec", "eventID", LastID, MediaData.txtVideoCodec
                                SetData "events", "colorspace", "eventID", LastID, MediaData.txtColorSpace
                                SetData "events", "chromasubsampling", "eventID", LastID, MediaData.txtChromaSubsmapling
                                If Trim(" " & GetData("events", "eventtype", "eventID", LastID)) = "" Then SetData "events", "eventtype", "eventID", LastID, GetData("xref", "format", "description", MediaData.txtVideoCodec)
                                SetData "events", "mediastreamtype", "eventID", LastID, MediaData.txtStreamType
                                SetData "events", "lastmediainfoquery", "eventID", LastID, FormatSQLDate(Now)
                            Else
                                Select Case UCase(Right(fileName, 3))
                                    Case "ISO"
                                        SetData "events", "clipformat", "eventID", LastID, "DVD ISO Image"
                                    Case "PDF"
                                        SetData "events", "clipformat", "eventID", LastID, "PDF File"
                                    Case "PSD"
                                        SetData "events", "clipformat", "eventID", LastID, "PSD Still Image"
                                    Case "SCC"
                                        SetData "events", "clipformat", "eventID", LastID, "SCC File (EIA 608)"
                                    Case "TIF"
                                        SetData "events", "clipformat", "eventID", LastID, "TIF Still Image"
                                    Case "DOC", "DOCX"
                                        SetData "events", "clipformat", "eventID", LastID, "Word File"
                                    Case "XLS", "XLSX"
                                        SetData "events", "clipformat", "eventID", LastID, "XLS File"
                                    Case "XML"
                                        SetData "events", "clipformat", "eventID", LastID, "XML File"
                                    Case "RAR"
                                        SetData "events", "clipformat", "eventID", LastID, "RAR Archive"
                                    Case "ZIP"
                                        SetData "events", "clipformat", "eventID", LastID, "ZIP Archive"
                                    Case "TAR"
                                        SetData "events", "clipformat", "eventID", LastID, "TAR Archive"
                                    Case Else
                                        SetData "events", "clipformat", "eventID", LastID, "Other"
                                End Select
                            End If
                            If l_rstLoggedEvents("user") = "DISNEY-ASPERA-NBCU" Then
                                cnn.Execute "INSERT INTO eventhistory (eventID, datesaved, username, description) VALUES (" & LastID & ", getdate(), 'DISNEY', 'Disney NBCU Upload');"
                            End If
                        End If
                    Else
                        LastID = l_rstID("eventID")
                        l_rstID("bigfilesize") = Trim(" " & l_rstAsperaFiles("size"))
                        l_rstID("soundlay") = FormatSQLDate(Now)
                        l_rstID.Update
                        l_rstID.Close
                    End If
                        
                    
                    SQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipid, accesstype, finalstatus, asperatoken, processafterupload) VALUES ("
                    If l_rstLoggedEvents("user") = "DISNEY-ASPERA-NBCU" Then
                        SQL = SQL & "2753, "
                        SQL = SQL & "1163, "
                    End If
                    SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                    SQL = SQL & LastID & ", "
                    SQL = SQL & "'Aspera Upload', "
                    SQL = SQL & "'Completed', "
                    SQL = SQL & "'" & l_rstLoggedEvents("session_id") & "', "
                    SQL = SQL & "0);"
                    
                    cnn.Execute SQL
                    
                    If l_rstLoggedEvents("user") = "DISNEY-ASPERA-NBCU" Then
                        SendSMTPMail g_strUserEmailAddress, g_strUserEmailName, "Disney Upload via Aspera Point-to-Point", fileName & " has been uploaded and is on the Asperastore in the DISNEY-ASPERA-NBCU folder.", "", True, g_strManagerEmailAddress, g_strManagerEmailName, g_strAdministratorEmailAddress
                    End If
                    l_rstAsperaFiles.MoveNext
                Wend
            End If
            l_rstAsperaFiles.Close
        End If
        l_rstLoggedEvents.MoveNext
    Wend
End If
l_rstLoggedEvents.Close
Set l_rstID = Nothing

End Sub

Public Sub CheckMainServerZodiakUploads()

Dim l_ImageDimensions As ImgDimType, Temp As String
Dim l_rstID As ADODB.Recordset, LastID As Long, AltLocation As String, l_blnWarpError As Boolean, l_blnWarpError2 As Boolean, l_lngCounter As Long

Set l_rstID = New ADODB.Recordset

SQL = "SELECT * FROM fasp_sessions WHERE (user = 'Aspera-Zodiak') AND operation = 'Upload' AND status = 'completed' AND created_at > '" & FormatSQLDate(Nowdate, True) & "' ORDER BY created_at;"
l_rstLoggedEvents.Open SQL, cnn2, 3, 3
If Not l_rstLoggedEvents.EOF Then
    Debug.Print "Aspera Zodiak Uploads " & l_rstLoggedEvents.RecordCount & " records"
    l_lngCounter = 1
    l_rstLoggedEvents.MoveFirst
    While Not l_rstLoggedEvents.EOF
        Debug.Print l_lngCounter
        l_lngCounter = l_lngCounter + 1
        If GetData("webdelivery", "webdeliveryID", "asperatoken", l_rstLoggedEvents("session_id")) = 0 Then
            'New Upload detected
            l_rstAsperaFiles.Open "SELECT * FROM fasp_files WHERE session_id = '" & l_rstLoggedEvents("session_id") & "';", cnn2, 3, 3
            If Not l_rstAsperaFiles.EOF Then
                l_rstAsperaFiles.MoveFirst
                While Not l_rstAsperaFiles.EOF
                    fileName = l_rstAsperaFiles("file_basename")
                    If l_rstLoggedEvents("user") = "Aspera-Zodiak" Then
                        TempResult = Mid(l_rstAsperaFiles("file_fullpath"), 35)
                    End If
                    Count = InStr(TempResult, fileName)
                    If Count > 2 Then
                        AltLocation = FlipSlash(Left(TempResult, Count - 2))
                    Else
                        AltLocation = ""
                    End If
                    
                    'Then carry on and log the file in and update it's MediaInfo.
                    If l_rstLoggedEvents("user") = "Aspera-Zodiak" Then
                        SQL = "SELECT eventID, bigfilesize, soundlay FROM events WHERE companyID = 1381 AND libraryID = 650950 AND altlocation = '" & QuoteSanitise(AltLocation) & "' AND clipfilename = '" & QuoteSanitise(fileName) & "';"
                    End If
                    l_rstID.Open SQL, cnn, 3, 3
                    If l_rstID.EOF Then
                        l_rstID.Close
                        SQL = "INSERT INTO events (eventmediatype, libraryID, altlocation, clipfilename, companyID, companyname, eventtype, clipsoundformat, clipreference, internalreference, cdate, cuser, mdate, muser, soundlay, bigfilesize) VALUES ("
                        SQL = SQL & "'FILE', "
                        SQL = SQL & "650950, "
                        SQL = SQL & "'" & QuoteSanitise(AltLocation) & "', "
                        SQL = SQL & "'" & QuoteSanitise(fileName) & "', "
                        If l_rstLoggedEvents("user") = "Aspera-Zodiak" Then
                            SQL = SQL & "1381, "
                            SQL = SQL & "'Zodiak Clips', "
                            SQL = SQL & "'Original Master', "
                            SQL = SQL & "'UK_1_ZODIAK', "
                        End If
                        Count = 0
                        While InStr(Mid(fileName, Count + 1), ".") > 0
                            Count = Count + InStr(Mid(fileName, Count + 1), ".")
                        Wend
                        If Count > 0 Then
                            SQL = SQL & "'" & QuoteSanitise(Left(fileName, Count - 1)) & "', "
                        Else
                            SQL = SQL & "'" & QuoteSanitise(fileName) & "', "
                        End If
                        SQL = SQL & GetNextSequence("internalreference") & ", "
                        SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                        SQL = SQL & "'ASPERA', "
                        SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                        SQL = SQL & "'ASPERA', "
                        SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                        SQL = SQL & "'" & l_rstAsperaFiles("size") & "');SELECT SCOPE_IDENTITY();"
                        
                        Set l_rstID = cnn.Execute(SQL)
                        LastID = l_rstID.NextRecordset().Fields(0).Value
                    
                        If LastID <> 0 Then
                            l_strPathToFile = GetData("library", "subtitle", "libraryID", 650950)
                            l_strPathToFile = l_strPathToFile & "\" & AltLocation
                            l_strPathToFile = l_strPathToFile & "\" & fileName
                            MediaData = GetMediaInfoOnFile(l_strPathToFile)
                            If MediaData.txtFormat = "TTML" Then
                                SetData "events", "clipformat", "eventID", LastID, "ITT Subtitles"
'                            ElseIf MediaData.txtFormat = "JPEG Image" Or MediaData.txtFormat = "PNG Image" Or MediaData.txtFormat = "BMP Image" Or MediaData.txtFormat = "GIF Image" Then
'                                SetData "events", "clipformat", "eventID", LastID, MediaData.txtFormat
'                                If getImgDim(l_strPathToFile, l_ImageDimensions, Temp) = True Then
'                                    SetData "events", "clipverticalpixels", "eventID", LastID, l_ImageDimensions.height
'                                    SetData "events", "cliphorizontalpixels", "eventID", LastID, l_ImageDimensions.width
'                                End If
                            ElseIf MediaData.txtFormat <> "" Then
                                SetData "events", "clipformat", "eventID", LastID, MediaData.txtFormat
                                SetData "events", "clipaudiocodec", "eventID", LastID, MediaData.txtAudioCodec
                                SetData "events", "clipframerate", "eventID", LastID, MediaData.txtFrameRate
                                SetData "events", "cbrvbr", "eventID", LastID, MediaData.txtCBRVBR
                                SetData "events", "cliphorizontalpixels", "eventID", LastID, MediaData.txtWidth
                                SetData "events", "clipverticalpixels", "eventID", LastID, MediaData.txtHeight
                                SetData "events", "videobitrate", "eventID", LastID, MediaData.txtVideoBitrate
                                SetData "events", "interlace", "eventID", LastID, MediaData.txtInterlace
                                SetData "events", "timecodestart", "eventID", LastID, MediaData.txtTimecodestart
                                SetData "events", "timecodestop", "eventID", LastID, MediaData.txtTimecodestop
                                SetData "events", "fd_length", "eventID", LastID, MediaData.txtDuration
                                SetData "events", "audiobitrate", "eventID", LastID, MediaData.txtAudioBitrate
                                SetData "events", "trackcount", "eventID", LastID, MediaData.txtTrackCount
                                SetData "events", "channelcount", "eventID", LastID, MediaData.txtChannelCount
                                If Val(MediaData.txtAudioBitrate) + Val(MediaData.txtVideoBitrate) <> 0 Then
                                    SetData "events", "clipbitrate", "eventID", LastID, Val(MediaData.txtAudioBitrate) + Val(MediaData.txtVideoBitrate)
                                Else
                                    SetData "events", "clipbitrate", "eventID", LastID, Val(MediaData.txtOverallBitrate)
                                End If
                                SetData "events", "aspectratio", "eventID", LastID, MediaData.txtAspect
                                If MediaData.txtAspect = "4:3" Then
                                    SetData "events", "fourbythreeflag", "eventID", LastID, 1
                                Else
                                    SetData "events", "fourbythreeflag", "eventID", LastID, 0
                                End If
                                SetData "events", "geometriclinearity", "eventID", LastID, MediaData.txtGeometry
                                SetData "events", "clipcodec", "eventID", LastID, MediaData.txtVideoCodec
                                SetData "events", "colorspace", "eventID", LastID, MediaData.txtColorSpace
                                SetData "events", "chromasubsampling", "eventID", LastID, MediaData.txtChromaSubsmapling
                                If Trim(" " & GetData("events", "eventtype", "eventID", LastID)) = "" Then SetData "events", "eventtype", "eventID", LastID, GetData("xref", "format", "description", MediaData.txtVideoCodec)
                                SetData "events", "mediastreamtype", "eventID", LastID, MediaData.txtStreamType
                                SetData "events", "lastmediainfoquery", "eventID", LastID, FormatSQLDate(Now)
                            Else
                                Select Case UCase(Right(fileName, 3))
                                    Case "ISO"
                                        SetData "events", "clipformat", "eventID", LastID, "DVD ISO Image"
                                    Case "PDF"
                                        SetData "events", "clipformat", "eventID", LastID, "PDF File"
                                    Case "PSD"
                                        SetData "events", "clipformat", "eventID", LastID, "PSD Still Image"
                                    Case "SCC"
                                        SetData "events", "clipformat", "eventID", LastID, "SCC File (EIA 608)"
                                    Case "TIF"
                                        SetData "events", "clipformat", "eventID", LastID, "TIF Still Image"
                                    Case "DOC", "DOCX"
                                        SetData "events", "clipformat", "eventID", LastID, "Word File"
                                    Case "XLS", "XLSX"
                                        SetData "events", "clipformat", "eventID", LastID, "XLS File"
                                    Case "XML"
                                        SetData "events", "clipformat", "eventID", LastID, "XML File"
                                    Case "RAR"
                                        SetData "events", "clipformat", "eventID", LastID, "RAR Archive"
                                    Case "ZIP"
                                        SetData "events", "clipformat", "eventID", LastID, "ZIP Archive"
                                    Case "TAR"
                                        SetData "events", "clipformat", "eventID", LastID, "TAR Archive"
                                    Case Else
                                        SetData "events", "clipformat", "eventID", LastID, "Other"
                                End Select
                            End If
                            If l_rstLoggedEvents("user") = "Aspera-Zodiak" Then
                                cnn.Execute "INSERT INTO eventhistory (eventID, datesaved, username, description) VALUES (" & LastID & ", getdate(), 'Zodiak', 'Zodiak Upload');"
                            End If
                        End If
                    Else
                        LastID = l_rstID("eventID")
                        l_rstID("bigfilesize") = Trim(" " & l_rstAsperaFiles("size"))
                        l_rstID("soundlay") = FormatSQLDate(Now)
                        l_rstID.Update
                        l_rstID.Close
                    End If
                        
                    
                    SQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipid, accesstype, finalstatus, asperatoken, processafterupload) VALUES ("
                    If l_rstLoggedEvents("user") = "Aspera-Zodiak" Then
                        SQL = SQL & "2753, "
                        SQL = SQL & "1381, "
                    End If
                    SQL = SQL & "'" & FormatSQLDate(Now) & "', "
                    SQL = SQL & LastID & ", "
                    SQL = SQL & "'Aspera Upload', "
                    SQL = SQL & "'Completed', "
                    SQL = SQL & "'" & l_rstLoggedEvents("session_id") & "', "
                    SQL = SQL & "0);"
                    
                    cnn.Execute SQL
                    
                    If l_rstLoggedEvents("user") = "Aspera-Zodiak" Then
                        SendSMTPMail g_strUserEmailAddress, g_strUserEmailName, "Zodiak Upload via Aspera", fileName & " has been uploaded and is on the Landing Pad in the Aspera-Zodiak folder.", "", True, g_strManagerEmailAddress, g_strManagerEmailName, g_strAdministratorEmailAddress
                    End If
                    l_rstAsperaFiles.MoveNext
                Wend
            End If
            l_rstAsperaFiles.Close
        End If
        l_rstLoggedEvents.MoveNext
    Wend
End If
l_rstLoggedEvents.Close
Set l_rstID = Nothing

End Sub

